# RDM in a CRC grant application

<!-- Table of contents for the GitLab version, must be deleted/commented out for a PDF version: -->
[[_TOC_]]

<!-- TO DO: email address of the institution's RDM officer. -->
For assistance, please contact FAU's **research data team** early in the application process: [forschungsdaten@fau.de](mailto:forschungsdaten@fau.de).

Please note the **DFG documents** on research data and the information on NFDI consortia listed in [RDM in a DFG grant application](en/fdm_dfg_antrag_en.md).

Please also take into account the **CRC-specific** information provided by the DFG:

- [Guidelines Collaborative Research Centres Programme](https://www.dfg.de/formulare/50_06/50_06_en.pdf), Section I.1 "Objective" and Section III.1.3 "Information Infrastructure Project" [Nr. 50.06 - 09/24].

You should present the handling of research data in the draft and in the application:

In the **draft**, 1 page is usually dedicated to research data management:

- [Project Description - Draft Proposals for Collaborative Research Centres](https://www.dfg.de/formulare/53_120_elan/53_120_en_elan.rtf), section 2.2 "Research programme and long-term objective" [Nr. 53.120 elan - 09/24].

In the **application** there should not be less than 2 pages on research data management; a detailed statement is expected for data-intensive projects:

- [Proposal Template for the Establishment of a Collaborative Research Centre][[RTF-Version](https://www.dfg.de/resource/blob/168438/46e877de1b213d83b102d97528082f55/60-100-en-rtf-data.rtf)], Section 1.2.2 "Detailed presentation of the research programme", Section 1.3.3 "Research infrastructure". If an Integrated Research Training Group is proposed, Section B.3.3 "Qualification programme" is also relevant. Section C concerns a possible Information Infrastructure Project [Nr. 60.100 - 09/24].

## Central points in the CRC grant application

For information on research data management in the CRC application, please refer to the section "[**Central points in a DFG application**](en/fdm_dfg_antrag_en.md#central-points-in-a-dfg-application)" in [RDM in a DFG grant application](en/fdm_dfg_antrag_en.md).

Please also note the [discipline-specific recommendations](https://www.dfg.de/en/research_funding/principles_dfg_funding/research_data/recommendations/index.html) provided by the DFG.
The handout [Discipline-specific RDM in a DFG grant application](en/fachspezifisches_fdm_dfg_antrag_en.md) provides an overview of the requirements and recommendations.

For CRC applications, the following points are added regarding research data management:

- Development of a cooperative concept for handling research data in the CRC
- Development and enhancement of data infrastructures: connection to existing institutions and structures, considerations on sustainability
- Development of methods for dealing with data
- Elements of education and training in the area of research data management

The description of the handling of used, newly collected and/or processed data should be given in the **Research programme (1.2.2)** or, if applicable, in the **Project plan (A.3.4)** with reference to 1.2.2.
The form in which the **participating institutions** support data management should be explained in **section 1.3.3**.
If you are applying for an **Information Infrastructure Project**, reference can be made to it at this point and the information mentioned below can be provided there:

- Characterisation of the emerging, generated, evaluated data
- Discipline-specific concepts and considerations for quality assurance
- Are there disciplinary standards?
- Archiving: if possible, discipline-specific repositories
- Explanation of any legal restrictions
- Plans regarding the time of data publication
- In what way do the institutions involved in the network support data and information management?

For research data management tasks as part of the coordination project or in the subprojects, you may apply for DFG [resources](https://www.dfg.de/en/research_funding/principles_dfg_funding/research_data/resources_available/index.html).

## Information Infrastructure Project

If the project is **data-driven** or if **particular challenges in data management** will arise: You may [apply for an Information Infrastructure Project](https://www.dfg.de/en/research_funding/programmes/coordinated_programmes/collaborative_research_centres/proposal_process/information_management/index.html) to establish a specific infrastructure to support data management in cooperation with institutions such as the university library and computing centre.
For the objectives and the presentation of a work programme, see application template, section C.3.4 "Project plan" (p. 21):

- Establishing a database for storing research data, ensuring interoperability
- Maintenance, linking and indexing of research data
- Virtual research environments
- Establishment of interoperable components for virtual collaboration within the CRC
- Adaptation and implementation of new technologies and procedures with benefits for the CRC (Information infrastructure not as a pure service provider, but with close scientific links)

According to the [Guidelines for Reviewing](https://www.dfg.de/formulare/60_14/60_14_en.pdf) [Nr. 60.14 - 09/24], the following points are particularly evaluated with regard to an INF project:

- Prototypical developments
- Particular, novel ideas in data handling
- Implementation of discipline-specific standards on data content and formats or formulated alternatives if not available.
- Connection to other institutions, networking with institutions on site
- Appropriate appreciation of the scientific sub-projects' content
- Competence transfer to sub-project leaders and further training of junior researchers in the area of research data
- Considerations on the sustainability/long-term availability of the information infrastructure to be developed.

## Integrated Research Training Group

If you are applying for an Integrated Research Training Group: Please also note the information in the checklist [RDM in an RTG grant application](en/fdm_grk_antrag_en.md) on the qualification concept in the area of research data („Training for the collection and sustainable preservation of research data"), as well as the working paper ["Erstellung eines Service-Portfolios zur FDM-Vermittlung in der Graduiertenausbildung"](https://doi.org/10.5281/zenodo.5572331) [in German only] and the [FAU Service Matrix Trainings](en/fau_service_matrix_schulungen_en.md).
