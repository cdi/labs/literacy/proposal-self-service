# RDM in a DFG grant application

<!-- Table of contents for the GitLab version, must be deleted/commented out for a PDF version -->
[[_TOC_]]

## General Regulations and Recommendations

<!-- TO DO: Enter the institution's RDM contact here. -->
Please contact FAU's **research data team** early in the application process: [forschungsdaten@fau.de](mailto:forschungsdaten@fau.de).
Research data management (RDM) should be incorporated into the project planning and the proposal draft.
**Information on the handling of research data** is **obligatory** in applications for individual and collaborative projects.
The handling of research data enters the DFG review and evaluation process.
This applies not only to digital research data, but also to information on physical research objects, materials, substances or tissues.
<!-- TO DO: If applicable, refer to the institution's policy here. -->
It is recommended to refer to the **[FAU-RDM policy](https://fau.info/fdm-policy)**, the FAIR **[principles](https://www.go-fair.org/fair-principles/)** and a thematically fitting **[NFDI initiative](https://www.nfdi.de/consortia/?lang=en)** somewhere in the application.

With regard to your previous activities, the new [CV template](https://www.dfg.de/formulare/53_200_elan/index.jsp) (obligatory from 01/03/2023) allows for information on additional services to science such as **building a scientific infrastructure** and on a **variety of publication formats such as datasets and software packages**.

Note the **information provided by DFG**:

- [Guidelines for Safeguarding Good Research Practice](http://doi.org/10.5281/zenodo.3923602) (2019), guidelines 11-13, 17
- [Leitlinien zum Umgang mit Forschungsdaten](https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/forschungsdaten/richtlinien_forschungsdaten.pdf) (2015, German only)
- **[Handling of research data - checklist](https://www.dfg.de/resource/blob/174736/92691e48e89bf4ac88c8eb91b8f783b0/forschungsdaten-checkliste-en-data.pdf)** (12/2021)
- [Webpage on handling research data](https://www.dfg.de/en/research_funding/principles_dfg_funding/research_data/index.html)
- [Proposal Preparation Instructions](https://www.dfg.de/formulare/54_01/54_01_en.pdf) (Individual Research Grants, Emmy Noether Programme, Research Units, Clinical Research Units, Priority Programmes) (Nr. 54.01 - 09/2022), Section 2.4

### Individual Research Grants

For individual projects, many proposals provide ½ - 1 ½ pages on research data management; 1-2 sentences per topic (see [Central topics](#central-points-in-a-dfg-application)) is sufficient.

- [Project Description – Project Proposals](http://www.dfg.de/formulare/53_01_elan/53_01_en_elan.rtf) (Nr. 53.01 elan - 03/24)

This does not necessarily have to be done in **Section 2.4** of the application (for individual research grants), but may be covered elsewhere in the application, for example in **Section 2.3** of the description of the planned project.
You may then simply refer to the relevant section for the sake of completeness.

### CRC

The checklist [RDM in a CRC grant application](en/fdm_sfb_antrag_en.md) contains specific guidelines for CRC applications.

### RTG

The checklist [RDM in an RTG grant application](en/fdm_grk_antrag_en.md) contains specific instructions for RTG applications, especially with regard to the qualification concept.
See also the handout "[Erstellung eines Service-Portfolios zur FDM-Vermittlung in der Graduiertenausbildung](https://doi.org/10.5281/zenodo.5572331)" (German only) and the [FAU Service Matrix Trainings](en/fau_service_matrix_schulungen_en.md).

### Discipline-specific guidance

In some cases, there are very specific, extensive [subject-specific requirements](https://www.dfg.de/en/research_funding/principles_dfg_funding/research_data/recommendations/index.html) that are not well referenced in the general application forms.

Recommendations are available for the following areas:

- Humanities and Social Sciences: Theology; Psychology; Education, Subject Didactics; Social Sciences; Ancient Cultures; Social and Cultural Anthropology, Non-European Cultures, Jewish Studies, Religious Studies; Economics; Linguistics; Scientific Editions.
- Life Sciences: Biodiversity Research; Medicine and Biomedicine
- Engineering Sciences: Materials Science and Engineering
- Natural Sciences: Chemistry; Physics; see also Life Sciences for Biology.

The checklist [Subject-specific RDM in a DFG grant applications](en/fachspezifisches_fdm_dfg_antrag_en.md) provides an overview of the requirements and recommendations.

### Methodology-specific information and special case: digitization projects

Attention regarding digitization projects: Very detailed information on metadata, formats, access, archiving, presentation, etc. is required. <!-- TO DO: Enter the mail address of the institution's RDM contact --> Be sure to ask the Research Data Team ([forschungsdaten@fau.de](mailto:forschungsdaten@fau.de)). Some guidelines that should be taken into account:

- „[Digitisation and Indexing Funding Programme](https://www.dfg.de/en/research_funding/programmes/infrastructure/lis/funding_opportunities/digitisation_cataloguing/index.html)"
- [List of forms and guidelines](https://www.dfg.de/foerderung/programme/infrastruktur/lis/lis_foerderangebote/digitalisierung_erschliessung/formulare_merkblaetter/index.jsp)
- [Hinweise zur Antragstellung im Förderprogramm „Digitalisierung und Erschließung"](https://www.dfg.de/download/pdf/foerderung/programme/lis/digitalisierung_erschliessung_hinweise_antragstellung.pdf) (05/2024) (German only)
- [Guidelines and Supplementary Instructions - Digitisation and Indexing](http://www.dfg.de/formulare/12_15/12_15_en.pdf) (Nr. 12.15 - 05/2023)
- [DFG Practical Guidelines on Digitisation](https://www.dfg.de/resource/blob/176110/76abec10bdc30b41f18695145003d6db/12-151-v1216-en-data.pdf) (Nr. 12.151 - 12/2016)
- [Handreichung zur Digitalisierung archivalischer Quellen](https://www.archivschule.de/DE/forschung/digitalisierung-archivalischer-quellen/handreichungen-zur-digitalisierung-archivalischer-quellen.html) (2019) (German only)
- [Guides for the digitisation of medieval manuscripts](https://en.handschriftenzentren.de/materialien/)
- [Handreichungen zur Digitalisierung historischer Zeitungen](https://www.zeitschriftendatenbank.de/zeitungsdigitalisierung/) (2019) (German only)

### NFDI

If appropriate, contact the [NFDI consortia](https://www.nfdi.de/konsortien/). The "**N**ationale **F**orschungs**D**aten**I**nfrastruktur" is a major initiative of the federal and state governments to develop and establish (mostly discipline-specific) services and best practices in data management. <!-- Refer to the participation of the institution, if applicable. --> FAU participates in 11 of 27 consortia:

- [DAPHNE4NFDI](https://www.daphne4nfdi.de/index.php) (Photon and Neutron Experiments - diffraction, tomography, imaging, spectroscopy)
- [FAIRmat](https://www.fairmat-nfdi.eu/fairmat/) (Condensed Matter and Chemical Physics)
- [KonsortSWD](https://www.konsortswd.de/) (Social, Behavioral, and Economic Sciences)
- [MaRDI](https://www.mardi4nfdi.de/) (Mathematics)
- [MatWerk](https://nfdi-matwerk.de/) (Materials Science)
- [NFDI4Cat](https://nfdi4cat.org/) (Catalysis Research)
- [NFDI4Culture](https://nfdi4culture.de/) (Material and Immaterial Cultural Heritage)
- [NFDI4Earth](https://www.nfdi4earth.de/) (Earth System Science - Earth Science)
- [NFDI4Energy](https://nfdi4energy.uol.de/) (Interdisciplinary Energy System Research)
- [NFDI4Microbiota](https://nfdi4microbiota.de/) (Life Sciences)
- [NFDI4Objects](https://www.nfdi4objects.net/) (Material Remains of Human History)

<!-- TO DO: If applicable, mention who may establish contact.  -->
[FAU Competence Center for Research Data and Information (CDI)](https://www.cdi.fau.de) is also happy to provide the local contact information.

Support for handling research data by participating institutions should be mentioned in the proposal.

## Central points in a DFG application

1. **Data description:** The type, scope and volume of the data should be presented. Also mention the data formats used in each research step (e.g. raw data, processed/anonymized data, aggregated data) and if these are standard/common in your field. If not include a half-sentence why you choose these formats (e.g., integral part of the established workflow in the group).

2. **Documentation and quality assurance:** The goal should be documenting the data well enough so that someone in the subject community may understand the process of creation and reproduce your derived results. To emphasize this, refer to existing standards (metadata, ontologies, classifications) that are widely used in the community, where possible. If there is an **NFDI** consortium in your field (see [above](#nfdi)) and you use a standard (or service) recommended there, **feel free to refer to their recommendations**. This also shows that you are already familiar with the NFDI. If you use particular software or use software you wrote yourself, also address how you ensure traceability of the research (by publishing your own software; the software you use is widely used in the discipline, etc.). Also, concisely mention quality assurance measures beyond documentation and standards; such as duplicate data collection or fixed calibration protocols. Finally, take the opportunity to **point out** that data description and documentation is done with the **[FAIR criteria](https://www.go-fair.org/fair-principles/)** in mind.

3. **Storage during the research phase:** Indicate where your data will be backed up and how backup copies will be made. <!-- TO DO: Mention institution's provider --> If you are using a network drive offered by the [RRZE](https://www.rrze.fau.de/), then refer to the fact that data is kept on server systems that are professionally maintained and backed up by the <!-- TO DO: see above --> RRZE. If you are collecting data "in the field" (weather stations, on-site interviews, videos of breeding enclosures ...), you should also succinctly address how data are backed up in the field and transferred to systems with regular backups as quickly as possible. You should also note if any special security measures are in place - such as a customised access rights allocation for sensitive data. If you get support from other institutions for storage (<!-- TO DO: Replace -->RRZE, [HPC Group](https://www.rrze.fau.de/forschung/hpc/), [Leibniz Computing Centre](https://www.lrz.de/)), point out that you can rely on their expertise. For projects with participants outside of <!-- TO DO: Replace with the institution --> FAU, it makes sense to also address access options for external partners and the planned data transfer. <!-- TO DO: If applicable, mention a suitable cloud solution of the institution.  --> The [FAUbox](https://faubox.rrze.uni-erlangen.de/login) is considered suitable for sensitive data by FAU's data protection office.

4. **Legal obligations:** This point is particularly important, if you are collecting data relevant to copyright or data protection laws. It would also be useful to indicate any resulting obligations with respect to data, if you intend to obtain patents or if you have cooperation agreements with partners in industry. If you have unproblematic data (measurement of atoms in an optical lattice; data on the fracture strength of a rod made of a particular alloy; videos of giraffes with no humans in them ...), then a short sentence, such as "The data generated in the research program are not subject to any legal or research ethics requirements or obligations.  <!-- TO DO: Adapt to the institution. --> The project follows the principles set forth in FAU's [Research Data Policy](https://fau.info/fdm-policy)" is sufficient. This way, you also refer to our FAU data policy right away. If there are common requirements or established practices in the discipline regarding data storage, follow them as much as possible and also explicitly refer to them in the proposal.

5. **Data sharing and data access:** In principle, the DFG strongly recommends that data are made available to the community for subsequent use as far as possible and reasonable. You may indicate why data are (not) made available. Here you can state reuse potential, great effort in collection, unique character, or e.g. data protection barriers, copyright, legitimate interests of industrial partners, as reasons. If you do not provide or archive your data in a long-term infrastructure (such as in [Zenodo](https://zenodo.org/), [RatSWD-certified repositories](https://www.konsortswd.de/datenzentren/alle-datenzentren/), NFDI-recommended infrastructures ...), you should always aim at archiving the data on which publications are based for at least ten years (DFG requirement). <!-- TO DO: adapt to the institution: Repository and contact. -->At FAU the [FAUDataCloud](https://www.cdi.fau.de/services/faudatacloud-it-services-fuer-forschungsdaten/) can be used for this purpose (here, please discuss volume requirements with the [CDI](https://www.cdi.fau.de/) before submitting the grant application).
If you make data available to other researchers or publish data, then please address the access conditions (for example, embargo periods until access is possible, access requirements).

6. **Responsibilities and Resources:**
Finally, you should name the persons who are ultimately responsible for individual data management steps in the project. Typical aspects would be training new employees in RDM workflows, coordination between work groups, long-term storage. In smaller projects, this is often one person. You should identify them by name if possible. Also, indicate who or which unit will be responsible for handling data after funding ends (keyword: sustainability).<!-- TO DO: Mention the storage providers mentioned above. --> For storage, you may refer here to services of the RRZE or the FAUDataCloud (see also 5.). Also indicate here which resources are allocated for RDM (personnel resources and other costs, see also next section).

## **Resources for RDM**

Costs for data management, e.g., access fees for qualitative research data, archiving fees, or reprocessing measures for reusability of data, may be requested on a project-specific basis. Individual projects may require IT staff resources for data management on a selective basis. For example, an IT position for only 3 months to customize a database or research environment. <!-- TO DO: If applicable, mention the possibility of using the institution's pool of IT experts. --> In such cases, please contact the [CDI](https://www.cdi.fau.de/): The CDI has access to a pool of IT experts, which may be used to ensure that requested person-months are available at the project's start.
