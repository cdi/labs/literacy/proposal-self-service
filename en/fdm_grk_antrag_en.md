# RDM in a RTG grant application

<!-- Table of contents for the GitLab version, must be deleted/commented out for a PDF version -->
[[_TOC_]]

## General regulations and recommendations

For assistance, please contact the FAU's **research data team** early in the application process: [forschungsdaten@fau.de](forschungsdaten@fau.de). <!-- TO DO: Replace institution name and contact of the institution's RDM office. -->

Note the **DFG documents** on research data and the information on NFDI consortia listed in [RDM in a DFG grant application](en/fdm_dfg_antrag_en.md).

Please also note the **RTG-specific** information provided by the DFG:

- [Draft Proposal Preparation Instructions for Research Training Groups and International Research Training Groups](https://www.dfg.de/formulare/1_303/1_303_en.pdf) [Nr. 1.303 - 01/25]

- [Proposal Preparation Instructions - Proposals to Establish Research Training Groups and International Research Training Groups](https://www.dfg.de/formulare/54_05/54_05_en.pdf), sub-section B. 3.2 (Handling of research data) and sub-section B.4.1 (Qualification programme) [Nr 54.05 - 01/25]

The handling of research data should be presented in the draft and in the application:

In the **draft**, usually ½ - 1 page is dedicated to research data management and the qualification concept regarding research data management:

- [Project Description - Draft Proposals for Research Training Groups and International Research Training Groups](https://www.dfg.de/formulare/53_60_elan/53_60_en_elan.rtf), section 2 "Research programme" and section 3 "Qualification programme and supervision strategy" [Nr. 53.60 elan - 08/22].

In the **application**, up to 2 pages including a section on research data management in the qualification concept are expected:

- [Project Description Research Training Groups and International Research Training Groups](https://www.dfg.de/formulare/53_61_elan/53_61_en_elan.rtf), section 3 "Research programme" and section 4 "Qualification programme" [Nr. 53.61 elan - 08/22].

## Central points in an RTG application

For information on research data management in the RTG application, please refer to the section "**[Central points in the DFG proposal](en/fdm_dfg_antrag_en.md#central-points-in-a-dfg-application)**" in [RDM in a DFG grant application](en/fdm_dfg_antrag_en.md).

Please also note the **[discipline-specific recommendations](https://www.dfg.de/en/research_funding/principles_dfg_funding/research_data/recommendations/index.html)** provided by the DFG.
The handout [Discipline-specific RDM in a DFG grant application](en/fachspezifisches_fdm_dfg_antrag_en.md) provides an overview of the requirements and recommendations.

For **RTG proposals**, the following points regarding research data management are added:

- **Development of a cooperative concept** for research data management within the group.
- **Development of methods** in the area of research data.
- **Development and expansion of data infrastructures**: Connection to existing structures, considerations regarding the sustainability.
- In what form do the **institutions involved** support data and information management?
- The **qualification concept** should include training in the collection, backup, processing and sustainable provision of research data. Which contact persons within <!--TO DO: Replace institution name--> FAU and which external lecturers will be involved in the training concept? For possible subject areas, see below. The FAU research data management team (<forschungsdaten@fau.de>) will also be happy to help. <!-- TO DO: Replace institution name and contact of the person responsible for RDM at the institution. -->

## Thematic areas for RDM training within the RTG

Teaching core competencies in research data management to doctoral students is becoming increasingly important and should be considered in an RTG grant application.
The "Research Data Management Department" [Referat Forschungsdatenmanagement] at the University Library offers an
[Overview of possible training topics and contacts](https://gitlab.rrze.fau.de/cdi/labs/literacy/proposal-self-service/-/blob/main/en/fau_service_matrix_schulungen_en.md). <!-- TO DO: Replace with the institution's corresponding contact point. -->
Further explanations regarding the topics can be found in the overview, see ["Erstellung eines Service-Portfolios zur FDM-Vermittlung in der Graduiertenausbildung"](https://doi.org/10.5281/zenodo.5572331) [in German only].

There are **10 central thematic areas**, which cover basic principles as well as discipline-specific topics and project specifics:

1. (Digital) research data and data management
2. General conditions (policies and requirements at local, national and international level)
3. Data management plans
4. Organisation and structure
5. Storage, backup and access security
6. Documentation and metadata
7. Long-term backup/archiving
8. Data publication
9. Legal aspects
10. Special infrastructure/software tools on site

For an introductory training that covers all topics basally, you may fall back on the courses provided by the university library.
<!-- TO DO: Replace with the relevant institution. -->

For questions and assistance in putting together a suitable training programme, feel free to contact: [forschungsdaten@fau.de](mailto:forschungsdaten@fau.de).
<!-- TO DO: E-mail address of the institution's RDM contact person -->

## Grant application: Text examples regarding the qualification concept

"The qualification concept incorporates workshops on submitting funding applications, research data management and handling of sensitive data.
Where applicable, the FAU-in-house offers are specifically extended by external expertise, e.g. through workshops by *XYZ*."
<!-- TO DO: Replace with in-house offers of the institution. -->

"Building on the basic seminars, the topic of research data management is extended in the Research Training Group by a discipline- and method-specific approach, focal points include *\[e.g. social media data, copyright, data protection, Wikidata, DMPs in the humanities, literature management ...\]*. 
For this, use is made of services from the Digital Skills HUB, the University Library and the FAU Competence Center for Research Data and Information, as well as external lecturers."
<!-- TO DO: Replace with institution's corresponding service offers. -->
