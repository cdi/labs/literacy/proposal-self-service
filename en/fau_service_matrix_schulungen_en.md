# FAU Service Matrix Trainings

| Number | Subject | FAU Competence Center for Research Data and Information | RRZE (Computing Centre) | CSC | University Library | Computer Science | Data Protection Officer (FAU / Clinic) | MIK & Med. Computer Science | Graduate Centre | S-Forschung | Inventor Advice and Patent Management | External or RTG-related |
|:---:|---|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| 1 | Research data and data management - clarification of terms | X |  |  | X |  |  |  | X |  |  |  |
| 2 | Policies and requirements at local, national and international level | X |  |  | X |  |  |  |  | X |  |  |
| 3 | Data Management Plans |  |  |  |  |  |  |  |  |  |  |  |
| 3.2 | Basics |  |  |  | X |  |  |  | X |  |  |  |
| 3.2 | Requirements ( funders, subject-related, ...)  |  |  |  | X |  |  |  |  | X |  |  |
| 3.3 | Tools (RDMO) |  |  |  | X |  |  |  |  |  |  |  |
| 4 | Organisation, order and structure |  |  |  |  |  |  |  |  |  |  |  |
| 4.1 | Basics |  | X |  | X |  |  |  |  |  |  |  |
| 4.2 | Best practices ( subject-specific / data-specific) | X |  | X |  |  |  |  |  |  |  |  |
| 4.3 | Data quality / Management | X |  | X |  | X |  |  |  |  |  |  |
| 4.4 | Tools and software (see detailed list) | (X) | (X) |  |  | (X) |  | (X) |  |  |  | X |
| 4.5 | |  |  |  |  |  |  |  |  |  |  |  |
| 5 | Storage and backup as well as access security | X | X |  |  |  |  |  |  |  |  |  |
| 6 | Documentation and metadata |  |  |  |  |  |  |  |  |  |  |  |
| 6.1 | Fundamentals of documentation | X |  |  | (X) |  |  |  |  |  |  |  |
| 6.2 | Metadata basics | X |  |  | X | (X) |  |  |  |  |  | X |
| 6.3 | Subject-specific / research-specific schemata  Best practices |  |  | (X) |  | (X) |  |  |  |  |  | X |
| 7 | Long-term storage / archiving | X | X |  |  |  |  |  |  |  |  |  |
| 8 | Data publication |  |  |  |  |  |  |  |  |  |  |  |
| 8.1 | Basics |  |  |  | X |  |  |  |  |  |  |  |
| 8.2 | On-site services | X |  |  |  |  |  |  |  |  |  |  |
| 8.3 | Repositories  |  |  |  | X |  |  |  |  |  |  |  |
| 9 | Legal aspects |  |  |  |  |  |  |  |  |  |  |  |
| 9.1 | Copyright for data |  |  |  | X |  |  |  |  |  |  |  |
| 9.1.1 | Licences and reuse |  |  |  | X |  |  |  |  |  |  |  |
| 9.1.2 | Image right |  |  |  | X |  |  |  |  |  |  |  |
| 9.2 | Social media data and data mining (legal issues) |  |  |  | (X)  |  | (X) |  |  |  |  |  |
| 9.3 | Patent law aspects |  |  |  |  |  |  |  |  |  | X (Part of patent training) |  |
| 9.4 | Data protection aspects |  |  |  |  |  |  |  |  |  |  |  |
| 9.4.1 | Basics of data protection, anonymisation, pseudonymisation |  |  |  | X |  | X |  |  |  |  |  |
| 9.4.2 | Informed consent |  |  |  |  |  | X |  |  |  |  |  |
| 9.4.3 | Data protection in international projects |  |  |  |  |  | X |  |  |  |  |  |
| 9.4.4 | Handling medical data |  |  |  |  |  | X | X |  |  |  |  |
| 9.5 | Special aspects (excavation law, ...) |  |  |  |  |  |  |  |  |  |  | X |
| 10 | Special infrastructure / software tools on site |  |  |  |  |  |  |  |  |  |  |  |
| 10.1 | HPC-System / Computing |  | X | X |  |  |  |  |  |  |  |  |
| 10.2 | Electronic lab notebook | X |  | X |  |  |  | X |  |  |  | X |
| 10.3 | Webcrawler / Harvesting | (X) | (X) |  | (X) |  |  |  |  |  |  |  |
| 10.4 | WissKI / CIDOC-CRM | X |  |  |  |  |  |  |  |  |  |  |
| 10.5 | Other (see list of requests) | (X) |  |  |  |  |  |  |  |  |  | X |
