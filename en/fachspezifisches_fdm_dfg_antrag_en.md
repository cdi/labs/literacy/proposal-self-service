# Subject-specific RDM in a DFG grant application

<!-- Table of contents for the GitLab version, must be deleted/commented out for a PDF version -->
[[_TOC_]]

There are sometimes very precise, extensive, [subject-specific
requirements](https://www.dfg.de/en/research_funding/principles_dfg_funding/research_data/recommendations/index.html),
which are not well referenced in the general application forms. This document is intended to serve as a guide, but it is essential to consult the documents provided by the review boards for details.

<!-- TO DO: Delete if necessary -->
For information: see the [repositories list](en/repositorien_en.md) for an overview of subject-specific repositories.

## Humanities and Social Sciences [*Geistes- und Sozialwissenschaften*]

### Theology (edition project) [*Theologie (Editionsvorhaben)*]

*Reference in German: [Fachkollegium "Theologie": Handreichung zu Editionsvorhaben](https://www.dfg.de/resource/blob/175758/06d80235b00decf7cae090b54bb70123/fachkollegium-107-editionen-data.pdf) (2022).*

The edition should be published **open access**; any restriction on access to the data must be justified in the application.

The review board recommends the consideration of the indications and criteria from the **[Förderkriterien für wissenschaftliche Editionen in der Literaturwissenschaft](https://www.dfg.de/resource/blob/172080/895fcc3cb72ec6cd1d5dc84292fb2758/foerderkriterien-editionen-literaturwissenschaft-data.pdf)** (2015) [in German only] provided by the DFG review board "*Literaturwissenschaft*" [Literary Studies].

See also the section [Literary Studies (scholarly editions)](#literary-studies-scientific-editions-literaturwissenschaften-wissenschaftliche-editionen).

### Psychology [*Psychologie*]

*Reference in German: [Management und Bereitstellung von Forschungsdaten in der Psychologie: Überarbeitung der DGPs-Empfehlungen](https://doi.org/10.31234/osf.io/hcxtm) (2020).*

*Reference in English: ["Psychology" Review Board on the handling of research data](https://doi.org/10.31234/osf.io/24ncs) (2020).*

A distinction is made between **raw, primary, and secondary data**.
If there are no legal or ethical reasons for not doing so, open or freely accessible primary data should always be published.
If this is not possible, at least the metadata should be open and accessible.

[The RatSWD provides information on research ethics in the research process: [Sammlung Best-Practice Forschungsethik](https://www.konsortswd.de/ratswd/best-practice-forschungsethik/) (in German only).]

Recommended overarching **metadata standards** are [DDI](https://ddialliance.org/) or [Dublin Core](https://dublincore.org). Further subject-specific information on metadata can be found on the website of the data management tool [DataWiz](https://datawizkb.leibniz-psychology.org/index.php/during-data-collection/what-should-i-know-about-metadata/), which is provided by ZPID.

<!--TO DO: If necessary, replace "BayDSG" with the respective federal state law -->
The relevant sections of the **DSVGO** and the **BayDSG** should be noted, see also the recommendations of [RatSWD](https://doi.org/10.17620/02671.7) [in German only].

Recommended **subject-specific repositories** are [PsychArchives](https://www.psycharchives.org/) from ZPID or [datorium](https://data.gesis.org/sharing/#!Home) at GESIS.

### Education, educational sciences, subject-related didactics [*Erziehungswissenschaften, Bildungswissenschaften, Fachdidaktik*]

*Reference in German: [Empfehlungen zur Archivierung, Bereitstellung und Nachnutzung von Forschungsdaten im Kontext erziehungs- und bildungswissenschaftlicher sowie fachdidaktischer Forschung](https://www.dfg.de/resource/blob/174560/67a06609aa9aaa98e73b9b7d798afbb9/stellungnahme-forschungsdatenmanagement-data.pdf) (2020).*

*Reference in German: [Bereitstellung und Nutzung quantitativer Forschungsdaten in der Bildungsforschung: Memorandum des Fachkollegiums "Erziehungswissenschaften" der DFG](http://dx.doi.org/10.25656/01:11505) (2015).*

A **data management plan** should be created during the planning/application phase and continued to be maintained during the project phase.
The [RDMO Questionnaire](https://rdmo.ub.fau.de/) "DFG grants (educational sciences, review board 109)" can help here. <!-- TO DO: Possibly reference an RDMO instance or another DMP tool. -->
Explicitly indicate in the application that a data management plan has been created and is being maintained.

If **similar research data already exists**, you should demonstrate why reuse is not possible or appropriate.

Considerations for **archiving and making available** your data should be part of the application, even if subsequent use cannot be facilitated; reasoned discussion of this issue may be used as a quality criterion by reviewers.

For **self-generated data**, you should weigh the **reuse potential** of your data against the **provision effort**, select from the following three consequences, and justify:

- Archiving and, on request, user-friendly provision directly by the data producer for the purpose of checking published results (in the sense of good scientific practice).
- Archiving, extended documentation (codebook) and, on request, user-friendly provision directly by the data producers for the purpose of further scientific analyses.
- Well documented transfer to a research data centre for archiving and general provision to the scientific community according to the regulations of the respective research data centre.

Also consider **legal and ethical obstacles** (data protection, copyright, contracts).

<!-- TO DO: Replace with link to the institution's data protection officers -->
Consult with [Data Protection Officers](https://www.fau.de/fau/leitung-und-gremien/gremien-und-beauftragte/beauftragte/datenschutzbeauftragter/) and [ethics committee](https://www.dgfe.de/service/ethik-kommission) as applicable.

For **Informed Consent** about primary research and secondary use of the data, the expertise of the research data centres (RDC) or the local ethics committee can be consulted. 
For example, VerbundFDB provides [Informationen zur Informierten Einwilligung, zur Erklärung zum Datenschutz bei Surveys und zur Aufklärung von Kindern bei Studien](https://www.forschungsdaten-bildung.de/einwilligung?la=en).
Also note: RatSWD provides information on research ethics in the research process: [Sammlung Best-Practice Forschungsethik](https://www.konsortswd.de/ratswd/best-practice-forschungsethik/) [in German only].

If you are collecting **personal data**, you should provide information on whether the data will be anonymized/pseudonymized and at what point in the study.
You should also outline to what extent, if any, "Informed Consent" will be obtained: Analysis/use in the project or analysis/use in the project and reuse.
If "Informed Consent" is not obtained, provide reasons.

Indicate if and which non-personal **sensitive data** will be collected.

State whether any **copyrights or other proprietary rights** are obtained regarding your data. State the licenses under which reused data was originally published.

The DFG recommendations also refer to the guidelines on **data preparation** by [VerbundFDB](https://www.forschungsdaten-bildung.de/handreichungen?la=en).

RatSWD provides an overview of **[data centres](https://www.konsortswd.de/en/datacentres/all-datacentres/)** in the field of social sciences and related disciplines.
[VerbundFDB](https://www.forschungsdaten-bildung.de/daten-teilen?la=en) is a contact point for empirical educational research; DIPF, GESIS, IQB, Qualiservice and leibniz-psychology.org cooperate here.

Especially in the educational sciences, **ethical or legal barriers** often need to restrict access to data.
Some research data centres offer specific options to restrict access. See also the [Empfehlungen der DGfE, GEBF und GFD (section 3.4)](https://www.dgfe.de/fileadmin/OrdnerRedakteure/Stellungnahmen/2020.03_Forschungsdatenmanagement.pdf) [in German only].

If you **do not publish or archive** data, you should justify this, distinguishing between legal, ethical and voluntary restrictions. 
Examples of ethical aspects can be found in the [Empfehlungen der DGfE, GEBF und GFD (page 13)](https://www.dfg.de/resource/blob/174560/67a06609aa9aaa98e73b9b7d798afbb9/stellungnahme-forschungsdatenmanagement-data.pdf) [in German only].

Deciding against offering a **reuse option** does not absolve you from the responsibility of archiving the data appropriately.

You should use **subject-specific data and metadata formats** wherever possible.

Data published in journals or repositories should be accompanied by meaningful **documentation** in order to understand and reuse the data.
Self-developed or adapted **data collection instruments** should be made available, and previously published instruments should be cited.

Data sets that are unique or can only be reproduced with disproportionate effort should be archived **for the long term**.
This can usually be ensured via [Zenodo](https://zenodo.org/), [RADAR](https://www.radar-service.eu/radar/en/home) or the [VerbundFDB centres](https://www.forschungsdaten-bildung.de/daten-teilen?la=en).
FAU has a [Zenodo community](https://zenodo.org/communities/fau/?page=1&size=20) and provides [access to RADAR](https://ub.fau.de/en/research/data-and-software-in-research/archiving-and-publication-of-data/#collapse_0). <!-- TO DO: Adapt or delete -->
Note that only some select centres accept personal data.
If you have to archive non-anonymous data and are unsure which centres are suitable contact the local RDM support.  

Indicate the **personnel resources** for the preparation and detailed documentation of data made available for reuse.
Also indicate what **costs** arise **for archiving and making available** the data (data archive fees, publication fee, etc.).

### Sociology, Political Science and Communication Studies [*Soziologie, Politik- und Kommunikationswissenschaft*]

*Reference in German: [Stellungnahme des Fachkollegiums 111 „Sozialwissenschaften“ zum Forschungsdatenmanagement in der Soziologie, der Politikwissenschaft und der Kommunikationswissenschaft](https://www.dfg.de/resource/blob/174570/f311ff26e5a3c4b427271e0ba76a2ced/fachkollegium111-forschungsdatenmanagement-data.pdf) (2020).*

The **reproducibility** and analysis of the research results should be facilitated.

If possible, use subject-specific **metadata formats**.
For survey data, for example, this would be the [DDI metadata format](https://ddialliance.org/), for market, opinion and social research
[ISO 20252:2019](https://www.iso.org/standard/73671.html) and for statistical aggregate data [SDMX (Statistical Data and Metadata Exchange)](https://sdmx.org/) is in use.

The DFG expects the data to be available for at least 10 years.
The long-term **archiving** of data beyond 10 years is to be aimed for and the non-archiving of a data set must justified.

A **non-dissemination** of information regarding the data is possible, if making the data public would jeopardise the survey, or because vulnerable groups of people or groups moving on the edge of legality or beyond legality are being researched.
The grant application must reflect the reasons for non-dissemination and justify the procedure.

The use of certified **data repositories** or institutional repositories is recommended.
Subject-specific repositories are, for example, [Qualiservice](https://www.qualiservice.org/en/) for qualitative social science research data and
[SowiDaNet|datorium](https://data.gesis.org/sharing/#!Home) for quantitative data from the social and economic sciences.
For archiving, DFG **funding** can be applied for.

### Ancient Cultures (Egyptian and Near Eastern Antiquities, Classical Archaeology, Prehistory and Early History, Classical Philology, Ancient History) [*Alte Kulturen (Ägyptische und Vorderasiatische Altertumswissenschaften, Klassische Archäologie, Ur- und Frühgeschichte, Klassische Philologie, Alte Geschichte)*]

*Reference in German: [Fachkollegium „Alte Kulturen“ - Handreichung zum Umgang mit Forschungsdaten](https://www.dfg.de/resource/blob/174204/c5714c685b19ff59f7dd7322c24f27b1/handreichung-fachkollegium-101-forschungsdaten-data.pdf) (2020).*

*Please also note other DFG recommendations depending on your orientation, for example the recommendations for [Scientific Editions](#literary-studies-scientific-editions-literaturwissenschaften-wissenschaftliche-editionen) or [Social and Cultural Anthropology, Non-European Cultures, Jewish Studies and Religious Studies](#social-and-cultural-anthropology-non-european-cultures-jewish-studies-and-religious-studies-sozial--und-kulturanthropologie-außereuropäische-kulturen-judaistik-und-religionswissenschaften).*

The DFG also explicitly refers to the recommendations of [IANUS (Datenzentrum für Archäologie & Altertumswissenschaften)](https://www.ianus-fdz.de/it-empfehlungen/) [in German only].

For data-intensive projects, a **data management plan** is expected. The [RDMO Questionnaire](https://rdmo.ub.fau.de/) "DFG grants (Ancient Cultures, Review Board 101)" may be helpful here. <!-- TO DO: Possibly refer to an RDMO instance or another DMP tool. -->
The data management plan can be attached to the grant application, even if it is not part of the review.
In any case, you should mention the planned data management plan.

The handling of research data should be explained as concretely and bindingly as possible in the grant application, i.e. essential information on:

The **type and extent** of the data should be presented.
Indicate whether the data is purely analogue, purely digital or both and which formats are used at different research stages.
Data types could be, for example, 3D modelling, qualitative interviews (.wav, .mp4), transcription of old documents (TEI P5), recordings of artworks (.tif, .jpeg2000, .png).

A binding **legal clarification** is expected **in advance**: open questions and special challenges must be explicitly noted.
Documents relating to these legal issues, for example agreemets regarding **access** to holdings of museums, libraries and archives or permits addressing the accessibility of excavation sites, must be attached to the grant application.
Agreements may also concern data protection issues, copyright issues and image rights.
Appropriate permits should accompany the grant application.
Unresolved issues should be explicitly mentioned in the grant application. It is useful to give reasons why e.g. a permit is still pending and to indicate the time horizon until the outstanding permit is expected.
<!-- TO DO: Replace the contacts in question. -->
Contact the [University Library](https://ub.fau.de/en/research/data-and-software-in-research/) for copyright issues and the [Data Protection Officer](https://www.fau.de/fau/leitung-und-gremien/gremien-und-beauftragte/beauftragte/datenschutzbeauftragter/) **before** collecting personal data.

The **processing, quality assurance and documentation** of the data should be explained: the orientation towards **standards** and **best practices** should be explicitly mentioned.

Also note the following best practice manuals and guides for handling data: [Guidelines from the EU ARCHES project](https://www.europae-archaeologiae-consilium.org/eac-guidlines), [IT recommandations from IANUS](https://www.ianus-fdz.de/it-empfehlungen/) [in German only] and [Best Practices from the Archeology Data Service, UK](https://archaeologydataservice.ac.uk/help-guidance/guides-to-good-practice/).
If no subject-specific standards exist, this should be mentioned.
If possible, existing solutions such as database applications or digital tools should be adopted and adapted.
If own solutions are developed, it must be justified why no existing tool can be adapted or used directly.

If possible, standardised metadata formats or controlled vocabularies (for example GeoNames) should be used for **documentation**.
See also the IANUS help on [metadata](https://www.ianus-fdz.de/it-empfehlungen/?q=node/53) [in German only] and on [controlled vocabulary](https://ianus-fdz.de/it-empfehlungen/projektphasen/dokumentation/kontrollierte-vokabulare-thesauri-und-normdaten/) [in German only].
If there are no established standards for the data type, you should mention this.

**Data from studies that in principle cannot be repeated at all or only to a very limited extent** should be securely archived and prepared for later review or reuse.

You should address the planned **storage and archiving** of the data:
The DFG expects data to be available for at least 10 years.
In general, long-term archiving of valuable datasets beyond these 10 years should be aimed for and the non-archiving of a dataset should be motivated.
The use of certified data repositories or institutional repositories is recommended.
Disciplinary repositories are, for example, the [Archaeology Data Service](https://archaeologydataservice.ac.uk/), [arthistoricum](https://www.arthistoricum.net/en), [Arachne](https://arachne.dainst.org/) and [propylaeum](https://www.propylaeum.de/en/home).

You are generally expected to provide **long-term archiving** for the following data:

- Data on the documentation of archaeological excavations and surveys
- Data on material analyses of archaeological artefacts and samples
- Data from editing projects and material presentations (full text, photos, drawings, etc.)

More information on how to prepare data in an archivable form can be found in the [IT recommendations](https://ianus-fdz.de/it-empfehlungen/) [in German only] of the IANUS project, which is setting up an [archiving service](https://ianus-fdz.de/langzeitarchivierung/), and in the [nestor handbook](http://nestor.sub.uni-goettingen.de/handbuch/) [in German only].

**Reuse** of datasets should be made possible: Plans to make the data available and citable should be noted in the grant application.
If the data is made available via a project website and downstream database or upon personal request, clear **referenceability** should be ensured and presented in the grant application.
You should comment on how access will be ensured beyond the end of the project.
Complete and early **access** should be sought, at the latest in connection with the publication of research results.
If no access to the data for reuse is planned, this must be justified in the grant application.
Typical reasons are copyright or data protection issues, contractual barriers with the data-providing institution or endangerment of the research object through publicly accessible data.
An appropriate licence (for example CC BY-SA-NC) should regulate access and terms of use.

Designate, if possible by name, **responsible persons** for data management in the project and for data archiving and provision after the end of the project.

### Social and Cultural Anthropology, Non-European Cultures, Jewish Studies and Religious Studies [*Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaften*]

*Reference in German: [Handreichung des Fachkollegiums 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft zum Umgang mit Forschungsdaten](https://www.dfg.de/resource/blob/173710/045c16e9430426ef321c76dabcbaaf51/handreichung-fachkollegium-106-forschungsdaten-data.pdf) (2019).*

A **data management plan** is not mandatory, but can provide structured support for research data management in the planning and project phases. <!-- TO DO: Possibly refer to an RDMO instance or another DMP tool. --> The [RDMO Questionnaire](https://rdmo.ub.fau.de/) "DFG grants (sozial & cultural anthropology, Judaic studies, religious studies)", which is based on these recommendations, can help here.

The grant application should include the description of the **digital and analogue research data** and the planned handling of research data during and after the end of the project.

A **description of your data and contextual information** is expected in the grant application.
Describe the generated data types and data formats, for example transcription of old documents (TEI P5), recordings of artworks (.tif, .jpeg2000, .png), qualitative interviews (.wav, .mp4).
If possible, use standardised, non-proprietary and subject-specific formats to enable reuse and long-term archiving.

With regard to **metadata**, you should use (subject-specific) standards if possible.

**Data protection and ethical issues** should be stated and guidelines from specialist societies should be consulted.
[The RatSWD provides information on the topic of research ethics in the research process: [Sammlung Best-Practice Forschungsethik](https://www.konsortswd.de/ratswd/best-practice-forschungsethik/) [in German only].]

The **repositories** of physical sources of the the data or materials should be indicated, especially in the case of historical or philological grant applications.

If your data includes the edition, digital provision and visualisation of the material documents, the **permission of the rights holders** (archives, libraries, museums, private individuals, collections) must be clarified **in advance** and documented in the grant application.

If personal data are collected or processed, information on **Informed Consent Forms** should be provided in the grant application.
If an **Informed Consent** is waived, this should be justified: for example, due to time limits, limitation of accessibility, anonymisation in the collection process or afterwards.

The data should be **archived** in digital (in the repository) or analogue (in an archive) form for at least 10 years; ideally in a subject-specific or local institutional infrastructure.
Specify in the grant application where and in what form, the research data and materials will be stored.
If possible, use subject-specific, non-proprietary formats to allow long-term archiving and reuse.

Regarding **data availability**:
You should explain the selection of the data that are made availible, the steps needed to comply with copyright and data protection regulations, which licences for reuse are considered (for example CC licences), as well as research ethics considerations and access options (who  can reus the data under which conditions?).

<!-- TO DO: Replace the contact accordingly. -->
If you have any questions about the legal consequences of particular **licences**, please feel free to contact the [FAU University Library](https://ub.fau.de/en/research/data-and-software-in-research/).

Usually, there are **research ethics considerations** that influence the reusability of the data. In this case also note  the DFG's guidance on ["When do I need a statement by an ethics committee?"](https://www.dfg.de/en/research-funding/proposal-funding-process/faq/humanities-social-sciences).
The review board 106 emphasises that the **rights of researchers and persons involved in research** deserve special protection and must be weighed against the interests of reuse.
Also provide information on whether an **access restriction** procedure is used and whether a data access commission is required. Technical procedures that authenticate and authorise access can also be mentioned at this point.

### Economics and Social Sciences [*Wirtschaftswissenschaften und Sozialwissenschaften*]

*Reference in German: [Management von Forschungsdaten: Was erwartet das Fachkollegium 112 „Wirtschaftswissenschaften“ von Antragsstellenden?](https://www.dfg.de/resource/blob/173412/90ed15de9437140a14c771339979329b/fachkollegium112-forschungsdatenmanagement-de-data.pdf) (2018).*

*Reference in English: ["Economic Sciences" Review Board on the handling of research data](https://www.dfg.de/resource/blob/173898/ae74d0037f587da0587ecc9b408043f7/fachkollegium112-forschungsdatenmanagement-en-data.pdf) (2019)*

*Reference in German: [Stellungnahme des Fachkollegiums 111 „Sozialwissenschaften“ zum Forschungsdatenmanagement in der Soziologie, der Politikwissenschaften und der Kommunikationswissenschaften](https://www.dfg.de/resource/blob/174570/f311ff26e5a3c4b427271e0ba76a2ced/fachkollegium111-forschungsdatenmanagement-data.pdf) (2020).*

The recommendation for the economic sciences refers to the RatSWD Orientation Guides: [RatSWD Output Series](https://www.konsortswd.de/en/publications/ratswd-output-series/) and in particular [„Forschungsdatenmanagement in den Sozial-, Verhaltens- und Wirtschaftswissenschaften“](https://doi.org/10.17620/02671.7) [in German only].

A **data management plan** is not mandatory, but can provide structured support for research data management in the planning and project phases. <!-- TO DO: Possibly refer to an RDMO instance or another DMP tool. --> The [RDMO Questionnaire](https://rdmo.ub.fau.de/) "DFG grants (social/economic sciences)", which is based on these recommendations, can help here.

Following the **Open Science** idea, a statement on the handling of research data is expected in the grant application.
Research data should be archived as comprehensively as possible, documented and made publicly accessible for reuse.

Describe which **data types and data formats** are used for generated, processed or reused data sets.
Data types can be, for example, quantitative online survey (csv, SPSS files), 3D modelling, qualitative interviews (.wav, .mp4 ...).
If possible, you should use standardised, non-proprietary formats that are widely used in the community. Further criteria regarding long-term archiving can be found in the [nestor handbook](http://nestor.sub.uni-goettingen.de/handbuch/) [in German only].

If possible, you should use **subject-specific standards** for documentation and metadata.
For social science data, the [DDI metadata standards](https://ddialliance.org/) are particularly prevalent.

It is strongly recommended to classify the data regarding the six variants mentioned in the [Orientierungshilfen zum Forschungsdatenmanagement des RatSWD](https://doi.org/10.17620/02671.7) [in German only]. State which variant you want to follow and justify the decision.

You should describe which **instruments, software, technologies or procedures are used to generate or collect** the data.
You should provide information to generate and reproduce data that are not retained.
Software, procedures and technologies needed to use the data should also be documented.
If possible, use standardised, open and established formats to enable reuse.
The **reproducibility** of research results in publications should be ensured: by providing programmes for data preparation and analysis, their documentation as well as a repository for the data.

The **data storage solution** should be presented and justified with reference to the three variants from the [RatSWD Orientierungshilfen](https://doi.org/10.17620/02671.7) [in German only].
Describe how and where data (raw data, processed data, etc.) will be stored and backed up during the project.
Explain who will have access to the data, how often backups will be made and what data security measures will be taken (protection against unauthorised access, data recovery, transfer of sensitive data).

**Data protection, copyright and research ethics aspects** and protection of business secrets must be taken into account. <!-- TO DO: If applicable, insert the institution's subject-specific ethics committee. --> If applicable, the [ethics committee](https://www.wiso.rw.fau.de/forschung/forschungsoutput/ethikkommission/) should be consulted.
Take position on your processing of personal data (anonymisation/pseudonymisation, informed consent) and non-personal sensitive data (for example company/trade secrets).
Mention whether an ethics committee has reviewed the research project and state the result of the review.
[The RatSWD provides information on the topic of research ethics in the research process: [Sammlung Best-Practice Forschungsethik](https://www.konsortswd.de/en/topics/best-practices-research-ethics/).]

Note in the social sciences that there must be a discussion in the grant application as to whether/why **archiving**/ **sharing** data is possible when considering the impact on all groups of people concerned. The approach must be justified.
Data should be made available as early as possible.
For **reuse** an **embargo period** may be set and justified.
If data is not made public, please give reasons. Reasons may be legal, contractual or voluntary restrictions.

Data should be made available/archived for at least 10 years.
Ideally data should be archived in certified **subject-specific or institutional repositories**.
<!-- TO DO: Replace with the corresponding contact -->
Feel free to contact the [Research Data Management Unit of the FAU University Library](mailto:ub-fdm@fau.de).
You should also mention under which **licences** (for example CC BY or CC BY-SA) your data will be published.

Detailed questions on **documentation and metadata** to clarify the context of the data are particularly important in economics and social sciences.
If possible, standards, ontologies, classification should be used for data description to enable reuse.
The Review Board "Wirtschaftswissenschaften" [Economics] expects the data to be provided with a meaningful description.
Also, indicate if components of the data documentation will only be made available upon request.

The potential long-term value of the dataset should be discussed.
Especially for datasets that are unique or can only be reproduced with disproportionate effort, **long-term archiving** should be considered.  
Long-term archiving can be ensured for the next 20/25 years, for example, with data archives such as [Zenodo](https://zenodo.org/), [RADAR](https://www.radar-service.eu/radar/en/home) or [GESIS](https://www.gesis.org/home).
FAU has its own [Zenodo community](https://zenodo.org/communities/fau/?page=1&size=20) and provides [RADAR access](https://ub.fau.de/en/research/data-and-software-in-research/archiving-and-publication-of-data/#collapse_0). <!-- TO DO: Adapt or delete -->

A **pre-registration** of the research hypotheses to be investigated can take place and be mentioned in the grant application.

The **costs** for research data management can be applied for.

The **project completion report** should address the implementation of the research data management plans.

### Linguistics (data standards and tools in the collection of language corpora) [*Sprachwissenschaften (datentechnische Standards und Tools bei der Erhebung von Sprachkorpora)*]

*Reference in German: [Empfehlungen zu datentechnischen Standards und Tools bei der Erhebung von Sprachkorpora](https://www.dfg.de/resource/blob/171632/383249f00d425a643bd8c120404bbff6/standards-sprachkorpora-data.pdf) (2019).*

#### Oral corpora

A distinction is made between primary data, additional material (handouts, presentation slides, etc.), transcriptions, further annotations, metadata for documenting the recording situation and circumstances.
The **archiving** of the primary data, annotations and metadata must be ensured.

With regard to **recording**, the following recommendations are given: digital, best possible recording quality (no lossy compression = uncompressed data with the highest possible sampling rate instead of MP3 format), external microphone if possible for video recordings, storage in non-proprietary formats or conversion to open standard formats.

The immediate **transfer** from the recording device to a computer and the guarantee of a backup should be ensured.

The **raw data** should be kept during the project, even if transcription and analysis is done using primary data (cut and converted). The relationship of the primary data to the raw data should be documented.

You should take into account the specific recommendations on **recording devices** (flash mobile recorders with exchangeable memory cards) and **formats** (pp. 5-6):

- Audio recordings: uncompressed linear PCM format (WAV), min. 16bit/22kHz.
- Video recordings: MPEG2 or MPEG4/H.264 with high bit rates, ideally MJPEG2000 as uncompressed format.

**Additional materials** (for example, presentation slides, agendas, questionnaires, whiteboard notes, etc.) for specific communicative events should be collected, archived and made available to corpus users: Relevance should be considered at the start of the project.
Consent forms should also be prepared for supplementary materials, legal issues clarified if necessary, and formats for storage determined.
Metadata should also be created for supplementary materials.
The supplementary materials should be documented in the metadata for the associated data.

Specific tools and procedures are recommended for **transcription and annotation**:

- **Software tools**: [ANVIL](https://www.anvil-software.org/), [ELAN](https://archive.mpi.nl/tla/elan), [EXMARaLDA](https://exmaralda.org/en/), [FOLKER](https://exmaralda.org/de/folker-de/), [PHon](https://www.phon.ca/phon-manual/getting_started.html), [Praat](https://www.fon.hum.uva.nl/praat/)
- International **standards**, for example ISO24624:201 Language resource management for structured data. For further specific recommendations, see p. 8 of the [recommendations](https://www.dfg.de/resource/blob/171632/383249f00d425a643bd8c120404bbff6/standards-sprachkorpora-data.pdf).
- **Documented procedures** should be used, otherwise **project-specific conventions and schemes** should be introduced, if possible, as an extension, modification or simplification of existing procedures with appropriate documentation:
  - GAT/cGAT or CHAT for orthography-based transcription of spontaneous speech.
  - IPA for phonetic transcription
  - STTS, Tiger, SALSA, GRAID, ToBi for advanced annotations
  - Leipzig Glossing Rules for interlinear glossing
  - Advanced glossing for language documentation purposes (see p. 9)

The systematic collection and documentation of **metadata** in the production process is expected.
You should also consider metadata that are not of interest to the original research interest.
They could be relevant for reuse by reseachers who were not involved in the survey:

- For the general metadata, the following is expected: place and time of the recording, technique used, information on additional materials, sociobiographical and sociolinguistic information on the speakers, data protection status of the recording.
- The use of structured text formats (XML file or CSV file) is recommended.
- The metadata should meet the minimum requirements, for example the metadata standard of the [Dublin Core](https://dublincore.org) initiative or the [Open Language Archives Community (OLAC)](http://www.language-archives.org/).
- The description is ideally done with the help of the [CMDI Framework](https://www.clarin.eu/content/component-metadata) (Component MetaData Infrastructure) and the [CLARIN Component Registry](https://catalog.clarin.eu/ds/ComponentRegistry/#/). You should contact the CLARIN centre suitable for archiving during project planning, if possible, so that advice with regard to metadata can be given at an early stage.

The **archiving** should ideally be done in a specialised centre that is contacted during the project planning, at the latest at the beginning of the project: for example, in one of the [centres participating in the CLARIN project](https://www.clarin-d.net/en/preparation/find-a-clarin-centre).
For data from the school / education sector, the [Deutsche Institut für Internationale Pädagogische Forschung (DIPF)](https://www.dipf.de/en/frontpage?set_language=en) is a good choice.

#### Written corpora

##### Digitisation and corpus acquisition

The **text selection** should be presented: Bibliography or source list of resources to be digitised and, if applicable, integrated; consideration of legal restrictions and their documentation; documentation of the criteria for text selection (traceability, generalisability and representativeness).
You should weigh the corpus size, data quality and depth of indexing against each other and document the decisions transparently.

For the **primary data** applies:

- Information on the text source: original source, time of transfer, subsequent processing steps (curation and enrichment), if applicable, compliance with the legal framework conditions.

Depending on the text source format, there are different specifications and procedures to follow:

- A corpus based on existing **physical text sources** has the following minimum requirements: uncompressed TIFF files or JPEG2000 in lossless form, min. 300 dpi, colour scans.
- A corpus based on **digital text data** (faithful full texts or critical editions): search for and provision of the corresponding underlying image files, deviations should be justified; special care regarding the reliability of the transcription compared to the original.
- A corpus from the **reuse of 'born digital' texts**: The web texts should be compiled, for example by web crawling for quality assurance and replicability, as the changeability of the source data must be factored in. The criteria must be defined and documented beforehand. The crawling methods and algorithms should be documented, ideally the crawler is made available.

The **text recognition** can be done by manual transcription or automatically via Optical Character Recognition (OCR) with manual or semi-automatic verification or correction.
You should mention and justify the chosen method.
The metadata of each text resource should include documentation of the process.
The text recognition should be as accurate as possible and the transcription guidelines should be documented.

The **annotation** (manual, automatic or semi-automatic) should take into account structural features, linguistic properties and linguistic specificities.
The appropriate scope and depth should be justifiably documented.
The tagsets, structuring depth, procedures and tools used in the case of automatic annotation and the expected annotation accuracy should be documented.
For concrete recommendations for structural annotation (formats, tag sets, tools), see pp. 23-24 of the recommendations.

**Quality assurance** includes text quality, the quality of structural annotation, linguistic annotation and metadata. The required measures are documented.

##### Standards and tools

The XML format is established as **standard**.
For structural mark-up, the TEI P5 rule set has become established, which can be restricted and adapted to specific projects with the help of the ODD formalism.
A project-specific format must be justified.
For linguistic annotation, you should use as far as possible specialised annotation tools that allow a standardised output format. For linguistic annotation, specialised annotation tools that allow a standardised output format should be used as far as possible.
Adaptations of tools or tag sets and the required effort should be outlined in the project proposal.

If **tools** are **developed** for under-researched topics, the implementation, documentation and availability for reuse (by publishing the source code for example) should follow required standards.

For **analysis tools** with standardised output formats, the various data modelling, query and analysis options should be discussed in an interdisciplinary manner depending on the database paradigms.

The **metadata** should contain the following information: Information on the text source, the digital edition, the project background, the scope of the data, the annotation depth, the guidelines for transcription or text compilation and annotation, information on content, information on conditions of use, on legal and technical availability, information on correct citation.
The metadata should be created in the following formats: [TEI header](https://tei-c.org/release/doc/tei-p5-doc/de/html/ref-teiHeader.html), [METS](http://www.loc.gov/standards/mets/)/[MODS](http://www.loc.gov/standards/mets/), [CMDI](https://www.clarin.eu/content/component-metadata), [EAD](https://www.loc.gov/ead/).

**Reuse**: You should present the possibilities and limitations of later reuse in other project contexts and for other questions in the project proposal.
Cooperation with a specialised centre or institution is recommended.
The FAIR principles should be considered for the data and metadata.
The legal aspects of reuse should be clarified in the **preliminary** stages of the grant application.
For specific points, see p. 28 of the recommendations.

### Linguistics (legal aspects of handling language corpora) [Sprachwissenschaften (rechtliche Aspekte bei der Handhabung von Sprachkorpora)]

**Disclaimer: The document cited below has been withdrawn by the DFG (as of October 2022). The information is likely outdated due to changes in German copyright law and the GDPR. Updated recommendations are not yet known. (Status: 12 January 2023)**

*Reference in German: [Handreichung: Informationen zu rechtlichen Aspekten bei der Handhabung von Sprachkorpora](https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/informationen_fachwissenschaften/geisteswissenschaften/standards_recht.pdf) (2013).*

<!-- TO DO: If applicable enter a contact for legal questions in the area of research data management otherwise delete this paragraph. -->
The representative for legal issues in the area of research data management at FAU, Ms Petra Heermann (<petra.heermann@fau.de>), can be contacted for specific questions or in case of uncertainties.

#### Oral corpora

**Data protection declarations, consent declarations and anonymisation/pseudonymisation**, for details see p. 6ff:
Observe the federal data protection laws and state data protection laws.
Handling personal data includes the collection, storage, processing and publication of the data concerned.
You must obtain the consent of the person concerned for the processing of personal data.

Data protection aspects of data handling and processing should be coordinated with the data protection officer.
<!-- TO DO: Replace with contact to the institution's data protection officer. -->
Contact the [FAU Data Protection Officer](https://www.fau.de/fau/leitung-und-gremien/gremien-und-beauftragte/beauftragte/datenschutzbeauftragter/) at an early stage.

Data concerned are at least audio and video recordings, transcriptions and metadata recorded on speakers and communication situations.

In order to comply with data protection regulations, you should provide for a **data protection statement** and obtain **informed consent statements** from the persons concerned.
The data should be **anonymised** and/or **pseudonymised** in an appropriate manner.

Details on the **consent forms** and the required contents can be found on pp. 6-8 of the recommendations.

Details on **anonymisation and pseudonymisation** depending on the type of data can be found on pp. 8-9 of the recommendations.
The method of anonymisation and pseudonymisation should be noted in the consent forms.

Details on **copyright aspects** can be found on pp. 10-11 of the recommendations.
Oral corpora to which copyright and neighbouring rights apply are, for example, audio or video recordings from radio and television, from the internet, written materials as supplementary material to an oral corpus, as well as photos and graphics produced by actors in the research field.

#### Written corpora

**Copyright** applies to texts of a certain level of creation (in German *Schöpfungshöhe*) and whose longest-lived author has not been dead for at least 70 years.
Further protective rights apply to press content (1 year) if it is made public, to scientific editions as well as to posthumous works (25 years).
Protective rights also apply to the structure of databases (15 years).

The relevant regulations of the Copyright Act mainly concern the right of reproduction (§ 16), the right of distribution (§ 17), the right of making available to the public (§ 19a), the neighbouring rights of scientific editions (§ 70) and of posthumous works (§ 71), the database producer's neighbouring rights protection (§ 87b) and the press publishers' neighbouring rights protection (§ 87f).
Text and data mining and quantitative linguistic analysis have not yet been legally clarified. [Note: Again this is outdated, as text mining **is** covered by the  current Copyright Act.]

Details on copyright aspects can be found on pp. 12-13 of the recommendations.

Note the restriction rule and its application to text corpora (pp. 13-14).
For the **construction of a corpus**, either the permission of a rights holder must be obtained or only texts that are fundamentally not protected by copyright, to which the copyright has expired or which do not reach a sufficient level of creation may be used.
For complete rights clearance, cooperation with centres that can track the legal status and keep the data permanently available is recommended.

Take into account the **rules for edits and rearrangements** of works (see pp. 15-16):
Research results such as statistics or annotations from **text and data mining** are not edits to the source text.
However, text and data mining may still be contractually prohibited.
Nor does it solve the problem that a protected source text must be cached for processing and this too may be contractually prohibited.

Note the regulations for **collective works and database values** both for the inclusion of parts of a database in your data and for making your data available (see p. 17).

Be aware of the regulations for **orphan works**, which allow public access and reproduction provided the works are not edited or transformed (see p. 18).

Observe the terms of use of commercial **software** and of software tools developed in an academic context that may be affected by third-party rights.
If you develop software yourself and publish it, you should clarify the licence to be granted with your employing institution.

Note that **data protection law** may also affect the compilation and making available of written texts (for example, chat transcripts).
A **pseudonymisation/anonymisation** may be useful.
As with oral corpora, an **informed consent** should be obtained.

The document gives some broad recommendations:

* If in doubt, **licences and consents** should be obtained.
Licences should be obtained as early as possible in the planning phase so that any licence fees can be calculated into the project costs.

* In the planning phase, contact should be made with a **centre** experienced in licensing your type of resource in order to incorporate the data basis and project results into the curation processes and archiving processes.

* The handout refers to the [CLARIN-D Legal Helpdesk](https://www.clarin-d.net/en/training-and-helpdesk/legal-helpdesk) for recommendations on draft licence agreements.
If the corpus cannot be made permanently available for legal reasons, the reasons should be documented and **compromise strategies** sought. This can be **documentation** on how subsequent users can obtain the rights themselves.

* Data protection concerns should be included in the planning phase and an explicit document on the **data protection concept** should be created.

* The handout also offers recommendations on making text corpora available to a restricted group of users and advice on end-user licensing agreements (see p. 20).

* **Self-created works** should be provided with a **licence statement**: CC BY or CC BY-SA or a GNU licence for software.
If possible, scientific works should be given a CC BY licence and pure data a CC0 licence.

* If **software** is used to produce derivative works and the licence is not known, any restrictions on its use should be identified.
For the use of commercial annotation tools, supplementary agreements should be made for the software output.
In general, it should be clarified what provisions apply to the continued use of the software outputs after the licence expires.

### Literary studies (scientific editions) [*Literaturwissenschaften (wissenschaftliche Editionen)*]

*Reference in German: [Förderkriterien für wissenschaftliche Editionen in der Literaturwissenschaft](https://www.dfg.de/resource/blob/172080/895fcc3cb72ec6cd1d5dc84292fb2758/foerderkriterien-editionen-literaturwissenschaft-data.pdf) (2015).*

Possibly, the recommendations of the DFG Review Board 104 "Sprachwissenschaften" on [data technology standards and tools in the collection of language corpora](https://www.dfg.de/resource/blob/172080/895fcc3cb72ec6cd1d5dc84292fb2758/foerderkriterien-editionen-literaturwissenschaft-data.pdf) and the 2013 document with information on legal aspects in the handling of language corpora" (withdrawn in 2022 due to changes in copyright and data protection regulations) are also of interest.

Please note that for editions in literary studies, the funding guidelines include some general requirements (form of publication, structure, etc.) in addition to regulations on data management.

A **data management plan** is not mandatory, but can provide structured support for research data management in the planning and project phases. <!-- TO DO: Possibly refer to an RDMO instance or another DMP tool. --> The [RDMO Questionnaire](https://rdmo.ub.fau.de/) "DFG grants (scientific editions in literary studies)", which is based on these recommendations, can help here.

The **data management of the text data are part of the eligibility criteria** for all applications.

Describe the generated **data types and data formats**. If possible, you should use standardised, non-proprietary formats that are generally or widely used in the community in order to enable reuse and long-term archiving.
See also detailed explanations in the [nestor handbook](http://nestor.sub.uni-goettingen.de/handbuch/) [German only].

For print editions, as well, the **textual basis** should be made available in standardised digital form, in the so-called **basic format** (German *Basisformat* - more on this later).

You should adhere to established **standards** of edition philology, formulate the underlying edition guidelines and explain and methodically justify the technical implementation.
The technical standards of digital text preparation (text mark-up, structure of online presentation, search and navigation options in the database) must be explained.
You should mention deviations and further developments of standards, methods and techniques.

Your application should include details of the **date and form of publication** (print-only, hybrid, digital) of your edition.
Even if your edition is published in print, it is expected to be backed up and **provided in digital form**.
Your working methods for **text production** should correspond to the *state-of-the-art* in terms of software use, work processes and quality assurance.

Cooperation with suitable institutions such as libraries, archives, specialised research centres is recommended for both text production and data archiving.

Questions regarding publication, such as **copyright, data protection, neighbouring rights and, if applicable, personal rights** must be clarified **in advance**, unresolved aspects must be named in the proposal and evaluated with regard to the impact on the publication of the edition.
Since measures to clarify the legal situation should be carried out before the proposal is submitted, these cannot be financed by DFG funds.

An online publication should ideally be **open access** ("golden road"), if necessary with a maximum waiting period of 24 months to allow for publishers' interests.
After this period, a **secondary publication** on servers of public information or research institutions ("green road") should be made possible.  
If the publication is not open access, this must be justified.

For the **bibliographic metadata**, standards must be adhered to and the findability of the online publication via common library reference systems must be guaranteed.
If the edition is published by a publisher or in a dedicated platform for editions, the metadata will normally be made available in common formats such as MARC21 anyway.

Common **interfaces** for a broad data exchange must be implemented.

The digital **long-term archiving** of the digital edition should be ensured via integration into certified repositories, archives or national libraries.
Funding for this cannot be applied for.

The underlying **text data** should always be made available for subsequent use in a **"basic format"** by storing it in a subject-recognised repository.
You must explicitly explain and justify deviations from the basic format.
This must be explained under point 2.4 in the grant application.

The **basic format** is defined as follows:
The text is stored in **Unicode**, encoded in **XML-based markup** (current version of the TEI model, for example [TEI Simple](http://www.tei-c.org/Guidelines/) or [DTABf](http://www.deutschestextarchiv.de/doku/basisformat)).
If you use a character encoding other than Unicode, justify why this is necessary for your project.
Regarding the structural and outline elements of the text in the markup, the eligibility criteria refer to the TextGrid collection [Baseline Encodings](https://wiki.de.dariah.eu/display/TextGrid/Search+Index+and+Baseline+Encoding) for guidance.
The decision for a specific representation of the structural elements must be justified.
Project-specific XML elements and attribute-value pairs shall be documented in the TEI header.
If this is not the case, justify why you don't do it.

A possible long-term **backup of the full text presentation** should be ensured by a suitable formatting language (XSLT, CSS).

If the archived data does not correspond to the final edition, justify why this is the case.

The basic format should be understood as a **minimum standard** that can be achieved with technical means and a reasonable amount of training and work.
Missing expertise should be compensated for by cooperations, for example with libraries, archives, data centres, etc. This can also mean a more efficient division of labour and cost savings.

The **funds** for the preparation in the basic format can be applied for, whereby offers from external service providers are to be used as a comparison.
The text data prepared in the basic format should be made available for subsequent use no later than **24 months** after the actual publication.
If the data is only made available after the expiry of an embargo period, please justify this.

In addition to the text data in the basic format, **other data** can also be created in edition projects, for example digital copies of manuscripts, which you can make available to other researchers.

The data should be archived in a certified institutional or professional **repository** that guarantees long-term availability and citeability through **persistent identifiers** (for example DOIs).
The confirmation of the institution should accompany the application.
Archiving must be carried out under a **licence** (e.g. CC BY or CC BY-SA) that allows free reuse for scientific purposes.

## Life sciences [*Lebenswissenschaften*]

### Biodiversity research [*Biodiversitätsforschung*]

*Reference in German: [Richtlinien zum Umgang mit Forschungsdaten in der Biodiversitätsforschung](https://www.dfg.de/resource/blob/171716/cad14d794c39d50da731d681a56eb651/richtlinien-forschungsdaten-biodiversitaetsforschung-data.pdf) (2015).*

Your research data should be publicly **accessible** and free of charge.
Restrictions for legal, copyright or ethical reasons may be stated.

It is expected that data quality will be ensured and that the data will be sufficiently documented to enable their reuse.

Your project proposal should be accompanied by a **data management plan**.
See point 3 of the guidelines for the aspects that should be addressed in the data management plan.

For **collaborative projects**, a specific **data guideline** is recommended to regulate the intra-project provision, sharing and reuse of data.

### Medicine and biomedicine [*Medizin und Biomedizin*]

*Reference : [Guidelines for quality-promoting aspects in medicine and biomedicine](https://wissenschaftliche-integritaet.de/en/comments/guidelines-for-quality-assurance-measures-in-medicine-and-biomedicine/) (2021).*

Where appropriate, the guidelines refer to the [Guidelines on Animal Experimentation in Research](https://www.dfg.de/resource/blob/309210/f6c41e7837a3936896dad950a83c0e32/handreichung-sk-tierversuche-data.pdf) (2019).

The application should justify the choice of a model system, a data source, a theoretical approach in relation to the research question and taking particular account of the **legal and ethical framework**.

The **suitability** of the research data should be substantiated, if possible.

If you require **technical, methodological or organisational research infrastructures** for implementation and quality assurance, you should state this in the application.
If you make use of existing competences, structures, offers on site, you should mention this.
**Existing data sets and biosamples** should be used where possible.

You should address in your application the **type of study, statistical design, use, analysis and provision** of the data.
Is it an exploratory or confirmatory study?
Would a replication study be useful?
Is registration of the study required?
Are there sources of biases in your statistical planning and how do you deal with them?

Your datasets and biospecimens should be deposited in **certified biobanks, collections or research data repositories** (for example, provided or recommended by the relevant [NFDI consortia](https://www.nfdi.de/consortia/?lang=en)).
PUBLISSO offers a detailed [overview of life sciences repositories](https://www.publisso.de/en/publishing/repositories) that may be of interest.
If there are ethical or legal reasons not to publish, mention this.
Take into account **funds** for the use of the infrastructures.

You should discuss possible **biases** based on the research question, planning, implementation and analysis of the data and explain your handling of them.

Address research contributions that have a qualitative added value for your research question: systematic review, replication study, participation in standardisation...

Specifically name the responsible party for data management in the project.

## Natural sciences [*Naturwissenschaften*]

See also [life sciences](#life-sciences-lebenswissenschaften) for biology.


### Mathematics

*Reference: [Review Board "Mathematik" on Handling Research Data in mathematics](https://www.dfg.de/resource/blob/176136/42e4fda0ee48534306f2c603780beeae/handreichung-fachkollegium-mathematik-forschungsdaten-data.pdf) (2023) [in German]*.

Despite the diversity of research data in the various mathematical disciplines, standardized processes have been established in many areas.
The existing **best practices** of the international subject communities and NFDI consortia should therefore be followed.
In particular, the **MaRDI** consortium provides information on use cases and offers tutorials on the subject-specific handling of data (https://www.mardi4nfdi.de/resources/publications).

The recommendations refer to the **FAIR principles** and the [DFG Checklist for Handling Research Data](https://www.dfg.de/resource/blob/174732/3c6343eed2054edc0d184edff9786044/forschungsdaten-checkliste-de-data.pdf).

The application should describe whether **costs** will be incurred by making the data accessible, reusable, or transferable to a public repository.
These can be requested. See [requestable funds](https://www.dfg.de/en/basics-topics/basics-and-principles-of-funding/research-data/resources-available).

The in-depth notes and regulations are based on the structure of the DFG checklist:

**Description of the data**.

At a minimum, the proposal should include **details on the type and amount** of research data that will be generated and the software that will be used.
You should use **file formats** established in subject communities and briefly describe existing structures and file formats. If possible, software should be created in the form of packages for (open-source) systems recognized in the community.

**Documentation and data quality**

Important points concerning **data preparation, quality assurance and documentation** should be mentioned. The recommendations here refer in particular to [Guideline 12 - Documentation](https://wissenschaftliche-integritaet.de/en/code-of-conduct/documentation/). 

A transparent documentation of the data can also include an orderly filing system,
for example project- or publication-related, can also contribute to a transparent documentation of the data. Data should be provided with **unique identifiers** to facilitate their assignment and subsequent use. 

Applicants are responsible for ensuring that project staff collecting data receive **training and extensive briefings on research data management**.
The application should outline what form this will take: Induction sessions, shift training, induction by experienced collaborators, etc.
In collaborations, this can be done organizationally, for example in integrated graduate schools or centralized projects.

Applicants are responsible for **data quality** (defined depending on the project) and the subsequent selection of data for archiving and long-term accessibility.

**Storage and technical backup during the course of the project**.

The **storage** of the data should be explained. The existing specifications of the respective research institution should be mentioned.
Archiving for at least 10 years should be guaranteed according to DFG requirements. See [Guideline 17 - Archiving](https://wissenschaftliche-integritaet.de/en/code-of-conduct/documentation/), which is referenced.

Review boards are aware that **not all data can be retained**, but deviations from community standards should be explicitly stated in the proposal.

**Legal obligations and framework conditions**. 

Legal obligations and framework conditions should be checked in advance and briefly named in the application. The legal obligations relate to the recording, documentation, storage, archiving and subsequent use of the data generated or used. Where third parties have rights to the data, any resulting restrictions should be documented.

**Data exchange and permanent accessibility of data**.

The application should explain why which data are **worthy of archiving** (judicious selection).
**Data underlying publications** should be archived permanently.
Archiving also includes **accessibility** of data, if possible.

For archiving, **certified repositories and data centers** are recommended, here please also consider the recommendations of the relevant NFDI.
Appropriate institutions should be contacted prior to the start of the project in order to be able to consider **time and financial needs**.
In order to enable later re-use, **archival-capable, standardized data formats** should be chosen. The repository [Zenodo](https://www.zenodo.org) is mentioned as a reasonable solution with inventory guarantee.

If **data format conversion solutions** are developed in the project, they should be made available for reuse.

**Responsibility and Resources**.

The application should outline who will be responsible for data management during the course of the project.
The **responsibility for data management and quality control** lies with the project leaders, but can also lie with a used infrastructure in the context of a collaboration.
This would have to be explicitly mentioned in the application.


### Physics [*Physik*]

*Reference in German: [Fachkollegien "Physik" zum Umgang mit Forschungsdaten im Fachbereich Physik](https://www.dfg.de/resource/blob/175982/abe8f1e92c9ff1243961bfc3b01e8d83/handreichung-forschungsdaten-physik-data.pdf) (2022).*

Despite the diversity of research data in the different physical disciplines, standardised processes have been established in many areas.
The existing **best practices** of the international subject communities and the NFDI consortia ([FAIRmat](https://www.fairmat-nfdi.eu/fairmat), [PUNCH4NFDI](https://www.punch4nfdi.de/), [DAPHNE4NFDI](https://www.daphne4nfdi.de/index.php)) should therefore be observed.

The recommendations refer to the **FAIR Principles** and the [DFG Checklist for Handling of research data](https://www.dfg.de/resource/blob/174732/3c6343eed2054edc0d184edff9786044/forschungsdaten-checkliste-de-data.pdf).

The grant application should state what **expenses** incur by making the data accessible, reusable or transferring it to a public repository.
These can be applied for. See [available resources](https://www.dfg.de/en/basics-topics/basics-and-principles-of-funding/research-data/resources-available).
The grant application should at least **summarise the nature and extent** of the research data to be generated.

The **international System of Units (SI)** should be used or the possibility of conversion to SI units should be provided to ensure interoperable measurement data.

You should use **file formats** established in subject communities and briefly describe the existing structures and file formats. As an example, the recommandations mention the Flexible Image Transport System (FITS) in astronomy.

Important points on **data preparation, quality assurance and documentation** should be mentioned. The recommendations here refer in particular to [Guideline 12 - Documentation](https://wissenschaftliche-integritaet.de/en/code-of-conduct/documentation/).

Even though there are not yet **electronic laboratory journals** suitable for all areas of physics, their use is recommended wherever possible.
Apart from archiving, the digitisation of handwritten laboratory notebooks is also considered a useful addition.

Data should be provided with **unique identifiers** to facilitate their assignment and subsequent use.
Thus, for experimental work, samples and measurements should be given an identifier.

The **data storage** should be project- or publication-related.

Applicants are responsible for ensuring that project staff collecting data receive **training on research data management**.
The application should specify what form this would take: introductory sessions, shift training, briefing by experienced collaborators, etc.
In collaborations, this can take place organisationally, for example, in integrated doctoral schools or central projects.

Applicants are responsible for **data quality** (defined depending on the project) and the subsequent selection of data for archiving and long-term accessibility.

The **storage** of the data should be outlined. The existing specifications of the respective research institution should be mentioned.
Archiving for at least 10 years must be guaranteed in accordance with DFG guidelines. See [Guideline 17 - Archiving](https://wissenschaftliche-integritaet.de/en/code-of-conduct/archiving/), to which reference is made.

The review boards are aware that **not all data can be preserved** (e.g. pre-selection of raw data for large particle accelerators), but deviations from community standards should be explicitly stated in the proposal.

**Legal obligations and framework conditions** should be checked in advance and briefly named in the application.

Should copyright-protected data be subject to an **embargo** to allow exclusive use, the date of its planned publication should be indicated in the application.

The application should explain why which data are **worth archiving**.
In addition to **data on which publications are based**, **other project-related measurements or data from measurement series** that are not accessible via a publication should also be archived permanently.
If possible, archiving also includes **accessibility** of the data.

For archiving, **certified repositories and data centres** are recommended; please also observe the recommendations of the relevant NFDI here.
Contact should be made with suitable institutions before the start of the project in order to be able to take **time and financial requirements into account**.
In order to enable reuse, **archivable, standardised data formats** should be selected.
If **solutions for the conversion of data formats** are developed in the project, these should be made available for subsequent use.

The proposal should state who is responsible for data management during the course of the project.
The **responsibility for data management and quality control** lies with the project leaders, but in the context of a cooperation it can also lie with an infrastructure unit.
This should be explicitly mentioned in the application.

### Chemistry [*Chemie*]

*Reference in German: [Handlungsempfehlung zum Umgang mit Forschungsdaten](https://www.dfg.de/resource/blob/175558/e4310513bf5e2d3bc3a8bfd611c3989c/handreichung-fachkollegien-chemie-forschungsdaten-data.pdf) (2021).*

A **data management plan** is not mandatory, but can provide structured support for research data management in the planning and project phase.
The [RDMO questionnaire](https://rdmo.ub.fau.de/) "DFG grants (chemistry)", which is based on these recommendations, can help here. <!-- TO DO: Possibly refer to an RDMO instance or another DMP tool. -->

The recommendation refers to the **NFDI consortia** and their infrastructures under construction, which will be useful for projects funded by individual research grants and consortia.
For chemistry, these are in particular the consortia [NFDI4Chem](https://www.nfdi4chem.de/), [NFDI4Cat](https://nfdi4cat.org/) and [FAIRmat](https://www.fairmat-nfdi.eu/fairmat/).

You should address the following points in the grant application:

The **data description** should include type (purely analogue, purely digital or both), origin (generated, reused) and scope of the data.
The scope should be mentioned at least in summary terms (pure volume, for example in GB, or number of expected files/measurements), as this allows a better assessment of costs and effort for data management, if applicable.
Also, address different data formats, for example for raw data, processed data or for long-term archiving.
Data formats can be for example:

- 3D model from cryo-EM measurement (MRC, MRCS, TIFF...)
- Software code
- Data from a study with Raman spectroscopy (CSV, SPC...)
- crystallographic structural data (CIF)

The **data processing, quality assurance and documentation** should be briefly explained.
See also the [Comment on Documentation of research results in experimental chemistry](https://wissenschaftliche-integritaet.de/en/comments/dokumentation-von-forschungsergebnissen-in-der-experimentellen-chemie/) as a suggestion for the procedure.

**Electronic lab notebooks** are considered to have a promising future, but there is not yet a suitable solution for all areas of chemistry.
Widely used examples are [Chemotion](https://www.chemotion.net/), [elabFTW](https://www.elabftw.net/) and [openBIS](https://openbis.ch/).
See also the [Comment on Electronic laboratory journal and repository in chemistry](https://wissenschaftliche-integritaet.de/en/comments/electronic-laboratory-journal-and-repository-in-chemistry/).
If **handwritten laboratory notebooks** are used, they should be digitised and archived.

Samples and measurements should have a unique **identifier** to enable attribution and to guarantee traceability of the chronological measurement sequence, the total number of measurements, the non-consideration of measurement data and the possibility of later use.

Different data types should be described with standardised **metadata formats** (for example CML or CHMINF).

You should describe the individual steps of **data preparation** and address the data formats.
If a custom tool is designed or programmed for data preparation, the grant application must justify why an existing tool cannot be adapted or used directly.

The **data storage** should be project- or publication-related and documented.

The **data quality** must be defined and documented depending on the project. This includes, for example, measures that ensure **data integrity** and that are already carried out during **data generation**.
Measures to ensure data integrity include checksums, the appointment of data stewards and automatic backups.
Data generation measures include standardised protocols for calibration of a measurement procedure, independent replication of all measurements/control samples and blinded analyses.

Project staff should receive **instruction in research data management**.
In collaborations, this can take place organisationally, for example in integrated doctoral schools or central projects.

**Storage and backup** should include backup, access protections and rights as well as organization system for the data.
You should observe and mention the existing regulations / guidlines of the institution / university / collaboration.
In general, the archiving of analogue and digital data must be guaranteed for 10 years.

The **legal obligations and framework conditions** should be **checked in advance** and named in the application: Data protection regulations when working with personal data, copyright law, for example when reusing a protected software code, contractual obligations when cooperating with (commercial) partners, patent law, ...

With regard to **data exchange and permanent accessibility of data**, a justification of why which data should be archived is expected.
All relevant and reproducible data on which publications are based should be archived.
This also applies if they are not yet specifically accessible and reusable via a publication.
Data based on established methods and measurements with commercially available equipment should be handled and archived as standard.
For data whose archiving and accessibility is not possible or practicable, you should submit an explanation.

Digital data should be archived in **certified repositories/data centres**:
The use of the suitable, subject-specific services of the NFDI ([NFDI4Chem](https://www.nfdi4chem.de/), [NFDI4Cat](https://nfdi4cat.org/), [FAIRmat](https://www.fairmat-nfdi.eu/fairmat/)) is generally recommended.
See also the [Comment on the Use of chemistry-specific repositories](https://wissenschaftliche-integritaet.de/en/comments/use-of-chemistry-specific-repositories/).
You should check with the repositories to determine suitability before submitting the grant application.
This also allows estimating the costs for making the data accessible and apply for these costs in the proposal.
For all chemical disciplines without a specific repository, NFDI4Chem offers the repository [RADAR4Chem](https://radar.products.fiz-karlsruhe.de/en/radarabout/radar4chem) free of charge.

Archivable, standardised, non-proprietary **data formats** should be used, with redundant storage of all original raw data.
Further criteria as well as explanations can be found, for example, in the [nestor handbook](http://nestor.sub.uni-goettingen.de/handbuch/) [in German only].
If software solutions for the practicable **conversion of data formats** are developed, these should be made available to the scientific community.
This would support a general standardisation.

The **responsibilities and accountabilities** for research data management in the course of the project should be clearly identified.
The responsibility for data management and final quality control lies with the project leaders.
The workload for making data FAIR should be taken into account in order to apply for project-specific **funds**.

Applying for data management costs is possible.
This includes the costs for storage space in a data archive, personnel costs, for example 3 person-months for a computer scientist.
Storage costs for data publication should be applied for under the heading ["Other costs"](https://www.dfg.de/en/research_funding/principles_dfg_funding/research_data/resources_available/index.html) for the use of established infrastructures.
It must be ensured that these services are distinguished from the basic infrastructure services of the university / institute which could not be funded via the DFG.

## Engineering [*Ingenieurwissenschaften*]

### Materials science and materials engineering [*Materialwissenschaft und Werkstofftechnik*]

*Reference in German: [Guidelines for dealing with
Research Data : Materials Science and Engineering Expert Forum](https://www.dfg.de/resource/blob/173258/64ce3b7ee138953d13d80530d891bfa3/fk-materialwissenschaft-werkstofftechnik-2023-data.pdf) (2023). [in German only]

The recommendations refer directly to the six topic areas of the [DFG Checklist](https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/forschungsdaten/forschungsdaten_checkliste_de.pdf). In addition to basic explanations, **core requirements** are presented in each case that **must** be met by proposals.

These core requirements are only briefly summarized here:

1. **Description of data**: It must be outlined which data types are generated in which quantity and according to which standards the different data types are stored. At the very least, the type and format of the data generated or used in the project must be described in the application and the volume estimated.
2. **Documentation and Data Quality**: Applicants must outline the essential steps of the documentation. For experimental data, explanations of measurement protocols are a central part of the documentation. (See original document for details - especially also on the use of ELN and the digitization of analogue data).
The *data quality* (among others criteria of statistical significance and consistency of the data) musst be discussed in relation to the project specific work program.
3. **Storage and technical backup during the course of the project**: Redundant data storage is expected and must be mentioned in the proposal with key steps on how storage will occur. Key components: Access protection, user management, back-up, file server and share point solutions (esp. for collaborative projects).
4. **Legal obligations and framework conditions**: Legal obligations and framework conditions (patents, export bans in international collaborations, dual use, (subsequent) use rights, data protection, ...) must be clarified in advance and named in the application. 
5. **Data exchange and permanent accessibility of data**: The systematics and basic criteria of data exchange must be presented. In the explanations on the handling of research data, it must be described which data are considered to be  worthy of archiving and reusable. It must also be discussed if data cannot be released for general re-use or if release is made individually according to defined criteria. If data is subject to an embargo period, this must be presented with an indication of the length of time.
All relevant and reproducible data on which publications are based should be archived or published in a publicly accessible form, in addition to the data that is already accessible via publications.
6. **Responsibility and Resources**: The proposal must clearly state who has primary responsibility for which individual steps of research data management during the course of the project. 
The long-term financing of infrastructure for handling research data must be ensured and described in the application for the respective research project.
