# RDM Support at FAU

<!-- Table of contents for the GitLab version, must be deleted/commented out for a PDF version -->
[[_TOC_]]

## General support during the application process

General information on funding, advice and application at FAU: [https://www.fau.eu/research/services-for-researchers/funding-advice-and-submitting-applications/](https://www.fau.eu/research/services-for-researchers/funding-advice-and-submitting-applications/).

### Individual grants

Contact person for ERC grants: [Dr. Sabine Eber](https://www.fau.de/person/sabine-eber/) (Presidential Staff).

Contact persons for DFG grant applications:
[https://www.fau.eu/research/services-for-researchers/funding-advice-and-submitting-applications/project-proposals-and-project-management/german-research-foundation/](https://www.fau.eu/research/services-for-researchers/funding-advice-and-submitting-applications/project-proposals-and-project-management/german-research-foundation/).

For research data management, see below ["Support for research data management"](#support-for-research-data-management) and the checklists on [RDM in a DFG grant application](en/fdm_dfg_antrag_en.md) or the checklists [RDM in a Horizon Europe grant application](en/fdm_horizon_europe_antrag_en.md) and [RDM in an ERC grant application](en/fdm_erc_antrag_en.md).

### DFG group funding

#### CRC

Contact for applying for Collaborative Research Centres:
[S-Forschung](https://www.fau.de/fau/leitung-und-gremien/geschaeftsverteilungsplan-der-verwaltung/pr-stab-s-praesidialstab/referat-s-forschung/).

For research data management, see below ["Support for research data management"](#support-for-research-data-management) and the checklist [RDM in a CRC grant application](en/fdm_sfb_antrag_en.md).

#### RTG

Contact for research training group grant applications:
[S-Nachwuchs](https://www.fau.de/fau/leitung-und-gremien/geschaeftsverteilungsplan-der-verwaltung/pr-stab-s-praesidialstab/referat-s-nachwuchs/).

For research data management, see below ["Support for research data management"](#support-for-research-data-management) and the checklist [RDM in an RTG grant application](en/fdm_grk_antrag_en.md) as well as the [FAU Service Matrix Trainings](en/fau_service_matrix_schulungen_en.md).

## Support for research data management

Contact FAU's research data management team early on so that the application planning can be accompanied in the best possible way: [forschungsdaten@fau.de](mailto:forschungsdaten@fau.de).

In the application: reference to FAU's [Research Data Policy](https://fau.info/fdm-policy).

Please find out what research funders currently expect with regard to your research data. Lists of the policies of the most important funding bodies are compiled here: [University Library website](https://ub.fau.de/en/research/data-and-software-in-research/policies-of-research-funding-organisations-and-publishers/) and [fdm-bayern.org](https://www.fdm-bayern.org/policies/).

### FAU Competence Center for Research Data and Information (CDI)

Central scientific unit to support all researchers in research data management and the Research Information System [CRIS](https://cris.fau.de/converis/portal?lang=en_GB).

Services:

- [Application advice](https://www.cdi.fau.de/services/antraege/)
- Advice on metadata (creation and completion)
- Advice on data storage and archiving
- Trainings
- Advice on the [FAUDataCloud](https://www.cdi.fau.de/services/faudatacloud-it-services-fuer-forschungsdaten/)
- Advice on [WissKI and the FAUWissKICloud](https://www.cdi.fau.de/services/wisski-und-fauwisskicloud/)
- Advice on [electronic lab notebooks](https://www.cdi.fau.de/services/elektronisches-laborbuch/)
- Advice on [CRIS](https://www.cdi.fau.de/services/forschungsinformationssystem-fau-cris/)

Website: [https://www.cdi.fau.de](https://www.cdi.fau.de)

Contact:

Dr. Marcus Walther\
Martensstraße 3, Room 08.154\
(Mondays 13-17 h & Thursdays 8-12 h)\
91058 Erlangen\
Tel: +49 9131 85-29972 bzw. -26387\
E-mail: [marcus.walther@fau.de](mailto:marcus.walther@fau.de)

### University Library

#### Research Data Management Department

Services:

- Advice on RDM: support from the initial planning and application process to the end of the project on research, planning, data description, data publication, access regulations, terms of use and data formats.
- Trainings: [Introduction to research data management](https://ub.fau.de/schulungen/einfuehrung-ins-forschungsdatenmanagement/) (120 minutes, by arrangement, min. 5 participants)
- Deployment of the [RDMO instance](https://rdmo.ub.fau.de/) and information on how to use it ([FAQ](https://ub.fau.de/haeufig-gestellte-fragen-faq/forschungsdaten/rdmo/) and [Videos on StudOn](https://www.studon.fau.de/studon/goto.php?target=lm_2993053))
- [Access to the RADAR repository](https://ub.fau.de/en/research/data-and-software-in-research/archiving-and-publication-of-data/#collapse_0) for the publication and archiving of research data from completed scientific studies and projects.

Website: [https://ub.fau.de/en/research/data-and-software-in-research/](https://ub.fau.de/en/research/data-and-software-in-research/)

Contact:

Dr. Jürgen Rohrwild\
Technisch-naturwissenschaftliche Zweigbibliothek\
Room 0.314\
Erwin-Rommel-Str. 60\
91058 Erlangen\
Tel: +49 9131 85-28591\
Fax: +49 9131 85-27843\
E-mail: [juergen.rohrwild@fau.de](mailto:juergen.rohrwild@fau.de)

#### Legal issues in the field of open access, research data management

Contact:

Petra Heermann\
Alte Universitätsbibliothek\
Universitätsstraße 4\
91054 Erlangen\
Tel: +49 9131 85-23920\
E-mail: [petra.heermann@fau.de](mailto:petra.heermann@fau.de)

#### Digital Humanities (Software tools, authority data, ontologies and linked data)

Services:

- Advice and training for digital humanities on:
  - Data management
  - Software tools
  - Data structures
  - Authority data ontologies and linked data

Website with information on advisory services:
[https://ub.fau.de/ub-coach/forschen/digital-humanities/](https://ub.fau.de/ub-coach/forschen/digital-humanities/)

Website of the Digital Skills HUB:
[https://ub.fau.de/lernen/digital-skills-hub/](https://ub.fau.de/lernen/digital-skills-hub/)

Contact for advice:

Martin Scholz\
Alte Universitätsbibliothek\
Universitätsstraße 4\
91054 Erlangen\
Tel: +49 9131 85-23935\
E-mail: [martin.scholz@fau.de](mailto:martin.scholz@fau.de)

Contact for the Digital Skills HUB:

Tonka Stoyanova\
Hauptbibliothek\
Universitätsstraße 4\
91054 Erlangen\
Tel: +49 9131 85-22161\
E-mail: [tonka.stoyanova@fau.de](mailto:tonka.stoyanova@fau.de)

### Regional Computing Centre Erlangen (RRZE)

Services:

- [Server services](https://www.rrze.fau.de/serverdienste)
(server hosting, server housing), web services, databases
- [High-performance computer (HPC)](https://www.rrze.fau.de/serverdienste/hpc/)
- [GitLab](https://www.rrze.fau.de/serverdienste/infrastruktur/gitlab/)
- [Backup and archiving](https://www.rrze.fau.de/serverdienste/infrastruktur/backup-archivierung/)

Overview of services for research data:
[https://www.rrze.fau.de/forschung/forschungsdaten/](https://www.rrze.fau.de/forschung/forschungsdaten/)

Website of the RRZE: [https://www.rrze.fau.de/](https://www.rrze.fau.de/)

Contact:

Zentrale Service-Theke\
Martensstraße 1\
Room 1.031\
91058 Erlangen\
Tel: +49 9131 85-29966\
E-mail: [rrze-zentrale@fau.de](mailto:rrze-zentrale@fau.de)

## Tools for research data management

The following is a list of some tools that are either (co-)developed at FAU or developed externally but found to be helpful and recommendable.

### Data management plans (DMP)

Even if not all funding sources require a data management plan (DMP), it makes sense to generate a DMP for yourself and the project, especially for collaborative work with data, and to keep it up to date throughout the project duration.

For questions about the DMP, please send an e-mail to [forschungsdaten@fau.de](mailto:forschungsdaten@fau.de).

There are various software tools that assist in the DMP creation:

- Internal tool: FAU's [RDMO instance](https://rdmo.ub.fau.de/) (Research Data Management Organizer)
  - Further notes on RDMO as [FAQ](https://ub.fau.de/en/frequently-asked-questions/research-data/rdmo/)
  - Notes and instructions on how to use the RDMO on [StudOn](https://www.studon.fau.de/studon/goto.php?target=lm_2993053).
- External tools:
  - [DMPOnline](https://dmponline.dcc.ac.uk/): useful if the DMP is to meet the requirements of a UK research funder.
  - [DMPTool](https://dmptool.org/): useful if the DMP is to meet the requirements of a research funder from the USA.
  - [DMP-Wizard](https://www.clarin-d.net/en/preparation/data-management-plan) provided by the CLARIN project for research projects on language. Very useful if the data will be archived in one of the [CLARIN Data Centres](https://www.clarin-d.net/en/preparation/find-a-clarin-centre).
  - [DataWiz](https://datawiz.leibniz-psychology.org/DataWiz/): especially for psychology.
  - [GFBio Data Management Plan Tool](https://www.gfbio.org/plan): specifically for biodiversity/life science, may also be useful in related disciplines.
  - [ARGOS](https://argos.openaire.eu/home) by OpenAIRE
  - [Data Stewardship Wizard](https://ds-wizard.org/): Science Europe and Horizon 2020 questionnaires, with FAIR metrics.

**Questionnaires** for various funding sources can help with the DMP preparation:

In FAU's [RDMO](https://rdmo.ub.fau.de/) instance you will find:

- RDMO-questionnaire (Standard)
- RDMO-questionnaire with additional information for the humanities
- DFG grant applications
- DFG grant (Ancient Cultures, Review Board 101)
- DFG grant (Social/Economic Sciences)
- DFG grant (Educational Sciences, review board 109)
- DFG grant (Social & Cultural Anthropology, Judaic studies, Religious Studies)
- DFG grant (Scientific Editions in Literary Studies)
- DFG grant (Chemistry)
- DFG grant (Physics)
- Horizon 2020 Grants
- VW Foundation -- Science Europe Research Data Management Plan

### Tools

- [WissKI](http://wiss-ki.eu/), a virtual research environment:
WissKI is a system for the management, analysis and publication of research data in cultural studies. It implements semantic web methods for the acquisition, storage, reuse and semantic annotation of artefacts. A standardised basic ontology ([CIDOC-CRM](http://www.cidoc-crm.org/)) with domain-specific extensions ensures interdisciplinary reusability on the one hand and enables a flexible data model on the other.
  - [WissKI distillery](<https://github.com/FAU-CDI/wisski-distillery>): a cloud system for WissKIs operated and developed by the CDI. This allows WissKIs for FAU projects to be created at the push of a button and centrally kept up to date with the latest technology (see also [WissKI and FAUWissKICloud](https://www.cdi.fau.de/services/wisski-und-fauwisskicloud/) on the CDI-website or the wiki [FAUWissKICloud](https://gitlab.rrze.fau.de/cdi/labs/wisski/FAUWissKICloud/-/wikis/FauWissKIcloud)).
  - FAU's [AG WissKI](https://www.izdigital.fau.de/team/wisski/)
  - Technical questions: [Laura Albers](mailto:laura.albers@fau.de) and [Tom Wiesing](mailto:tom.wiesing@fau.de)
- [DataCite Metadata Generator](https://dhvlab.gwi.uni-muenchen.de/datacite-generator/) for metadata creation according to the DataCite schema
  - [DataCite Best Practice Guide](https://doi.org/10.5281/zenodo.3559799)

### Storage & Archiving/Repositories

- [FAUbox](https://www.rrze.fau.de/serverdienste/server/faubox) for storage and data exchange during the project, also suitable for sensitive data, 50 GB.
- [FAUDataCloud](https://www.cdi.fau.de/services/faudatacloud-it-services-fuer-forschungsdaten/), 2 TB archive space as basic provision, 100 GB project storage, in future 50 GB database space, general services such as GitLab, chat service and specific services such as ELN, WissKI, JupyterLab, etc. Needs beyond this can be requested from the CDI. Large projects such as RTG/CRC must be applied for separately.
- Data repository [RADAR](https://www.radar-service.eu/radar/en/home): further info at [ub.fau.de](https://ub.fau.de/en/research/data-and-software-in-research/archiving-and-publication-of-data/#collapse_0)
- [FAU Zenodo Community](https://zenodo.org/communities/fau/?page=1&size=20)
- Search for further repositories on [re3data](https://www.re3data.org/)
- Storage and archiving of research data by the [RRZE](https://www.rrze.fau.de/forschung/forschungsdaten/)

### High-performance computer (HPC)

For numerical simulations and big data applications, the RRZE provides high-performance computers and compute servers.

Websites: [NHR@FAU](https://hpc.fau.de/) und
[HPC-Support](https://www.rrze.fau.de/infocenter/kontakt-hilfe/hpc-beratung/)

Contact:

HPC-Beratung\
Zentrale Systeme\
Martensstraße 1\
91058 Erlangen\
E-mail: [support-hpc@fau.de](mailto:support-hpc@fau.de)
