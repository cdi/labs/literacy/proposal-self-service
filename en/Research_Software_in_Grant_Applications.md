# Research software
## What is research software?
Research software comprises a variety of programs, scripts and algorithms that are used in scientific research. It ranges from simple scripts for data aggregation and software tools for controlling measuring devices to complex data analysis tools and elaborate simulations.

Research software is therefore a crucial building block for the creation of new research results and thus also for their reproducibility. For this reason, Open Science and research funders who have taken up this cause also demand transparency, openness and broad access in relation to research software as far as possible.

## Guidelines / recommendations of the research funders
### DFG
DFG is a member of the “Research Software Alliance” (https://www.researchsoft.org/) and a signatory to the “Amsterdam Declaration on Funding Research Software Sustainability” (https://adore.software/declaration).

DFG does not currently (as of 01/25) explicitly require a software management plan. The topic of research software should nevertheless be addressed in the funding proposal, if applicable. The following points follow the recommendations of the DFG paper “Handling of Research Software in the DFG`s Funding Activities” (2024), pp. 7-10):

* To what extent is new or further development of research software taking place?
* Documentation of the software used or (further) developed in order to ensure long-term accessibility to the research results. This concerns not only the source code, the workflows and the functionality of the software, but also the comprehensive documentation of the individual parameters in order to make the research process verifiable and reproducible.
* Version management of the software
* Measures for quality assurance of the software in the research and development process
* Integration, use and licensing of third-party codes 
* Transparent citation
* Clarification of the rights regarding the authorship of the software developers before the start of the project
* License conditions
* Publication of research software and software documentation in accordance with the requirements of Open Science and [FAIR4RS](#fair4rs-principles) (within the scope of legal possibilities)
* Measures to ensure accessibility, preservation, further development, findability and interoperability (especially with a view to the long-term perspective)

DFG funding can be requested for personnel and material costs, but also for external service providers who develop the research software components. Measures to improve the ecological sustainability of the software are also eligible for funding.

If the respective call for proposals contains specific statements relating to software management and software management plans, these must be taken into account.

Also consider the DFG guidelines on good scientific practice (https://www.dfg.de/download/pdf/foerderung/rechtliche_rahmenbedingungen/gute_wissenschaftliche_praxis/kodex_gwp.pdf).


### Requirements of other research funders

**ERC / Horizon Europe** does not currently (as of 02/25) require a software management plan as standard (in contrast to the data management plan). However, there is a requirement that access to research data and results should be guaranteed. In order to achieve this, the use of and thus access to specific research software is often necessary. 

**BMBF** does not currently have any general guidelines with regard to research software. However, there are often specifications in the individual funding lines.

The situation is similar at the **Volkswagenstiftung**. Software management is not explicitly required here either. However, since software is not only seen as a “by-product”, but as an essential component of research, this topic should be addressed in the application if necessary. 

## FAIR4RS principles
The FAIR4RS principles (FAIR for Research Software) of the Research Data Alliance (RDA) adapt the well-known FAIR principles to research software (https://zenodo.org/records/6623556). If your own development follows these principles, it makes sense to mention this in the application.

1. **Findable**: Research software should be findable through unique, persistent identifiers such as DOI or URN. In addition, the software should be deposited in repositories or catalogs that are well documented and accessible.
2. **Accessible**: The software should be accessible, where this includes both download and access to the source code and documentation. License conditions and usage rights should also be presented clearly and transparently.
3. **Interoperable** : Research software should be developed to be compatible with other software systems and data. It should use open standards, interfaces and protocols to promote integration and interoperability.
4. **Reusable**: The code should be documented and structured in a way that allows other researchers to understand, modify and reuse it. Transparent licenses and detailed instructions play an important role here.

## Further relevant documents:

### Helmholtz Centers
* Guidelines for the utilization and licensing of research software https://www.gfz.de/fileadmin/gfz/zentrum/Transfer_Innovation/Technologietransfer/GFZ_Software-Richtlinie-v.1.1.pdf (as of 04/23)
* Guidelines for research software: https://www.gfz.de/fileadmin/gfz/zentrum/Transfer_Innovation/Technologietransfer/GFZ_Software-Leitfaden-v.2.0.pdf (as of 04/23)
### Alliance of Science Organizations 
* “Handreichung zum Umgang mit Forschungssoftware” (https://zenodo.org/records/1172970) (2018) 
* “Digitale Dienste für die Wissenschaft - wohin geht die Reise?” https://zenodo.org/records/4301924

Wilson, G; Bryan, J. et al (2017): Good enough practices in scientific computing. PLoS Comput Biol 13(6). https://doi.org/10.1371/journal.pcbi.1005510

Videos on various topics relating to research software on the ZB Med homepage: https://darum.zbmed.de/warum-braucht-es-forschungssoftware/

## Sources
Biernacka, K., & Helbig, K. (2023). Train-the-Trainer-Konzept zum Thema Forschungsdatenmanagement: Erweiterungsmodul Softwaremanagementplan (SMP). Zenodo. https://doi.org/10.5281/zenodo.10197107
https://forschungsdaten.info/themen/beschreiben-und-dokumentieren/softwareentwicklung-und-gute-wissenschaftliche-praxis/

