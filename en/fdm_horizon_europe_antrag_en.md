# RDM in a Horizon Europe grant application

<!-- Table of contents for the GitLab version, must be deleted/commented out for a PDF version -->
[[_TOC_]]

## General regulations and recommendations

<!-- TO DO: email address of the institution's RDM officer. -->
For assistance, please contact FAU's **research data team** early in the application process: <forschungsdaten@fau.de>.

<!-- TO DO: Replace with the institution's research data policy, if applicable. -->
- Refer to FAU's [Research Data Policy](https://fau.info/fdm-policy) and [Open Science Policy](https://doi.org/10.5281/zenodo.5602559).
- Refer to the [FAIR Principles](https://www.go-fair.org/fair-principles/).

In Horizon Europe, **Open Science** should be the *modus operandi* for all researchers: Knowledge, data and tools are shared as openly as possible at an early stage.

Note the **Information provided by Horizon Europe** on research data and Open Science:

- [Model Grant Agreement (MGA)](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/common/agr-contr/general-mga_horizon-euratom_en.pdf) (Annex 5, Article 17)
- [Annotated Grant Agreement (AGA)](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/common/guidance/aga_en.pdf) (Notes to Annex 5, Article 17).

For more information, see the relevant sections on "Open Science" in the [Horizon Europe Programme Guide](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/horizon/guidance/programme-guide_horizon_en.pdf).

Quality and adequacy of research procedures are assessed with regard to Open Science under **Part B, Section 1. Excellence** (in particular under **1.2 Methodology**).
Open Science practices should be explained on approximately one page.
If Open Science practices are not possible in your project, explain why.
Research projects that generate or reuse data should describe their research data management on one (additional) page.
Innovative research practices that ensure that research results are timely, relevant, transparent and reproducible can be funded via the grant programme.
Details on publication, licences and metadata are usually specified in the corresponding grant agreement.
Researchers should be aware of these and include them in their applications.
Under **Section 3. Quality and efficiency of the implementation** (in particular **3.2 Capacity of participants and consortium as a whole**), the expertise and proven track record of the project partners with regard to Open Science should also be included.

Depending on the Horizon Europe work programme/tender, there may be further requirements related to Open Science and data management.
Please take note of all relevant documents and information.

### NFDI

If applicable, refer to a suitable [NFDI consortia](https://www.nfdi.de/konsortien/). The **N**ationale **F**orschungs**D**aten**I**nfrastructure is a major initiative of the federal and states governments to develop and establish (mostly discipline-specific) services and best practices in data management. <!-- TO DO: If applicable, mention the institution's participation in NFDI consortia. --> FAU participates in 11 of 27 consortia:

- [DAPHNE4NFDI](https://www.daphne4nfdi.de/index.php) (Photon and Neutron Experiments - diffraction, tomography, imaging, spectroscopy)
- [FAIRmat](https://www.konsortswd.de/https:/www.fair-di.eu/fairmat/fairmat_/consortium) (Condensed Matter and Chemical Physics)
- [KonsortSWD](https://www.konsortswd.de/) (Social, Behavioral, and Economic Sciences)
- [MaRDI](https://www.mardi4nfdi.de/) (Mathematics)
- [MatWerk](https://nfdi-matwerk.de/) (Materials Science)
- [NFDI4Cat](https://nfdi4cat.org/) (Catalysis Research)
- [NFDI4Culture](https://nfdi4culture.de/) (Material and Immaterial Cultural Heritage)
- [NFDI4Earth](https://www.nfdi4earth.de/) (Earth System Science - Earth Science)
- [NFDI4Energy](https://nfdi4energy.uol.de/) (Interdisciplinary Energy System Research)
- [NFDI4Microbiota](https://nfdi4microbiota.de/) (Life Sciences)
- [NFDI4Objects](https://www.nfdi4objects.net/) (Material Remains of Human History)

<!-- TO DO: Replace with relevant contact if necessary -->
The [FAU Competence Center for Research Data and Information (CDI)](https://www.cdi.fau.de/) is also happy to provide local contact information.

If you will use support infrastructures for handling research data via participating institutions, mention this in the application.

## Central points in a Horizon Europe application

The application should generally cover the following topics.

1. **Generation or reuse of data**:
You should present the nature and scope of your data.
Mention here also the **data formats** used in the individual research steps (e.g. raw data, processed / anonymised data, aggregated data).
Also briefly mention **quality assurance measures** beyond documentation and standards; such as duplicate data collection or fixed calibration protocols.
Finally, take the opportunity to point out that data description and documentation is done with the [FAIR Principles](https://www.go-fair.org/fair-principles/) in mind.

2. **Findability**:
As a general principle, data should be available to the community for reuse in **trusted repositories** (see [AGA](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/common/guidance/aga_en.pdf), Art. 17) as early as possible and within the timeframes set out in the data management plan, where possible and appropriate.
Applications for work programmes that require the use of repositories federated by the EOSC should explicitly mention the use of these repositories.
If you do not provide or archive the data in a long-term infrastructure (such as [Zenodo](https://zenodo.org/), [RatSWD-certified repositories](https://www.konsortswd.de/datenzentren/alle-datenzentren/), NFDI-recommended infrastructures [e.g. [NFDI4Culture](https://nfdi4culture.de/de/ressourcen/repositorien.html), [NFDI4Chem](https://www.nfdi4chem.de/index.php/repos/)] ...), you should always aim at archiving the data on which publications are based for at least ten years at <!-- TO DO: If applicable, replace by the institution and its service --> a local FAU system like the [FAUDataCloud](https://www.cdi.fau.de/services/faudatacloud-it-services-fuer-forschungsdaten/) (please discuss the volume requirements with the [CDI](https://www.cdi.fau.de/) before submitting an application).

3. **Access to data**:
In principle, **Open Access** to research data applies: "as open as possible, as closed as necessary"; ideally with a **CC0** or **CC BY** licence.
If you make data available to other researchers or publish data, then please address the access conditions (e.g. embargo periods until accessibility, access requirements).
Indicate why data are (not) made available. Here you may indicate the potential for subsequent use, the great effort required to collect the data, the unique nature of the data or data protection barriers, copyright, legitimate interests of industry partners, etc.

4. **Interoperability**: The aim should be documenting the data well enough so that someone from the subject community can understand the creation process and reuse the data.
To emphasise this, refer where possible to existing **standards** (metadata, ontologies, classifications) that are widely used in the community.
If there is an **NFDI** in your subject (see above) and you use a standard (or service) recommended there, **feel free to refer to their recommendations**.

**FAIR metadata** should be published under **CC0** or similar licence and include persistent identifiers of associated publications and other research results where appropriate.

5. **Reuse**: You should mention the **licences to use** the data and access to tools, software and models for data collection and validation, interpretation and reuse.
If you use special software or self-written software, then also address how you ensure research reproducibility (publishing your own software; the software used is widely used in the field, etc.).
**Documentation** of software, algorithms and models should be provided to ensure reproducibility and reusability.

6. **Curation, backup and archiving costs**:
Finally, you should name the persons who are ultimately responsible for individual data management steps in the project.
Typical areas would be training new employees in RDM workflows, coordination between work groups, long-term storage.
In smaller projects, this is often one person.
You should identify them by name if possible.
Also, indicate who or which office will be responsible for handling data after funding ends (keyword: sustainability).
For storage, you can e.g. refer to the services of the <!-- TO DO: If applicable, replace with the corresponding institution (RRZE) and services (FAUDataCloud) --> RRZE or the FAUDataCloud (see also 2.).
Also indicate here which **resources** are allocated for RDM (personnel resources and other costs).
You should also include the costs of data management (quality assurance, storage, processing, archiving) in the budget, as these are refundable.
Individual projects may require IT staff resources for data management on a selective basis.
For example, an IT position for only 3 months to customize a database or research environment.
In such cases, please <!-- TO DO: Replace with the corresponding contact if applicable --> contact the [CDI](https://www.cdi.fau.de/):
The CDI has access to a pool of IT experts, which may be used to ensure that the requested person-months are available at project start.

## Data management plan

A full data management plan is not required at the time of application submission, but should ideally be started before project start and must normally be submitted **6 months after project start**.
An updated DMP must be submitted halfway through the project (if longer than 12 months) and at the end of the project.
Ideally, DMPs should be published.
A [**DMP template**](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/horizon/temp-form/report/data-management-plan_he_en.docx) is provided for Horizon Europe.
<!-- TO DO: If applicable, replace the link to the RDMO instance or mention another DMP tool. -->
<!--When creating the DMP for Horizon Europe, the corresponding **[RDMO questionnaire](https://rdmo.ub.fau.de/) can be used in FAU's RDMO instance.-->
We recommend using the template, even though it is lengthy. This makes it easier to formally check your DMP for completeness / compliance. 

If you choose not use the HE DMP template, be sure to cover the same topics; particularly:

- Description of datasets: scientific focus, technical approach, data types, data volume.
- Standards and metadata.
- Name and persistent identifiers of the datasets.
- Curation and preservation measures: Standards to ensure data integrity, storage in trusted repository.
- Data security: backup measures, measures for handling sensitive data.
- Data availability: access to data for validation should be ensured; licences for use, access restrictions, timing of publication, duration. Data should be published as soon as possible after generation, at the latest at the end of the project. Data associated with a publication should be made available with the publication. Not only publications and data should be made openly available, but if possible also software, models, apps, algorithms, protocols, workflows, electronic lab notebooks, etc. that are developed and/or used in the research project. These should also be considered in the data management plan.
- Ethical and legal aspects, for example informed consent.
- Costs of data management and responsibilities: Costs of generating and/or re-using data, of data documentation, of data backup, of publication; persons responsible for data management and quality assurance.
