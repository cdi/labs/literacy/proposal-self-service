# RDM in an ERC grant application

<!-- Table of contents for the GitLab version, must be deleted/commented out for a PDF version -->
[[_TOC_]]

## General regulations and recommendations

<!-- TO DO: email address of the institution's RDM officer. -->
For assistance, please contact FAU's **research data team** early in the application process: <forschungsdaten@fau.de>.

Research data management should be incorporated in the project planning and application process.

For **ERC applications**, please consider the ERC's [Open Science](https://erc.europa.eu/managing-project/open-science) website, the section on Open Science in the "[ERC Work Programme 2022](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/horizon/wp-call/2022/wp_horizon-erc-2022_en.pdf)" and "[Open Research Data and Data Management Plans. Information for ERC grantees](https://erc.europa.eu/sites/default/files/document/file/ERC_info_document-Open_Research_Data_and_Data_Management_Plans.pdf)".

Depending on the call for proposals, there may be further requirements regarding Open Science and data management.
Please take all relevant documents and information into account.

In the **application form** there is no separate section on Open Science practices in your project. In **Part B2 b. Methodology**, you should nevertheless briefly (2-3 sentences) refer to your research data and **Open Science practices**.
Important Open Science practices for EU-projects are:

- Data should be treated according to **[FAIR principles](https://www.go-fair.org/fair-principles/)**, shared as **"open as possible - as closed as necessary"** in a trusted **repository**: if possible with **CC BY**, **CC0** licence or similar.
- If research data cannot be made available, this must be explained and justified in the data management plan. They must still be managed according to the [FAIR principles](https://www.go-fair.org/fair-principles/) and **FAIR metadata** (with bibliographic and administrative information) should always be made available (under licence like CC0) in a machine-readable format.
- **Metadata**: The metadata basic standard is [Dublin Core](https://dublincore.org/). Reference to ERC funding must be included. For the required bibliographic and administrative information, see Horizon Europe [Model Grant Agreement](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/common/agr-contr/general-mga_horizon-euratom_en.pdf) (Annex 5, Article 17): Record information, Horizon Europe funding, project name, acronym and number, licence, persistent identifiers for the records (e.g. DOI), the authors (e.g. [ORCID iD](https://orcid.org/)) and if possible for your institution (e.g. [ROR ID](https://ror.org/)). Where applicable, the metadata must also include persistent identifiers to associated publications and other research outputs. Metadata should be published with a CC0 licence and persistent identifier.

All data-generating ERC projects must submit a **data management plan** no later than **6 months after project start** and update it during the project lifetime. A [DMP template for ERC applications](https://erc.europa.eu/sites/default/files/document/file/ERC-Data-Management-Plan.docx) is provided.

ERC funding for **data management costs** may be requested.

***Once the project has been approved, please note the following points regarding the data management plan and NFDI.***

<!-- for a PDF version: -->
<!-- \newpage -->

## Data management plan

Create a data management plan for discoverable, accessible, interoperable, reusable and publicly available research data and update it regularly during the project lifetime. Funded projects should demonstrate that their data management follows the [FAIR principles](https://www.go-fair.org/fair-principles/).

For approved ERC projects, a **[DMP template](https://erc.europa.eu/sites/default/files/document/file/ERC-Data-Management-Plan.docx)** is provided.
<!-- TO DO: If applicable, replace the link to the RDMO instance or mention another DMP tool. -->
<!-- When creating the DMP for Horizon Europe, the corresponding **[RDMO questionnaire](https://rdmo.ub.fau.de/)[^RDMO]** may be used.-->

If you do not use the template, be sure to cover these topics:

- **Description** of datasets: scientific focus, technical approach, data types, volume.
- **Standards** and metadata.
- Name and **persistent identifiers** of the datasets.
- **Curation and preservation measures**: Standards to ensure data integrity, storage in trusted repository.
- **Data security**: Backup measures, measures for handling sensitive data.
- **Data availability**: Access to data for validation should be ensured; use licences, access restrictions, timing of publication, duration. Data should be published as soon as possible after generation, at the latest at the project end. Data associated with a publication should be made available with the publication. Not only publications and data should be made openly available, but if possible also software, models, apps, algorithms, protocols, workflows, electronic lab notebooks, etc. that are developed and/or used in the research project. These should also be addressed in the DMP.
- **Ethical and legal aspects**, for example informed consent.
- **Data management costs** and **responsibilities**: Costs of generating and/or re-using data, data documentation, data backup, data publication; persons responsible for data management and quality assurance.

## NFDI

If applicable, contact should be made with the [NFDI consortia](https://www.nfdi.de/konsortien/). The **N**ationale **F**orschungs**D**aten**I**nfrastruktur is a major initiative of the federal and state governments to develop and establish (mostly discipline-specific) service and best practices in data management. <!-- TO DO: If applicable, mention the institution's participation in NFDI consortia. --> FAU participates in 11 of 27 consortia:

- [DAPHNE4NFDI](https://www.daphne4nfdi.de/index.php) (Photon and Neutron Experiments - diffraction, tomography, imaging, spectroscopy)
- [FAIRmat](https://www.fairmat-nfdi.eu/fairmat/) (Condensed Matter and Chemical Physics)
- [KonsortSWD](https://www.konsortswd.de/) (Social, Behavioral, and Economic Sciences)
- [MaRDI](https://www.mardi4nfdi.de/) (Mathematics)
- [MatWerk](https://nfdi-matwerk.de/) (Materials Science)
- [NFDI4Cat](https://nfdi4cat.org/) (Catalysis Research)
- [NFDI4Culture](https://nfdi4culture.de/) (Material and Immaterial Cultural Heritage)
- [NFDI4Earth](https://www.nfdi4earth.de/) (Earth System Science - Earth Science)
- [NFDI4Energy](https://nfdi4energy.uol.de/) (Interdisciplinary Energy System Research)
- [NFDI4Microbiota](https://nfdi4microbiota.de/) (Life Sciences)
- [NFDI4Objects](https://www.nfdi4objects.net/) (Material Remains of Human History)

<!-- TO DO: Replace with intermediary institution if necessary. -->
The [FAU Competence Center for Research Data and Information (CDI)](https://www.cdi.fau.de/) is also happy to provide the local contact information.

You should mention support in handling research data by participating institutions in the proposal.
