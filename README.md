# FDM-Handreichungen

Die deutsch- und englischsprachigen Handreichungen richten sich an Forschende, sowie an Forschungsreferate, Drittmittelstellen und FDM-Beratungsstellen. Letztere können sie zur Unterstützung im Beratungsprozess einsetzen.

Diese Handreichungen wurden im Rahmen des Projektes "[eHumanities - interdiziplinär](https://www.fdm-bayern.org/projekte/ehumanities-interdisziplinaer/)" erstellt.

## FDM an der FAU / RDM at FAU

* [Unterstützung an der FAU](de/fdm_unterstuetzung_fau.md) / [RDM Support at FAU](en/fdm_unterstuetzung_fau_en.md)
* [FAU Service-Matrix Schulungen](de/fau_service_matrix_schulungen.md) / [FAU Service Matrix Trainings](en/fau_service_matrix_schulungen_en.md)

## Antragshilfen / Application assistance

### DFG

* [FDM im DFG-Antrag](de/fdm_dfg_antrag.md) / [RDM in a DFG grant application](en/fdm_dfg_antrag_en.md)
* [Fachspezifisches FDM im DFG-Antrag](de/fachspezifisches_fdm_dfg_antrag.md) / [Subject-specific RDM in a DFG grant application](en/fachspezifisches_fdm_dfg_antrag_en.md)
* [FDM im SFB-Antrag](de/fdm_sfb_antrag.md) / [RDM in a CRC grant application](en/fdm_sfb_antrag_en.md)
* [FDM im GRK-Antrag](de/fdm_grk_antrag.md) / [RDM in a RTG grant application](de/fdm_grk_antrag_en.md)
    * Dazugehörig: Krömer, Cora, & Rohrwild, Jürgen. (2021). Erstellung eines Service-Portfolios zur FDM-Vermittlung in der Graduiertenausbildung (Version 1). Zenodo. DOI: [10.5281/zenodo.5572331](https://doi.org/10.5281/zenodo.5572331). [in German only]
    * [Potenzielle Kandidaten für fachliche Themenschwerpunkte](de/potenzielle_schulungsangebote.md) [in German only]
    * [FAU Service-Matrix Schulungen](de/fau_service_matrix_schulungen.md) / [FAU Service Matrix Trainings](en/fau_service_matrix_schulungen_en.md)

### EU

* [FDM im Horizon Europe-Antrag](de/fdm_horizon_europe_antrag.md) / [RDM in a Horizon Europe grant application](en/fdm_horizon_europe_antrag_en.md)
* [FDM im ERC-Antrag](de/fdm_erc_antrag.md) / [RDM in an ERC grant application](en/fdm_erc_antrag_en.md)

## Weitere Hilfen / Further help

* [Repositorien](de/repositorien.md) / [Repositories](en/repositorien_en.md)
* [FDM & Rechtliche Aspekte](de/fdm_rechtliche_aspekte.md) [in German only]

---

## Hinweise zur Nachnutzung

In den vorliegenden Handreichungen werden die zahlreichen Vorgaben und Dokumente zum Forschungsdatenmanagement, die im Antragsprozess beachtet werden sollten, übersichtlich aufbereitet.
Die Handreichungen sollen bei der Projektplanung und bei der Antragstellung unterstützen, indem zusätzlich standortspezifische Informationen bereitgestellt werden.
Die Handreichungen richten sich in erster Linie an Forschende sowie Forschungsreferate, Drittmittelstellen und FDM-Beratungsstellen; diese können sie zur Unterstützung im Beratungsprozess einsetzen.
Die Handreichungen wurden für die Beratung an der Friedrich-Alexander-Universität Erlangen-Nürnberg entwickelt und enthalten standortbezogene Spezifika, die in den bereitgestellten Markdown-Dateien für die Überarbeitung zur Nachnutzung gekennzeichnet wurden.
In den *Antragshilfen* und *Weiteren Hilfen* sind die entsprechenden Stellen mit ```<!-- TO DO: <Text> -->``` gekennzeichnet.

### Lizenz

Die FDM-Handreichungen werden unter der Lizenz [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) veröffentlicht.

### Zitationsvorschlag

Krömer, Cora & Rohrwild, Jürgen. (2022). FDM-Handreichungen (Version 1.0). eHumanities - interdisziplinär. [https://gitlab.rrze.fau.de/cdi/labs/literacy/proposal-self-service](https://gitlab.rrze.fau.de/cdi/labs/literacy/proposal-self-service).

### Kontakt

Bei Fragen oder Anmerkungen schreiben Sie uns gerne eine E-Mail an [ub-fdm@fau.de](mailto:ub-fdm@fau.de).
