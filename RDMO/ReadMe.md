# ReadMe

## Was ist in dem RDMO-Ordner?

In diesem Verzeichnis werden (weitgehend) fertige RDMO-Fragenkataloge bereitgestellt, für die ein Pull-Request im zentralen RDMO-GitHub-Repositorium (https://github.com/rdmorganiser/rdmo-catalog/tree/master/shared/ub_fau_erlangen_nuernberg) erstellt wurde, aber der Request noch nicht angenommen wurde. 
Das RDMO-GitHub (https://github.com/rdmorganiser/rdmo-catalog/tree/master/shared) enthält viele Fragenkataloge aus der Community, die alle einen kurzen Review durchlaufen. Der Reviewprozess dauert unterschieldich lange - je nach Komplexität des Fragenkatalogs. Regelmäßig gibt es Verbesserungswünsche, etwa eine Anpassung der "Attribute" an die von RDMO bevorzugten Konventionen.  
Die hier verfügbaren Fragenkataloge werden unter Umständen nur so lange bereitgestellt, bis der Einreichungsprozess im zentralen Repositorium abgeschlossen ist. Danach sind sie in der offiziellen RDMO-Sammlung enthalten. 

## Ein Fragen-Katalog ist verschwunden - was tun?

Wenn ein Fragenkatalog nicht mehr in diesem GitLab auffindbar ist, dann wurde er vom RDMO-GitHub akzeptiert (siehe oben). Er ist dann dort in der neusten Version verfügbar: https://github.com/rdmorganiser/rdmo-catalog/tree/master/shared/ub_fau_erlangen_nuernberg.
