# ERC Example DMP

__[toc]__


## Preliminaries

This file provides example answers for this RDMO ERC DMP template. The example assumes the research project studies signal transmission in the brain. Answers are **not** based on a real project - so e.g. software and standards are either made up or not the ideal choice for these types of data. Nonetheless, the answers are inspired by DMPs of actual EU research projects (from different fields). 

We assume that there are two main datasets: data from a simulation (dataset "Simulations of neural signal transmission") and an EEG study using human subjects (dataset "EEG measurements").

If answers are given in brackets "[ ... ]", they correspond to choices from a collection of answer options. 

We use curly brackets \{ \} for rare comments in the answers, typically reminding the reader that this is an example that sometimes takes shortcuts so that the text is a placeholder for some specific name (e.g. the PI's).



## General information


### Introductory information

### Project information

**What is the project acronym?**

An-O-NYM

**What is the project number?**

1234567

## Summary


### Description

**Give a short descriptive abstract for the dataset:**

*Dataset "Simulations of neural signal transmission":*  The simulations use the open-source “QuantumNeuroBrain” \{totally made up\} software suite. We determine potentials and signals for various environments. The simulations vary the environment parameters (ion concentrations, temperature, ..) and consider eight different models for the transduction mechanism proposed in the literature \{or not, the example is completely made-up\}. The simulations themselves follow from input files (for models, parameters, etc.) and configuration files that capture the used settings in software and hardware. The output of the simulation is further analysed and forms the basis for illustrative images.
Input, configuration files als well as output and derived data such as images are intimately connected and are considered a single diverse dataset.

*Dataset "EEG measurements":*  This dataset includes high-resolution EEG measurements of up to 100 probands. The measurements will be accompanied by additional measurements that determine the environmental conditions (see previous dataset). The measurements will be performed using a Neuro++ machine \{again this is a made-up example\} allowing for state-of-the-art sensitivity. The dataset will be supplemented by existing measurements from a local neuroscience group. We use the open-source tool EEGalyse \{made-up example\} to analyse and visualise the results.

### Data origin

**Is the dataset being created or reused?**

*Dataset "Simulations of neural signal transmission":*  [Created]

*Dataset "EEG measurements":*  [Both (combination of re-used and newly
generated data)]

**If reused, who created the dataset?**

*Dataset "EEG measurements":*  NeuroScience Department, Group of A. Synapse

**If reused, where can the dataset be found?**

*Dataset "EEG measurements":*  DOI: 10.neuro/123456789

### Data type

**Which file formats are used?**

*Dataset "Simulations of neural signal transmission":*  Input and simulation output are stored in CSV format. Derived datasets such as cleansed or aggregated data of multiple simulations and statistical analyses are also stored in common CSV formats. All illustrative images created from the data are stored as jpg. Each data file is accompanied by a ReadMe (plain text) file that provides information on the structure of the associated data file such as column descriptions, units, software version, initial configuration, etc.

*Dataset "EEG measurements":*  The created EEG data records are converted to and initially stored as BrainVision Core Data Format triples (.vhdr, .vmrk, .eeg). Additional information such as information extracted from the manufacturer provided data files are recorded as JSON files. The data will automatically be transformed to the EEG-BIDS standard which allows for combination with the existing EEG dataset.

**What is the actual or expected size of the dataset?**

*Dataset "Simulations of neural signal transmission":*  [1 TB to 100 TB]

*Dataset "EEG measurements":*  [1 TB to 100 TB]

### Further information

**What measures will be taken to ensure integrity and quality of the
data?**

*Dataset "Simulations of neural signal transmission":*  Data quality will be ensured already during data acquisition / creation. Consistency and integrity of the data are verified during all processing steps, which also include the deposition of sets of raw data as well as curated and already analysed datasets. Each processing step is documented and all files (input, output) as well as the used version of the involved software are labeled by a unique identifier. 

*Dataset "EEG measurements":*  The measurement follows established and standardised protocols that were already employed to generate the existing data set. Each lab member is briefed in depth on the procedures. The data is converted to a widely used file standard and the group has substantial expertise in this process. The data will be stored on a RAID system in the laboratory. The regular backups are stored in the IT centre.

**Do you use support from external institutions within the project?**

*Dataset "Simulations of neural signal transmission":*  The simulations are performed on the HPC cluster. The HPC support assists with the efficient use of the hardware and collecting information on needed system specifications.

## Making data findable


### Identifiers and metadata

**Will you use unique identifiers for measurements and samples? Are you assigning persistent identifiers to datasets at certain stages of processing?**

*Dataset "Simulations of neural signal transmission":*  We plan to make the data available via the institutional repository \{NAME\}. The system automatically provides both a DOI as well as a unique local PID. Both IDs provide a long-term reference so that the datasets can be found and cited reliably. This is a crucial component to achieving FAIR data. The DOI also guarantees that basic bibliographic information can be harvested and indexed by standard search engines. 

*Dataset "EEG measurements":*  Each measurement is assigned an ID that is the pseudonym of the corresponding proband. The ID is used to distinguish data files from different measurements. The pre-processed data is still highly sensitive. The final dataset will be assigned a DOI to ensure citability see later sections.

**What metadata will be created?**

*Dataset "Simulations of neural signal transmission":*  This dataset will be made publicly available (see also next section) via the institutional repository. Basic bibliographic metadata is therefore provided according to the DataCite standard. In the absence of an
established standard that can adequately capture all crucial meta-information, we further provide additional detailed metadata (context information, experimental setup, standardised structure of the naming conventions for all files as well as variable-level details and
description of all data processing steps) is collected in ReadMe files (see also the section on interoperability). We also provide short Jupyter notebooks and scripts that can be used to reproduce specific data processing steps.

*Dataset "EEG measurements":*  Basal metadata (creators, volume, descriptions, etc.) is collected in simple JSON files (see the section on interoperability for details). The use of EEG-BIDS standard automatically encodes most other meta-information \[Note: we assume here that it does\]. 

## Making Data Openly Accessible


### Data sharing

**Will the data be made available to other research projects / published?**

*Dataset "Simulations of neural signal transmission":*  The processed data along with documentation, model files, and input / configuration files will be made available in full. The output files are quite large and can in principle be reproduced directly using free software and our model and input files. Nonetheless, the output data will be stored and
made available to researchers on request. The associated metadata is made available and each part will be assigned a DOI.

*Dataset "EEG measurements":*  The full dataset will not be publicly shared. Only the associated metadata will be registered and published along with a detailed description of the dataset.

**If some data is not published, please explain your reasoning. Differentiate between legal and contractual reasons and voluntary restrictions.**

*Dataset "EEG measurements":*  The dataset contains sensible personal information. Both data protection regulations and common research ethics prevent broad and unregulated sharing. Only an aggregated and fully anonymised version of the data can be made directly available. The anonymisation process will unfortunately substantially reduce the reuse
potential for these data but allow other researchers to judge whether the data are potentially interesting. The full data will - following the informed consent agreements - only be shared for research purposes after the evaluation of each access request by the instituted data access commission.

**Where will the data (including metadata, documentation and, if applicable, relevant code) be published or archived after the end of the project?**

*Dataset "Simulations of neural signal transmission":*

-   [Own institution]
-   [Generic data center: Zenodo]

*Dataset "EEG measurements":*

-   [Own institution]
-   [Discipline specific data center: EEGDataCentre (anonymised and aggregated data)]

**How will you ensure that the data can be accessed and used?**

*Dataset "Simulations of neural signal transmission":*  Since all files use standardised formats (see summary section) no special software is needed. Notebooks and scripts are provided that can be used to retrace the more involved analysis steps. The scripts can be found on GitHub \[Link to group Git\]. The notebooks and the scripts require no specific tool beyond basic and free software (python, emacs, ...).

*Dataset "EEG measurements":*  Since all files use established and common highly standardised formats (see summary section), the data can be used in the scientific community. The needed software is broadly available and open-source / free solutions exist.

## Making Data Interoperable


### Documentation and metadata

**Which information is necessary and provided for other parties to understand the data (that is, to understand their collection or creation, analysis, and research results obtained on its basis) and to reuse it?**

*Dataset "Simulations of neural signal transmission":*

-   [Content]
-   [Methodology]
-   [Creation process]
-   [Technology]
-   [Documentation of the software necessary to use the data]
-   [Identifiers]

*Dataset "EEG measurements":*

-   [Methodology]
-   [Creation process]
-   [Technology]
-   [Time]
-   [Sources]
-   [Agents]
-   [Identifiers]

**Which standards, ontologies, classifications etc. are used to describe the data?**

*Dataset "Simulations of neural signal transmission":* [A custom description system is used (please briefly outline and, if necessary, indicate where it is documented in more detail): ReadMe files for in-depth documentation.]

*Dataset "EEG measurements":* [Discipline-specific standards, classifications etc. are used: NeuroML \{again made-up example\}]


## Increase Data Reuse


### Reuse

**How will you ensure that the data is preserved?**

*Dataset "Simulations of neural signal transmission":*  All data either are published directly via the institutional repository or are stored locally on the IT centre archive servers. Published data will be available for the foreseeable future. The closed data archive provides curated storage and ensures that the full dataset is archived in accordance with DFG Good Scientific Practice regulations for at least 10 years. Hence, crucial data will be available far beyond the funding period of the project and our choice of open formats (see Summary section) reduces the risk of storing valuable data in obsolete formats and structures.

*Dataset "EEG measurements":*  All data either are published directly via the institutional repository (if fully anonymised) or are stored locally on secure servers. Published data will be available for the foreseeable future. The closed data archive provides curated storage and ensures that the full dataset is archived in accordance with DFG Good Scientific Practice regulations for at least 10 years. Hence, crucial data will be available far beyond the funding period of the project and our choice of open formats (see Summary section) reduces the risk of storing valuable data in obsolete formats and structures.

**Under what license will the data be made available?**

*Dataset "Simulations of neural signal transmission":*  [Creative Commons Attribution (CC-BY)]

*Dataset "EEG measurements":*  [Creative Commons Attribution (CC-BY)]

**Shall there be an embargo period before the data are made available?**

*Dataset "Simulations of neural signal transmission":*  This dataset is planned to be published along with the journal publication without any embargo periods.

*Dataset "EEG measurements":*  The anonymised datasets will be made available at the latest with the end of the funding period.

## Allocation of Resources and Data Security


### Storage

**Where will the data be stored during the project?**

*Dataset "Simulations of neural signal transmission":*  While all data are initially stored on local servers in the research institute, all data that pass the initial quality checks are additionally transferred to servers of the university computing centre. Here automatic backup services are available, which will allow for straightforward recovery in case of data loss.

*Dataset "EEG measurements":*  All data are stored on local servers in the research institute. The RAID system provides redundancy and all data is backed up daily. Fully anonymised data are also stored on servers of the IT centre to add a layer of redundancy.

**Describe the data transfer to project partners, if applicable.**

*Dataset "Simulations of neural signal transmission":*  The data will not be transferred outside of the university data infrastructure.

*Dataset "EEG measurements":*  The data will not be transferred outside of the university data infrastructure.

### Data security

**Which measures or provisions are in place to ensure data security (e.g., protection against unauthorised access, data recovery, transfer of sensitive data)?**

*Dataset "Simulations of neural signal transmission":*  

* Protection against unauthorized access: detailed access and rights management via the university IdM system
* Data recovery: backups: Automated backups to RRZE (local IT centre) servers


*Dataset "EEG measurements":*  

* [Protection against unauthorized access: detailed access and rights management via the university IdM system]
* [Data robustness: encryption: transfer of data between systems only in encrypted form]
* [Data recovery: backups: Encrypted and fully anonymised dataset is backed up at RRZE (IT Centre) servers. The local secure RAID system provides redundancy for sensitive data during all stages of data handling.]
* [Sensitive data: anonymisation or pseudonymisation: access to sensitive (not fully anonymous) data is only possible on a computer physically located in the rooms of the research group]
* [Other: The PI will ensure that all members of the group are comfortable with the established data handling procedures, in particular regarding sensitive information, as well as the use of the backup service and documentation of all data handling and versioning steps.]




### Potential reuse and long-term prospects

**Which individuals, groups or institutions could be interested in re-using this dataset? What are possible scenarios? What consequences
does the reuse potential have for the provision of the data later?**

*Dataset "Simulations of neural signal transmission":*  While substantial parts of the dataset are published and therefore available to the broad public, all data is additionally stored locally for at least 10 years. This is also recommended by the DFG codex and the university research data policy. The data can be used to verify our research results from the ground up, but are less useful for new research. The software code and models, however, may be employed in various studies / settings or be used as a baseline for future models.

*Dataset "EEG measurements":*  All data are stored locally for at least 10 years. This is also recommended by the DFG codex and the university research data policy. Primarily, the published (anonymous) data support the transparency of the research. Reusing the data in different research contexts requires the full dataset (pre-anonymisation), which can only be made available upon detailed ethical approval (see section 2 Accessibility).

### Costs

**What personnel and material costs are associated with data
management in the project (e.g., in terms of data documentation,
creation of metadata, assignment of persistent identifiers, and
long-term archiving)?**

The institutional repository is free-of-charge. This is sufficient for most of our purposes. The larger datasets are stored in \{XYZ\} (also free-of-charge) and the local data archive. The costs associated with the archive are small (1000€ / a) for these data volumes. The detailed description and documentation of the datasets are part of the normal research workflow. Additional personnel resources for making data FAIR are therefore minimal (3 PM).

**How will the data management costs of the project be covered?**

Costs beyond basic institutional services will be covered via the project RDM budget. It amounts to approximately 5% of the whole project funding volume - as recommended by the European Commission in "Turning FAIR into reality : final report and action plan from the European Commission expert group on FAIR data" (https://data.europa.eu/doi/10.2777/1524).
