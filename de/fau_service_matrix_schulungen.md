# FAU Service-Matrix Schulungen

| Nummer  | Thema | FAU Competence Center for Research Data and Information | RRZE (Rechenzentrum) | CSC | Universitätsbibliothek | Informatik | Datenschutzbeauftragte (FAU / Klinikum) | MIK & Med. Infomatik | Graduiertenzentrum | S-Forschung | Erfinderberatung und  Patentmanagement | Extern bzw.  GRK-bezogen |
|:---:|---|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| 1 | Forschungsdaten und Datenmanagement  - Begriffsklärung | X |  |  | X |  |  |  | X |  |  |  |
| 2 | Policies und Forderungen auf  lokaler, nationaler und internationaler Ebene | X |  |  | X |  |  |  |  | X |  |  |
| 3 | Datenmanagementpläne |  |  |  |  |  |  |  |  |  |  |  |
| 3.2 | Grundlagen |  |  |  | X |  |  |  | X |  |  |  |
| 3.2 | Anforderungen (Förderer, fachlich, ...)  |  |  |  | X |  |  |  |  | X |  |  |
| 3.3 | Werkzeuge (RDMO) |  |  |  | X |  |  |  |  |  |  |  |
| 4 | Organisation, Ordnung und Struktur |  |  |  |  |  |  |  |  |  |  |  |
| 4.1 | Grundlagen |  | X |  | X |  |  |  |  |  |  |  |
| 4.2 | Best Practices (fachlich / datenspezifisch) | X |  | X |  |  |  |  |  |  |  |  |
| 4.3 | Datenqualität / Management | X |  | X |  | X |  |  |  |  |  |  |
| 4.4 | Werkzeuge und Software (siehe Detailliste) | (X) | (X) |  |  | (X) |  | (X) |  |  |  | X |
| 4.5 | |  |  |  |  |  |  |  |  |  |  |  |
| 5 | Speichern und Backup sowie Zugriffssicherheit | X | X |  |  |  |  |  |  |  |  |  |
| 6 | Dokumentation und Metadaten |  |  |  |  |  |  |  |  |  |  |  |
| 6.1 | Grundlagen Dokumentation | X |  |  | (X) |  |  |  |  |  |  |  |
| 6.2 | Grundlagen Metadaten | X |  |  | X | (X) |  |  |  |  |  | X |
| 6.3 | Fachspezifische / forschungsspezifische Schemata Best Practices |  |  | (X) |  | (X) |  |  |  |  |  | X |
| 7 | Langzeitsicherung / -archivierung | X | X |  |  |  |  |  |  |  |  |  |
| 8 | Publikation von Daten |  |  |  |  |  |  |  |  |  |  |  |
| 8.1 | Grundlagen |  |  |  | X |  |  |  |  |  |  |  |
| 8.2 | Services vor Ort | X |  |  |  |  |  |  |  |  |  |  |
| 8.3 | Repositorien  |  |  |  | X |  |  |  |  |  |  |  |
| 9 | Rechtliche Aspekte |  |  |  |  |  |  |  |  |  |  |  |
| 9.1 | Urheberrecht bei Daten |  |  |  | X |  |  |  |  |  |  |  |
| 9.1.1 | Lizenzen und Nachnutzung |  |  |  | X |  |  |  |  |  |  |  |
| 9.1.2 | Bildrecht |  |  |  | X |  |  |  |  |  |  |  |
| 9.2 | Social Media Daten und Data Mining  (rechtliche Fragen) |  |  |  | (X)  |  | (X) |  |  |  |  |  |
| 9.3 | Patentrechtliche Aspekte |  |  |  |  |  |  |  |  |  | X (Teil der Patentschulung) |  |
| 9.4 | Datenschutzaspekte |  |  |  |  |  |  |  |  |  |  |  |
| 9.4.1 | Grundlagen des Datenschutzes,  Anonymisierung, Pseudonymisierung |  |  |  | X |  | X |  |  |  |  |  |
| 9.4.2 | Informierte Einwilligung |  |  |  |  |  | X |  |  |  |  |  |
| 9.4.3 | Datenschutz in internationalen Projekten |  |  |  |  |  | X |  |  |  |  |  |
| 9.4.4 | Umgang mit medizinischen Daten |  |  |  |  |  | X | X |  |  |  |  |
| 9.5 | Spezielle Aspekte (Grabungsrecht, …) |  |  |  |  |  |  |  |  |  |  | X |
| 10 | Besondere Infrastruktur / Software-Tools vor Ort |  |  |  |  |  |  |  |  |  |  |  |
| 10.1 | HPC-System / Computing |  | X | X |  |  |  |  |  |  |  |  |
| 10.2 | Elektronisches Laborbuch | X |  | X |  |  |  | X |  |  |  | X |
| 10.3 | Webcrawler / Harvesting | (X) | (X) |  | (X) |  |  |  |  |  |  |  |
| 10.4 | WissKI / CIDOC-CRM | X |  |  |  |  |  |  |  |  |  |  |
| 10.5 | Sonstiges (siehe Anfragenliste) | (X) |  |  |  |  |  |  |  |  |  | X |
