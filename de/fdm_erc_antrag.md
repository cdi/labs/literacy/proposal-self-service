# FDM im ERC-Antrag

<!-- Inhaltsverzeichnis für die GitLab-Version, muss für eine PDF-Version gelöscht/auskommentiert werden -->
[[_TOC_]]

## Allgemeine Regeln und Empfehlungen

<!-- TO DO: E-Mail-Adresse der FDM-Verantwortlichen der Einrichtung. -->
Bitte kontaktieren Sie möglichst früh im Antragsprozess das **Forschungsdatenmanagement-Team** der FAU: <forschungsdaten@fau.de>.

Das Forschungsdatenmanagement sollte in die Projektplanung und Antragstellung einfließen.

Für **ERC-Anträge** beachten Sie bitte die Website zu [Open Science](https://erc.europa.eu/managing-project/open-science) des ERC, den Abschnitt zu Open Science im „[ERC Work Programme 2022](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/horizon/wp-call/2022/wp_horizon-erc-2022_en.pdf)" und „[Open Research Data and Data Management Plans. Information for ERC grantees](https://erc.europa.eu/sites/default/files/document/file/ERC_info_document-Open_Research_Data_and_Data_Management_Plans.pdf)".
Der Leitfaden enthält spezifischere Hinweise zu Repositorien und Metadaten für die Lebenswissenschaften, die Sozial- und Geisteswissenschaften sowie zu Physik und Ingenieurwissenschaften.

Je nach Ausschreibung kann es weitere Forderungen in Bezug auf Open Science und Datenmanagement geben.
Beachten Sie bitte alle relevanten Dokumente und Hinweise.

Im **Antragsformular** gibt es keinen gesonderten Abschnitt zu Open Science-Praktiken in Ihrem Projekt. Sie sollten dennoch kurz (2-3 Sätze) in **Part B2 b. Methodology** auf Ihre Forschungsdaten und **Open Science-Praktiken** Bezug nehmen.
Für die EU wichtige Open-Science-Praktiken sind:

- Daten sollten nach den **[FAIR-Prinzipien](https://www.go-fair.org/fair-principles/)** behandelt werden, so **"offen wie möglich – so beschränkt wie nötig"** in einem vertrauenswürdigen **Repositorium** geteilt werden: wenn möglich mit **CC BY**, **CC0** Lizenz oder ähnlich.
- Falls Forschungsdaten nicht zur Verfügung gestellt werden können, muss dies im Datenmanagementplan erklärt und begründet werden. Sie müssen dennoch nach den [FAIR-Prinzipien](https://www.go-fair.org/fair-principles/) verwaltet werden und **FAIRe Metadaten** (mit bibliografischen und administrativen Informationen) müssen mit einer CC0 Lizenz in einem maschinenlesbaren Format veröffentlicht werden.
- **Metadaten**: Als Metadaten-Basisstandard gilt [Dublin Core](https://dublincore.org/). Die Referenz zur ERC-Förderung muss enthalten sein. Für die geforderten bibliografischen und administrativen Angaben, siehe Horizon Europe [Model Grant Agreement](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/common/agr-contr/general-mga_horizon-euratom_en.pdf) (Annex 5, Artikel 17): Informationen über Datensätze, Horizon Europe Funding, Projektname, Akronym und Nummer, Lizenz, persistente Identifikatoren für die Datensätze (z. B. DOI), die Autoren (z. B. [ORCID iD](https://orcid.org/)) und falls möglich für Ihre Institution (z. B. [ROR ID](https://ror.org/)). Gegebenenfalls müssen die Metadaten auch persistente Identifikatoren zu dazugehörigen Publikationen und anderen Forschungsergebnissen beinhalten. Metadaten sollten mit CC0 Lizenz und persistentem Identifikator veröffentlicht werden.

Alle Daten generierenden ERC-Projekte müssen spätestens **6 Monate nach Projektstart** einen **Datenmanagementplan** vorlegen und während der Projektlaufzeit aktualisieren. Ein [DMP-Template für ERC-Anträge](https://erc.europa.eu/sites/default/files/document/file/ERC-Data-Management-Plan.docx) wird zur Verfügung gestellt.

**Kosten** für das Datenmanagement können beantragt werden.

***Nach der Bewilligung des Projektes bitte die nachfolgenden Punkte zum Datenmanagementplan und zur NFDI beachten.***

<!-- für PDF-Version: -->
<!-- \newpage -->

## Datenmanagementplan

Erstellen Sie einen Datenmanagementplan für auffindbare, zugängliche, interoperable, wiederverwendbare und öffentlich verfügbare Forschungsdaten und aktualisieren Sie ihn regelmäßig während der Projektlaufzeit. Geförderte Projekte sollten aufzeigen, dass ihr Datenmanagement den [FAIR-Prinzipien](https://www.go-fair.org/fair-principles/) folgt.

Für bewilligte ERC-Projekte wird ein **[DMP-Template](https://erc.europa.eu/sites/default/files/document/file/ERC-Data-Management-Plan.docx)** zur Verfügung gestellt.
<!-- TO DO: Ggf. Link zur RDMO-Instanz ersetzen oder ein anderes DMP-Tool erwähnen. -->
<!-- Bei der Erstellung des DMPs für Horizon Europe kann der entsprechende **[RDMO-Fragenkatalog](https://rdmo.ub.fau.de/)[^RDMO]** genutzt werden.-->

- **Beschreibung** der Datensätze: wissenschaftlicher Fokus, technische Herangehensweise, Datentypen, -volumen.
- **Standards** und Metadaten.
- Name und **persistente Identifikatoren** der Datensätze.
- **Kuratierungs- und Erhaltungsmaßnahmen**: Standards zur Sicherung der Datenintegrität, Ablage in vertrauenswürdigem Repositorium.
- **Datensicherheit**: Backup-Maßnahmen, Maßnahmen zur Behandlung sensibler Daten.
- **Datenverfügbarkeit**: Zugang zu Daten zwecks Validierung ist zu gewährleisten; Nutzungslizenzen, Zugangsbeschränkungen, Zeitpunkt der Veröffentlichung, Dauer. Daten sollten so früh wie möglich nach der Generierung, spätestens bei Projektende veröffentlicht werden. Mit einer Publikation verbundene Daten sollten mit der Publikation zugänglich gemacht werden. Nicht nur Publikationen und Daten sollten offen verfügbar gemacht werden, sondern möglichst auch Software, Modelle, Apps, Algorithmen, Protokolle, Workflows, Elektronische Laborbücher etc., die im Forschungsprojekt entwickelt und/oder verwendet werden. Auch diese sollten im DMP berücksichtigt werden.
- **Ethische und rechtliche Aspekte**, zum Beispiel Informierte Einwilligungserklärung.
- **Kosten des Datenmanagements** und **Verantwortlichkeiten**: Kosten zur Generierung und/oder Nachnutzung von Daten, der Datendokumentation, der Datensicherung, der Veröffentlichung; Verantwortliche des Datenmanagements und der Qualitätssicherung.

## NFDI

Gegebenenfalls sollte mit den [NFDI-Konsortien](https://www.nfdi.de/konsortien/) Kontakt aufgenommen werden. Die **N**ationale **F**orschungs**D**aten**I**nfrastruktur ist eine Großinitiative des Bundes und der Länder, um (meist fachbezogene) Dienste und Best Practices im Datenmanagement zu entwickeln und zu etablieren. <!-- TO DO: Ggf. die Beteiligung der Einrichtung an NFDI-Konsortien erwähnen. --> Bei 11 von 27 Konsortien ist die FAU beteiligt:

- [DAPHNE4NFDI](https://www.daphne4nfdi.de/index.php) (Photonen- und Neutronenexperimente - Beugung, Tomographie, Bildgebung, Spektroskopie)
- [FAIRmat](https://www.fairmat-nfdi.eu/fairmat/) (Kondensierte Materie und chemische Physik)
- [KonsortSWD](https://www.konsortswd.de/) (Sozial-, Verhaltens- und Wirtschaftswissenschaften)
- [MaRDI](https://www.mardi4nfdi.de/) (Mathematik)
- [MatWerk](https://nfdi-matwerk.de/) (Material- und Werkstoffwissenschaft)
- [NFDI4Cat](https://nfdi4cat.org/) (Katalyseforschung)
- [NFDI4Culture](https://nfdi4culture.de/) (Materielle und immaterielle Kulturgüter)
- [NFDI4Earth](https://www.nfdi4earth.de/) (Erdsystemforschung - Geowissenschaften)
- [NFDI4Energy](https://nfdi4energy.uol.de/) (Interdisziplinäre Energiesystemforschung)
- [NFDI4Microbiota](https://nfdi4microbiota.de/) (Lebenswissenschaften)
- [NFDI4Objects](https://www.nfdi4objects.net/) (Materielle Hinterlassenschaften der Menschheitsgeschichte)

<!-- TO DO: Ggf. durch vermittelnde Einrichtung ersetzen. -->
Den Kontakt vor Ort stellt auch gerne das [FAU Competence Center for Research Data and Information (CDI)](https://www.cdi.fau.de/) her.

Unterstützung beim Umgang mit Forschungsdaten durch die beteiligten Institutionen sollte im Grant Agreement erwähnt werden.
