# Fachspezifisches FDM im DFG-Antrag

<!-- Inhaltsverzeichnis für die GitLab-Version, muss für eine PDF-Version gelöscht/auskommentiert werden -->
[[_TOC_]]

Es gibt teilweise sehr genaue, umfangreiche, [fachspezifische
Anforderungen](https://www.dfg.de/foerderung/grundlagen_rahmenbedingungen/forschungsdaten/empfehlungen/index.html),
die in den allgemeinen Antragsformularen nicht gut referenziert sind. Dieses Dokument soll als Orientierungshilfe dienen, es ist jedoch unerlässlich die Dokumente der Fachkollegien für Details zu konsultieren.

<!-- TO DO: Gegebenenfalls löschen -->
Zur Information: In der [Repositorien-Liste](de/repositorien.md) finden Sie einen Überblick zu fachspezifischen Repositorien.

## Geistes- und Sozialwissenschaften

### Theologie (Editionsvorhaben)

*Referenz: [Fachkollegium "Theologie": Handreichung zu Editionsvorhaben](https://www.dfg.de/resource/blob/175758/06d80235b00decf7cae090b54bb70123/fachkollegium-107-editionen-data.pdf) (2022).*

Die Edition sollte **Open Access** veröffentlicht werden; eine Zugangsbeschränkung zu den Daten muss im Antrag begründet werden.

Das Fachkollegium empfiehlt die Beachtung der Hinweise und Kriterium aus den **[Förderkriterien für wissenschaftliche Editionen in der Literaturwissenschaft](https://www.dfg.de/resource/blob/172080/895fcc3cb72ec6cd1d5dc84292fb2758/foerderkriterien-editionen-literaturwissenschaft-data.pdf)** (2015) des Fachkollegiums "Literaturwissenschaft.
Siehe hierzu auch den Abschnitt [Literaturwissenschaften (wissenschaftliche Editionen)](#literaturwissenschaften-wissenschaftliche-editionen).

### Psychologie

*Referenz: [Management und Bereitstellung von Forschungsdaten in der Psychologie: Überarbeitung der DGPs-Empfehlungen](https://doi.org/10.31234/osf.io/hcxtm) (2020).*

Es wird zwischen **Roh-, Primär-, und Sekundärdaten** unterschieden. Wenn keine rechtlichen oder forschungsethischen Gründe entgegenstehen, sollten immer offene beziehungsweise frei zugängliche Primärdaten veröffentlicht werden. Ist dies nicht möglich, sollten zumindest die Metadaten offen und zugänglich sein.
[Der RatSWD stellt Informationen zum Thema Forschungsethik im Forschungsprozess zur Verfügung: [Sammlung Best-Practice Forschungsethik](https://www.konsortswd.de/ratswd/best-practice-forschungsethik/).]

Als übergreifende **Metadatenstandards** werden [DDI](https://ddialliance.org/) oder [Dublin Core](https://dublincore.org) empfohlen. Weitere disziplinspezifische Informationen zu Metadaten befinden sich auf der Webseite des Datamanagement-Werkzeugs [DataWiz](https://datawizkb.leibniz-psychology.org/index.php/during-data-collection/what-should-i-know-about-metadata/), das vom ZPID bereitgestellt wird.

<!--TO DO: Gegebenfalls "BayDSG" durch das jeweilige Landesgesetz ersetzen -->
Die relevanten Abschnitte der **DSVGO** und des **BayDSG** sollten beachtet werden, siehe hierzu auch die Empfehlungen von [RatSWD](https://doi.org/10.17620/02671.7).

Als **Fach-Repositorien** werden [PsychArchives](https://www.psycharchives.org/) vom ZPID oder [datorium](https://data.gesis.org/sharing/#!Home) bei GESIS empfohlen.

### Erziehungswissenschaften, Bildungswissenschaften, Fachdidaktik

*Referenz: [Empfehlungen zur Archivierung, Bereitstellung und Nachnutzung von Forschungsdaten im Kontext erziehungs- und bildungswissenschaftlicher sowie fachdidaktischer Forschung](https://www.dfg.de/resource/blob/174560/67a06609aa9aaa98e73b9b7d798afbb9/stellungnahme-forschungsdatenmanagement-data.pdf) (2020).*

*Referenz: [Bereitstellung und Nutzung quantitativer Forschungsdaten in der Bildungsforschung: Memorandum des Fachkollegiums "Erziehungswissenschaften" der DFG](http://dx.doi.org/10.25656/01:11505) (2015).*

Ein **Datenmanagementplan** sollte in der Planungs-/Antragsphase erstellt und während der Projektphase weiter gepflegt werden. <!-- TO DO: Eventuell auf eine RDMO-Instanz oder ein anderes DMP-Tool verweisen. -->Hier kann der [RDMO-Fragenkatalog](https://rdmo.ub.fau.de/) „Erziehungswissenschaften“ helfen.
Weisen Sie im Antrag explizit darauf hin, dass ein Datenmanagementplan erstellt wurde und gepflegt wird.

Falls **ähnliche Forschungsdaten schon vorhanden** sind, sollten Sie nachweisen, warum eine Nachnutzung nicht möglich oder sinnvoll ist.

Überlegungen zur **Archivierung und Bereitstellung** sollten Teil des Antrags sein, auch wenn keine Nachnutzung ermöglicht werden kann. Die begründete Auseinandersetzung mit der Thematik kann als Qualitätskriterium für die Begutachtung herangezogen werden.

Für **selbst erzeugte Daten** wird erwartet, dass Sie das **Nachnutzungspotenzial** Ihrer Daten gegen dem **Bereitstellungsaufwand** abwägen, unter folgenden drei Konsequenzen auswählen und dies begründen:

- Archivierung und auf Nachfrage nutzerfreundliche Bereitstellung direkt durch den Datenproduzenten zwecks Prüfung publizierter Ergebnisse (im Sinne guter wissenschaftlicher Praxis).
- Archivierung, erweiterte Dokumentation (Codebuch) und auf Nachfrage / Antrag nutzerfreundliche Bereitstellung direkt durch die Datenproduzenten zwecks weiterführender wissenschaftlicher Analysen.
- Gut dokumentierte Übergabe an ein Forschungsdatenzentrum zwecks Archivierung und allgemeiner Bereitstellung an die Scientific Community nach den Regularien des jeweiligen Forschungsdatenzentrums.

Beachten Sie dabei auch **rechtliche und ethische Hinderungsgründe** (Datenschutz, Urheberrecht, Verträge).

<!-- TO DO: Durch Link zu den Datenschutzbeauftragten der Einrichtung austauschen -->
Halten Sie Rücksprache mit den [Datenschutzbeauftragten](https://www.fau.de/fau/leitung-und-gremien/gremien-und-beauftragte/beauftragte/datenschutzbeauftragter/) und gegebenenfalls der [Ethikkommission](https://www.dgfe.de/service/ethik-kommission).

Für **Einverständniserklärungen** über Primärforschung und Sekundärnutzung der Daten kann auf die Expertise der Forschungsdatenzentren (FDZ) oder der lokalen Ethikkommission zurückgegriffen werden. Der VerbundFDB stellt zum Beispiel [Informationen zur Informierten Einwilligung, zur Erklärung zum Datenschutz bei Surveys und zur Aufklärung von Kindern bei Studien](https://www.forschungsdaten-bildung.de/einwilligung) zur Verfügung.
[Der RatSWD stellt Informationen zum Thema Forschungsethik im Forschungsprozess zur Verfügung: [Sammlung Best-Practice Forschungsethik](https://www.konsortswd.de/ratswd/best-practice-forschungsethik/).]

Falls Sie **personenbezogene Daten** erheben, sollten Sie darüber Auskunft geben, ob und zu welchem Zeitpunkt der Studie die Daten anonymisiert/pseudonymisiert werden.
Sie sollten auch darlegen, in welchem Umfang gegebenenfalls die "Informierte Einwilligungserklärung" eingeholt wird: Analyse/Nutzung im Projekt oder Analyse/Nutzung im Projekt und Nachnutzung.
Falls keine "Informierte Einwilligungserklärung eingeholt wird, begründen Sie dies.

Geben Sie an, ob und welche nicht-personenbezogenen **sensiblen Daten** erhoben werden.

Legen Sie dar, ob an Ihren Daten **Urheberrechte oder andere Schutzrechte** be- oder entstehen. Nennen Sie die Lizenzen unter denen nachgenutzte Daten veröffentlicht wurden.

Die Empfehlungen verweisen auf Hinweise zur **Datenaufbereitung** beim [VerbundFDB](https://www.forschungsdaten-bildung.de/publikationsreihen).

Der RatSWD bietet eine Übersicht zu **[Datenzentren](https://www.konsortswd.de/datenzentren/alle-datenzentren/)** im Bereich der Sozialwissenschaften und verwandter Disziplinen, der [VerbundFDB](https://www.forschungsdaten-bildung.de/daten-teilen) ist Anlaufstelle für empirische Bildungsforschung. Hier kooperieren das DIPF, GESIS, IQB, Qualiservice und leibniz-psychology.org.

Gerade in den Erziehungswissenschaften sind **ethische oder rechtliche Schranken** an den Zugang oft nötig und einige Forschungsdatenzentren bieten Optionen an. Siehe hierzu auch die [Empfehlungen der DGfE, GEBF und GFD (Abschnitt 3.4)](https://www.dgfe.de/fileadmin/OrdnerRedakteure/Stellungnahmen/2020.03_Forschungsdatenmanagement.pdf).

Falls Sie Daten **nicht veröffentlichen oder archivieren**, sollten Sie dies begründen und dabei zwischen rechtlichen, ethischen und freiwilligen Einschränkungen unterscheiden. Beispiele für ethische Aspekte finden Sie in den [Empfehlungen der DGfE, GEBF und GFD (Seite 13)](https://www.dgfe.de/fileadmin/OrdnerRedakteure/Stellungnahmen/2020.03_Forschungsdatenmanagement.pdf).  

Die Entscheidung gegen **Nachnutzungsmöglichkeiten** entbindet nicht von der Verantwortung einer angemessenen Archivierung der Daten.

Sie sollten möglichst **fachspezifische Daten und Metadatenformate** verwenden.

Bei Zeitschriften oder in Repositorien veröffentlichte Daten sollten von einer aussagekräftigen **Dokumentation** begleitet sein, um die Daten verstehen und nachnutzen zu können.
Eigens entwickelte oder angepasste **Instrumente zur Datenerhebung** sollten zugänglich gemacht, bereits publizierte Instrumente zitiert werden.

Datensätze, die einzigartig sind oder nur unter unverhältnismäßigem Aufwand reproduziert werden können, sollten **langfristig archiviert** werden. Dies wäre bei [Zenodo](https://zenodo.org/), [RADAR](https://www.radar-service.eu/radar/de/home) oder den [VerbundFDB-Zentren](https://www.forschungsdaten-bildung.de/daten-teilen) meist sichergestellt.
Die FAU hat eine [Zenodo-Community](https://zenodo.org/communities/fau/?page=1&size=20) und bietet einen [Zugang zu RADAR](https://ub.fau.de/forschen/daten-software-forschung/archivierung-und-publikation-der-daten/#collapse_0). <!-- TO DO: Ggf. anpassen oder löschen --> Beachten Sie, dass nur einige ausgewählte Zentren personenbezogene Daten akzeptieren. Wenn Sie nicht-anonyme Daten archivieren müssen und nicht sicher sind, welche Zentren geeignet sind, wenden Sie sich an den lokalen FDM-Support.

Geben Sie an welche **Personalressourcen** für die Aufbereitung und ausführliche Dokumentation für zur Nachnutzung bereitgestellte Daten entstehen.
Geben Sie auch an, welche **Kosten für die Archivierung und Bereitstellung** der Daten entstehen (Datenarchivgebühren, Publikationspauschale usw.).

### Soziologie, Politik- und Kommunikationswissenschaft

*Referenz: [Stellungnahme des Fachkollegiums 111 „Sozialwissenschaften“ zum Forschungsdatenmanagement in der Soziologie, der Politikwissenschaft und der Kommunikationswissenschaft](https://www.dfg.de/resource/blob/174570/f311ff26e5a3c4b427271e0ba76a2ced/fachkollegium111-forschungsdatenmanagement-data.pdf) (2020).*

Die **Reproduzierbarkeit** und Analyse der Forschungsergebnisse sollte ermöglicht werden.

Es sollten möglichst standardisierte, nicht-proprietäre und allgemein beziehungsweise in der Community verbreitete **Datenformate** genutzt werden, um die Nachnutzung und Langzeitarchivierung zu ermöglichen.
Siehe hierzu auch detaillierte Erläuterungen im [nestor Handbuch](http://nestor.sub.uni-goettingen.de/handbuch/).

Verwenden Sie möglichst fachspezifische **Metadatenformate**.
Für Umfragedaten wäre dies zum Beispiel der [DDI-Metadatenformat](https://ddialliance.org/), für die Markt-, Meinungs- und Sozialforschung
[ISO 20252:2019](https://www.iso.org/standard/73671.html) und für statistische Aggregatdaten ist der [SDMX (Statistical Data and Metadata Exchange)](https://sdmx.org/) in Verwendung.

Die DFG erwartet eine Verfügbarkeit der Daten für mindestens 10 Jahre.
Eine möglichst langfristige **Archivierung** über 10 Jahre hinaus ist anzustreben und die Nicht-Archivierung eines Datensatzes ist zu begründen.

Eine **Nicht-Weitergabe** der Daten ist möglich, wenn die Veröffentlichung der Daten die Erhebung gefährden würde, oder weil vulnerable Personengruppen oder Gruppen, die sich am Rande der Legalität oder jenseits der Legalität bewegen, beforscht werden.
Dies ist im Antrag zu reflektieren und das Vorgehen ist zu begründen.

Empfohlen wird die Nutzung von zertifizierten **Datenrepositorien** oder institutionellen Repositorien.
Fachliche Repositorien sind zum Beispiel [Qualiservice](https://www.qualiservice.org/de/) für qualitative sozialwissenschaftliche Forschungsdaten und
[SowiDaNet|datorium](https://data.gesis.org/sharing/#!Home) für quantitative Daten aus den Sozial- und Wirtschaftswissenschaften.
Für die Archivierung können **Finanzmittel** beantragt werden.

### Alte Kulturen (Ägyptische und Vorderasiatische Altertumswissenschaften, Klassische Archäologie, Ur- und Frühgeschichte, Klassische Philologie, Alte Geschichte)

*Referenz: [Fachkollegium „Alte Kulturen“ - Handreichung zum Umgang mit Forschungsdaten](https://www.dfg.de/resource/blob/174204/c5714c685b19ff59f7dd7322c24f27b1/handreichung-fachkollegium-101-forschungsdaten-data.pdf) (2020).*

*Bitte beachten Sie je nach Ausrichtung auch andere DFG-Empfehlungen, zum Beispiel die Empfehlungen für [Wissenschaftliche Editionen](#literaturwissenschaften-wissenschaftliche-editionen) oder [Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaften](#sozial--und-kulturanthropologie-außereuropäische-kulturen-judaistik-und-religionswissenschaften).*

Die DFG weist explizit auch auf die Empfehlungen von [IANUS (Datenzentrum für Archäologie & Altertumswissenschaften)](https://www.ianus-fdz.de/it-empfehlungen/) hin.

Für datenintensive Projekte wird ein **Datenmanagementplan** erwartet. Der [RDMO-Fragenkatalog](https://rdmo.ub.fau.de/) „Alte Kulturen“ kann hier hilfreich sein. <!-- TO DO: Eventuell auf eine RDMO-Instanz oder ein anderes DMP-Tool verweisen. -->
Der Datenmanagementplan kann dem Antrag beigelegt werden, auch wenn er nicht Gegenstand der Begutachtung ist.
In jedem Fall sollte der geplante Datenmanagementplan erwähnt werden.

Der Umgang mit Forschungsdaten sollte möglichst konkret und verbindlich im Antrag erläutert werden, das heißt wesentliche Informationen zu:

Die **Art und der Umfang** der Daten sollte dargestellt werden.
Geben Sie an, ob die Daten rein analog, rein digital oder in beiden Formen vorliegen und welche Formate in unterschiedlichen Phasen verwendet werden.
Datentypen könnten zum Beispiel 3D-Modellierungen, qualitative Interviews (.wav, .mp4), Transkription alter Urkunden (TEI P5), Aufnahmen von Kunstwerken (.tif, .jpeg2000, .png) sein.  

Es wird eine **voraus**gehende verbindliche **rechtliche Klärung** erwartet: offene Fragen und besondere Herausforderungen müssen ausdrücklich erläutert werden.
Unterlagen zur Dokumentation der rechtlichen Klärung, zum Beispiel **Zugang** zu Beständen von Museen, Bibliotheken und Archiven oder Zugänglichkeiten zu Grabungsstätten, sind dem Antrag beizufügen.
Regelungen können auch Datenschutzfragen, Urheberrechtliche Fragen und Bildrechte betreffen.
Entsprechende Genehmigungen sollte dem Antrag beiliegen.
Ungeklärte Fragen sollten explizit im Antrag erwähnt werden. Eine Begründung, warum die Genehmigung noch aussteht und ein Hinweis auf Fristen bis zur erwarteten Erlaubnis, ist sinnvoll.
<!-- TO DO: Die betreffenden Kontakte ersetzen. -->
Wenden Sie sich bei Fragen des Urheberrechts an die [Universitätsbibliothek](https://ub.fau.de/forschen/daten-software-forschung/) und bei datenschutzrechtlichen Fragen an den [Datenschutzbeauftragten](https://www.fau.de/datenschutz/) **vor** der Datensammlung.

Die **Aufbereitung, Qualitätssicherung und Dokumentation** der Daten sollte erläutert werden: die Orientierung an **Standards** und **Best Practices** ist ausdrücklich zu erwähnen.
Beachten Sie Best-Practice-Handbücher und Leitfäden für den Umgang mit Daten: [Leitfäden des EU-Projekts ARCHES](https://www.europae-archaeologiae-consilium.org/eac-guidlines), [IT-Hinweise von IANUS](https://www.ianus-fdz.de/it-empfehlungen/) und [Best Practices des Archeology Data Service, UK](https://archaeologydataservice.ac.uk/help-guidance/guides-to-good-practice/).
Falls keine fachspezifischen Standards vorliegen, sollte dies erwähnt werden.
Bestehende Lösungen wie Datenbankanwendungen oder digitale Werkzeuge sollten übernommen und angepasst werden.
Falls eigene Lösungen entwickelt werden, muss begründet werden, warum kein bestehendes Werkzeug angepasst oder direkt verwendet werden kann.

Falls möglich sollten für die **Dokumentation** standardisierte Metadatenformate oder kontrollierte Vokabulare (zum Beispiel GeoNames) verwendet werden.
Siehe auch die Hilfestellungen von IANUS zu [Metadaten](https://www.ianus-fdz.de/it-empfehlungen/?q=node/53) und zu [kontrolliertem Vokabular](https://ianus-fdz.de/it-empfehlungen/projektphasen/dokumentation/kontrollierte-vokabulare-thesauri-und-normdaten/).
Falls es für den Datentyp keine etablierten Standards gibt, sollten Sie dies erwähnen.

**Daten aus prinzipiell gar nicht oder nur sehr eingeschränkt wiederholbaren Untersuchungen** sollten sicher archiviert und für die spätere Überprüfung oder Nachnutzung aufbereitet werden.

Die **Speicherung und Archivierung** der Daten sollte dargestellt werden:
Die DFG erwartet eine Verfügbarkeit der Daten für mindestens 10 Jahre.
Eine möglichst langfristige Archivierung über 10 Jahre hinaus ist anzustreben und die Nicht-Archivierung eines Datensatzes ist zu begründen.
Die Nutzung von zertifizierten Datenrepositorien oder institutionellen Repositorien wird empfohlen.
Fachliche Repositorien sind zum Beispiel der [Archaeology Data Service](https://archaeologydataservice.ac.uk/), [arthistoricum](https://www.arthistoricum.net/), [Arachne](https://arachne.dainst.org/) und [propylaeum](https://www.propylaeum.de/).

Für folgende Daten ist unbedingt eine **Langzeitarchivierung** vorzusehen:

- Daten zur Dokumentation archäologischer Ausgrabungen und Surveys
- Daten zu Materialanalysen archäologischer Artefakte und Proben
- Daten aus Editionsvorhaben und Materialvorlagen (Volltext, Fotos, Zeichnungen etc.)

Wie Daten archivfähig aufbereitet werden können, finden Sie in den [Archivierungs-Empfehlungen](https://ianus-fdz.de/langzeitarchivierung/) des IANUS-Projektes, das einen Archivierungsdienst aufbaut, und im [nestor Handbuch](http://nestor.sub.uni-goettingen.de/handbuch/).

Die **Nachnutzung** sollte ermöglicht werden: Planungen zur Bereitstellung und Zitierbarkeit der Daten sind im Antrag zu vermerken.
Falls die Daten über eine Projektwebseite und nachgeschalteter Datenbank oder auf persönliche Anfrage bereitgestellt werden, sollte die eindeutige **Referenzierbarkeit** sichergestellt werden und im Antrag dargestellt werden.
Auch sollte Stellung dazu genommen werden, wie der Zugang über das Projektende hinaus sichergestellt wird.
Ein möglichst vollständiger und früher **Zugang** ist anzustreben, spätestens im Zusammenhang mit der Publikation der Ergebnisse.
Falls kein Zugang für Nachnutzung geplant ist, muss dies im Antrag begründet werden.
Gründe sind urheberrechtlicher oder datenschutzrechtlicher Natur, vertragliche Schranken mit der datengebenden Institution oder Gefährdung des Forschungsobjekts durch öffentlich zugängliche (GPS-)Daten.
Zugangs- und Nutzungsbedingungen sollten über eine entsprechende Lizenz (beispielsweise CC BY-SA-NC) geregelt werden.

Benennen Sie, möglichst namentlich, **Verantwortliche** für das Datenmanagement im Projekt und für die Archivierung und Bereitstellung nach Ende des Vorhabens.

### Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaften

*Referenz: [Handreichung des Fachkollegiums 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft zum Umgang mit Forschungsdaten](https://www.dfg.de/resource/blob/173710/045c16e9430426ef321c76dabcbaaf51/handreichung-fachkollegium-106-forschungsdaten-data.pdf) (2019).*

Ein **Datenmanagementplan** ist nicht vorgeschrieben, kann aber in der Planungs- und Projektphase das Forschungsdatenmanagement strukturiert unterstützen. <!-- TO DO: Eventuell auf eine RDMO-Instanz oder ein anderes DMP-Tool verweisen. --> Hier kann der [RDMO-Fragenkatalog](https://rdmo.ub.fau.de/) „Sozial- & Kulturanthropologie, Judaistik, Religionswissenschaft“, der auf diesen Empfehlungen basiert, helfen.

Der Antrag sollte die Beschreibung der **digitalen und analogen Forschungsdaten** und des geplanten Umgangs mit Forschungsdaten während und nach dem Projektende beinhalten.

Im Antrag wird eine **Beschreibung Ihrer Daten und Kontextinformationen** erwartet.
Beschreiben Sie die erzeugten Datentypen und Datenformate, zum Beispiel Transkription alter Urkunden (TEI P5), Aufnahmen von Kunstwerken (.tif, .jpeg2000, .png), qualitative Interviews (.wav, .mp4).
Verwenden Sie möglichst standardisierte, nicht-proprietäre und fachlich verbreitete Formate um die Nachnutzung und Langzeitarchivierung zu ermöglichen.

Hinsichtlich der **Metadaten** sollten Sie möglichst (Fach-)Standards verwenden.

**Datenschutzrechtliche und ethische Fragen** sind zu benennen und Richtlinien von Fachgesellschaften sind heranzuziehen.
[Der RatSWD stellt Informationen zum Thema Forschungsethik im Forschungsprozess zur Verfügung: [Sammlung Best-Practice Forschungsethik](https://www.konsortswd.de/ratswd/best-practice-forschungsethik/).]

Die **Aufbewahrungsorte** von physischen Quellen der Daten oder Materialien sollten insbesondere bei historischen oder philologischen Anträgen benannt werden.

Falls Ihre Daten, die Edition, die digitale Bereitstellung und Sichtbarmachung der materiellen Dokumente einschließen, muss die **Erlaubnis der Rechte-Inhaber** (Archive, Bibliotheken, Museen, Privatpersonen, Sammlungen) **vorab** geklärt werden und im Antrag dokumentiert werden.

Falls personenbezogene Daten erhoben oder bearbeitet werden, sollte im Antrag Auskunft über **Informierte Einwilligungserklärungen** gegeben werden.
Falls auf eine **Informierte Einwilligungserklärung** verzichtet wird, ist dies zu begründen: zum Beispiel wegen Fristenregelungen, Begrenzung der Zugänglichkeit, Anonymisierung im Erhebungsprozess oder im Anschluss).

Die Daten sollten mindestens 10 Jahre in einer fachlich einschlägigen Infrastruktur oder der eigenen institutionellen Einrichtung in digitaler (im Repositorium) oder analoger (im Archiv) Form **archiviert** werden.
Geben Sie im Antrag an, wo und in welcher Form die Forschungsdaten und Materialien abgelegt werden.
Verwenden Sie möglichst fachspezifische, nicht-proprietäre Formate, um die langfristige Archivierung und Nachnutzung zu erlauben.

Bezüglich der **Datenbereitstellung** sollte die Auswahl der Daten, die Maßnahmen zur Berücksichtigung von Urheberschutz und Datenschutz, die Lizenzen zur Nachnutzung (zum Beispiel CC-Lizenzen), sowie forschungsethische Überlegungen und Zugangsmöglichkeiten erläutert werden.

<!-- TO DO: Den Kontakt entsprechend ersetzen. -->
Bei Fragen zu den rechtlichen Konsequenzen einzelner **Lizenzen** können Sie gerne die [Universitätsbibliothek der FAU](https://ub.fau.de/schreiben-publizieren/daten-software-forschung/) kontaktieren.

Gegebenenfalls gibt es **forschungsethischen Überlegungen**, die die Nachnutzbarkeit der Daten beeinflussen. Beachten Sie hierzu auch die Hinweise der DFG zu ["Wann brauche ich ein Ethik-Votum?"](https://www.dfg.de/foerderung/faq/geistes_sozialwissenschaften/index.html)

Das Fachkollegium 106 betont, dass den **Rechten von Forschenden und an Forschung beteiligter Personen** ein besonderer Schutz gebührt und gegen die Interessen von Nachnutzung abzuwägen sind.

Geben Sie auch Auskunft, ob ein Verfahren für die **Zugriffsbeschränkung** eingesetzt wird und, ob eine Datenzugangskommission benötigt wird. Auch technische Verfahren, die Zugriffe authentifizieren und autorisieren, können an dieser Stelle erwähnt werden.

### Wirtschaftswissenschaften und Sozialwissenschaften

*Referenz: [Management von Forschungsdaten: Was erwartet das Fachkollegium 112 „Wirtschaftswissenschaften“ von Antragsstellenden?](https://www.dfg.de/resource/blob/173412/90ed15de9437140a14c771339979329b/fachkollegium112-forschungsdatenmanagement-de-data.pdf) (2018).*

*Referenz: [Stellungnahme des Fachkollegiums 111 „Sozialwissenschaften“ zum Forschungsdatenmanagement in der Soziologie, der Politikwissenschaften und der Kommunikationswissenschaften](https://www.dfg.de/resource/blob/174570/f311ff26e5a3c4b427271e0ba76a2ced/fachkollegium111-forschungsdatenmanagement-data.pdf) (2020).*

Die Empfehlung für die Wirtschaftswissenschaften verweist auf die RatSWD Orientierungshilfen: [RatSWD Output Series](https://www.konsortswd.de/en/publications/ratswd-output-series/) und insbesondere [„Forschungsdatenmanagement in den Sozial-, Verhaltens- und Wirtschaftswissenschaften“](https://doi.org/10.17620/02671.7).

Ein **Datenmanagementplan** ist nicht vorgeschrieben, kann aber in der Planungs- und Projektphase das Forschungsdatenmanagement strukturiert unterstützen. <!-- TO DO: Eventuell auf eine RDMO-Instanz oder ein anderes DMP-Tool verweisen. --> Hier kann der [RDMO-Fragenkatalog](https://rdmo.ub.fau.de/) „Wirtschafts-/Sozialwissenschaften“, der auf diesen Empfehlungen basiert, helfen.

Dem **Open Science** Gedanken folgend, wird im Antrag eine Stellungnahme zum Umgang mit Forschungsdaten erwartet.
Forschungsdaten sollten soweit und so umfassend wie möglich archiviert, dokumentiert und zur Nachnutzung öffentlich zugänglich abgelegt werden.

Beschreiben Sie welche **Datentypen und Datenformate** für erzeugte, bearbeitete oder wiederverwendete Datensätze verwendet werden.
Datentypen können zum Beispiel quantitative Online-Befragung (csv, SPSS-Dateien), 3D-Modellierungen, qualitative Interviews (.wav, .mp4) ... sein.
Sie sollten möglichst standardisierte, nicht-proprietäre und in der Community verbreitete Formate verwenden. Weitere Kriterien bezüglich der langfristigen Archivierung finden Sie im [nestor Handbuch](http://nestor.sub.uni-goettingen.de/handbuch/).

Wenn möglich sollten Sie **fachspezifische Standards** zur Dokumentation verwenden.
Bei sozialwissenschaftlichen Daten sind insbesondere die [DDI-Metadaten-Standards](https://ddialliance.org/) verbreitet.

Besonders wichtig ist die Einordnung hinsichtlich der sechs in den [Orientierungshilfen zum Forschungsdatenmanagement des RatSWD](https://doi.org/10.17620/02671.7) genannten Varianten. Legen Sie dar, welcher Variante Sie folgen wollen und begründen Sie die Entscheidung.

Sie sollten beschreiben, welche **Instrumente, Software, Technologien oder Verfahren zur Erzeugung oder Erfassung** der Daten genutzt werden.
Bei reproduzierbaren Daten, die nicht aufbewahrt werden, sollten Sie Informationen, die zu ihrer Erstellung nötig sind, bereitstellen.
Auch Software, Verfahren und Technologien, die zur Nutzung der Daten notwendig sind, sollten dokumentiert werden.
Verwenden Sie möglichst standardisierte, offene und etablierte Formate, um die Nachnutzung zu ermöglichen.

Die **Nachvollziehbarkeit** von Forschungsergebnissen in Publikationen sollte gewährleistet sein: durch die Bereitstellung von Programmen zur Datenaufbereitung und Analyse, deren Dokumentation sowie eine Ablage der Daten.

Die **Datensicherung** sollte mit Verweis auf die drei Varianten aus den [RatSWD Orientierungshilfen](https://doi.org/10.17620/02671.7) dargelegt und begründet werden.
Beschreiben Sie, wie und wo Daten (Rohdaten, aufbereitete Daten usw.) während des Projekts gespeichert und gebackupt werden.
Legen Sie dar, wer auf die Daten zugreifen darf, wie oft Backups erstellt werden und welche Maßnahmen für die Datensicherheit getroffen werden (Schutz vor unbefugtem Zugriff, Datenwiederherstellung, Übertragung sensibler Daten).

**Datenschutzrechtliche, urheberrechtliche und forschungsethische Aspekte** und Wahrung von Geschäftsgeheimnissen sind zu berücksichtigen. <!-- TO DO: Ggf. die fachspezifische Ethikkommission der Einrichtung einfügen. --> Gegebenenfalls sollte die [Ethikkommission](https://www.wiso.rw.fau.de/forschung/forschungsoutput/ethikkommission/) konsultiert werden.
Nehmen Sie Stellung zu Ihrer Verarbeitung personenbezogener Daten (Anonymisierung/Pseudonymisierung, Informierte Einwilligungserklärung) und nicht-personenbezogener sensibler Daten (beispielsweise Betriebs-/Geschäftsgeheimnisse).
Erwähnen Sie, ob das Forschungsvorhaben von einer Ethikkommission begutachtet wurde und das Ergebnis der Begutachtung.
[Der RatSWD stellt Informationen zum Thema Forschungsethik im Forschungsprozess zur Verfügung: [Sammlung Best-Practice Forschungsethik](https://www.konsortswd.de/ratswd/best-practice-forschungsethik/).]

Beachten Sie in den Sozialwissenschaften, dass im Antrag eine Reflektion stattfinden muss, ob/warum eine **Archivierung**/das **Teilen** von Daten bei Berücksichtigung der Auswirkungen auf alle betroffenen Personengruppen möglich ist. Das Vorgehen ist zu begründen.
Die Daten sollten so früh wie möglich verfügbar gemacht werden.
Für die **Nachnutzung** kann eine **Ausschlussfrist** festgelegt und begründet werden.
Falls Daten nicht veröffentlicht werden, begründen Sie dies bitte. Gründe können rechtliche, vertragliche oder freiwillige Einschränkungen sein.

Daten sollten für mindestens 10 Jahre verfügbar gemacht/archiviert werden.
Die Daten sollten in zertifizierten **fachspezifischen oder institutionellen Repositorien** archiviert werden.
<!-- TO DO: Durch den betreffenden Kontakt ersetzen -->
Kontaktieren Sie gerne das [Referat Forschungsdatenmanagement der Universitätsbibliothek der FAU](mailto:ub-fdm@fau.de).

Sie sollten erwähnen, unter welchen **Lizenzen** (zum Beispiel CC BY oder CC BY-SA) Ihre Daten veröffentlicht werden.

Detailfragen zu **Dokumentation und Metadaten**, um den Kontext der Daten zu klären, sind besonders in den Wirtschaftswissenschaften und Sozialwissenschaften von Bedeutung.
Wenn möglich sollten Standards, Ontologien, Klassifikation zur Datenbeschreibung verwendet werden, um eine Nachnutzung zu ermöglichen.
Das Fachkollegium "Wirtschaftswissenschaften" erwartet, dass die Daten mit einer aussagekräftigen Beschreibung abgelegt werden.
Geben Sie auch an, falls Komponenten der Datendokumentation erst auf Anfrage zugänglich gemacht werden.

Der potenzielle langfristige Wert des Datensatzes sollte in fachlicher und interdisziplinärer Perspektive reflektiert werden.
Vor allem für Datensätze, die einzigartig sind oder nur unter unverhältnismäßigem Aufwand reproduziert werden können, sollte über eine **langfristige Archivierung** nachgedacht werden.  
Eine langfristige Langzeitarchivierung kann zum Beispiel mit Datenarchiven wie [Zenodo](https://zenodo.org/), [RADAR](https://www.radar-service.eu/radar/en/home) oder [GESIS](https://www.gesis.org/home) für die nächsten 20/25 Jahre sichergestellt werden.
Die FAU hat eine eigene [Zenodo-Community](https://zenodo.org/communities/fau/?page=1&size=20) und stellt einen [RADAR-Zugang](https://ub.fau.de/forschen/daten-software-forschung/archivierung-und-publikation-der-daten/#collapse_0) bereit. <!-- TO DO: Anpassen oder löschen -->

Eine **Präregistrierung** der zu untersuchenden Forschungshypothesen kann erfolgen und im Antrag erwähnt werden.

Die **Kosten** für das Forschungsdatenmanagement können beantragt werden.

Im **Projektabschlussbericht** sollte auf die Umsetzung der Pläne zum Forschungsdatenmanagement eingegangen werden.

### Sprachwissenschaften (datentechnische Standards und Tools bei der Erhebung von Sprachkorpora)

*Referenz: [Empfehlungen zu datentechnischen Standards und Tools bei der Erhebung von Sprachkorpora](https://www.dfg.de/resource/blob/171632/383249f00d425a643bd8c120404bbff6/standards-sprachkorpora-data.pdf) (2019).*

#### Mündliche Korpora

Es wird zwischen Primärdaten, gegebenenfalls Zusatzmaterial (Handouts, Präsentationsfolien etc.), Transkriptionen, weitere Annotationen, Metadaten zur Dokumentation der Aufnahmesituation und -umstände unterschieden.
Die **Archivierung** der Primärdaten, Annotation und Metadaten ist zu gewährleisten.

Hinsichtlich der **Aufnahme** werden folgende Empfehlungen gegeben: digital, bestmögliche Aufnahmequalität (keine verlustbehaftete Kompression = unkomprimierte Daten mit möglichst hoher Samplingrate anstatt MP3-Format), möglichst externes Mikrofon bei Videoaufnahmen, Speicherung in nicht proprietären Formaten oder Konvertierung in offene Standard-Formate.

Das unmittelbare **Überspielen** vom Aufnahmegerät auf einen Rechner und die Gewährleistung eines Backups sollte sichergestellt werden.

Die **Rohdaten** sollten während der Projektlaufzeit behalten werden, auch wenn die Transkription und Analyse anhand von Primärdaten (geschnitten und konvertiert) erfolgt. Der Bezug der Primärdaten zu den Rohdaten sollte dokumentiert werden.

Die konkreten Empfehlungen zu **Aufnahmegeräten** (Flash-Mobilrekorder mit wechselbaren Speicherkarten) und **Formaten** (S. 5-6) sollten beachten werden:

- Audio-Aufnahmen: unkomprimiertes lineares PCM-Format (WAV), mind. 16bit/22kHz
- Video-Aufnahmen: MPEG2 oder MPEG4/H.264 mit hohen Bitraten, idealerweise MJPEG2000 als unkomprimiertes Format

**Zusatzmaterialien** (zum Beispiel Präsentationsfolien, Tagesordnungen, Fragebögen, Tafelanschrieb etc.) für bestimmte kommunikative Ereignisse sollten gesammelt, archiviert und Korpusnutzenden zugänglich gemacht werden: Relevanz sollte bei Projektbeginn bedacht werden.
Auch zu den Zusatzmaterialien sollten Einverständniserklärungen erstellt, gegebenenfalls rechtliche Fragen geklärt und die Formate zur Speicherung festgelegt werden.
Metadaten sollten auch zu Zusatzmaterialien erstellt werden.
Die Zusatzmaterialen sollten in den Metadaten zu den dazugehörigen Daten dokumentiert werden.

Für die **Transkription und Annotation** werden spezifische Tools und Verfahren empfohlen:

- **Software-Tools**: [ANVIL](https://www.anvil-software.org/), [ELAN](https://archive.mpi.nl/tla/elan), [EXMARaLDA](https://exmaralda.org/de/), [FOLKER](https://exmaralda.org/de/folker-de/), [PHon](https://www.phon.ca/phon-manual/getting_started.html), [Praat](https://www.fon.hum.uva.nl/praat/)
- Internationale **Standards**, zum Beispiel ISO24624:201 Language resource management für strukturierte Daten. Für weitere konkrete Empfehlungen siehe S. 8 der [Empfehlungen](https://www.dfg.de/resource/blob/171632/383249f00d425a643bd8c120404bbff6/standards-sprachkorpora-data.pdf).
- Es sollten **dokumentierte Verfahren** verwendet werden, ansonsten sollten **projektspezifische Konventionen und Schemata** möglichst als Erweiterung, Modifikation oder Vereinfachung existierender Verfahren mit entsprechender Dokumentation vorgenommen werden:
  - GAT/cGAT oder CHAT für orthographie-basierte Transkription von Spontansprache
  - IPA für phonetische Transkription
  - STTS, Tiger, SALSA, GRAID, ToBi für weiterführende Annotationen
  - Leipzig Glossing Rules für Interlinearglossierungen
  - Advanced Glossing zu Zwecken der Sprachdokumentation (siehe S. 9)

Die systematische Erhebung und Dokumentation von **Metadaten** bei der Erstellung wird erwartet.
Auch Metadaten, die nicht für das ursprüngliche Forschungsinteresse interessant sind, sollten berücksichtigt werden.
Sie könnten für die Nachnutzung durch Personen, die nicht an der Erhebung beteiligt waren, von Belang sein:

- Für die generellen Metadaten wird folgendes erwartet: Ort und Zeitpunkt der Aufnahmen, verwendete Technik, Angaben zu Zusatzmaterialien, soziobiographische und soziolinguistische Angaben zu den Sprechenden, datenschutzrechtlicher Status der Aufnahme.
- Die Verwendung strukturierter Textformate (XML-Datei oder CSV-Datei) wird empfohlen.
- Die Metadaten sollten den Mindestanforderungen entsprechen, zum Beispiel dem Metadatenstandard der [Dublin Core](https://dublincore.org) Initiative oder der [Open Language Archives Community (OLAC)](http://www.language-archives.org/).
- Die Beschreibung erfolgt idealerweise mit Hilfe des [CMDI-Frameworks](https://www.clarin.eu/content/component-metadata) (Component MetaData Infrastructure) und des [CLARIN Component-Registry](https://catalog.clarin.eu/ds/ComponentRegistry/#/). Die Kontaktaufnahme mit dem zur Archivierung geeigneten CLARIN-Zentrum sollte möglichst während der Projektplanung erfolgen, damit eine Beratung im Hinblick auf Metadaten frühzeitig möglich ist.

Die **Archivierung** sollte am besten in einem spezialisierten Zentrum, das während der Projektplanung, spätestens bei Projektbeginn kontaktiert wird, erfolgen: zum Beispiel in einem der [Zentren, die am CLARIN Projekt](https://www.clarin-d.net/de/aufbereiten/clarin-zentrum-finden) teilnehmen.
Für Daten aus dem Schulbereich bietet sich das [Deutsche Institut für Internationale Pädagogische Forschung (DIPF)](https://www.dipf.de/de) an.

#### Schriftliche Korpora

##### Digitalisierung und Korpuserfassung

Die **Textauswahl** sollte dargestellt werden: Bibliographie oder Quellenliste der zu digitalisierenden und gegebenenfalls zu integrierenden Ressourcen; Beachtung rechtlicher Beschränkungen und deren Dokumentation; Dokumentation der Kriterien der Textauswahl (Nachvollziehbarkeit, Generalisierbarkeit und Repräsentativität).
Die Korpusgröße, Datenqualität und Erschließungstiefe sind gegeneinander abzuwägen und die Entscheidungen transparent zu dokumentieren.

Für die **Primärdaten** gilt:

- Angaben zur Textquelle: ursprüngliche Quelle, Zeitpunkt der Übernahme, gegebenenfalls nachfolgende Bearbeitungsschritte (Kuration und Anreicherung), Beachtung der rechtlichen Rahmenbedingungen

Je nach Format der Textquelle gibt es unterschiedliche Angaben und Verfahrensweisen zu beachten:

- Ein Korpus auf Grundlage bestehender **physischer Textquellen** hat folgende Mindestanforderungen: unkomprimierte TIFF-Dateien oder JPEG2000 in verlustfreier Form, mind. 300 dpi, Farbscans.
- Ein Korpus auf Grundlage **digitaler Textdaten** (originalgetreue Volltexte oder kritische Editionen): Recherche und Bereitstellung der zugehörigen zugrundeliegenden Bilddateien, Abweichungen sind zu begründen; besondere Sorgfalt bezüglich der Zuverlässigkeit der Transkription gegenüber der Vorlage.
- Ein Korpus aus der **Nachnutzung von 'born digital' Texten**: Die Webtexte sollten kompiliert werden, zum Beispiel durch Webcrawling für die Qualitätssicherung und Replizierbarkeit, da die Änderbarkeit der Quelldaten einkalkuliert werden muss. Die Kriterien müssen vorher festgelegt und dokumentiert werden. Die Crawling-Methoden und Algorithmen sollten dokumentiert werden, idealerweise wird der Crawler zur Verfügung gestellt.

Die **Texterfassung** kann durch manuelle Transkription oder automatisch via Optical Character Recognition (OCR) mit manueller oder halbautomatischer Überprüfung beziehungsweise Nachkorrektur vorgenommen werden.
Das gewählte Verfahren sollte erwähnt und begründet werden.
Die Metadaten jeder Textressource sollte die Dokumentation des Verfahrens beinhalten.
Die Texterfassung sollte möglichst genau sein und die Transkriptionsrichtlinien festgehalten werden.

Die **Annotation** (manuell, automatisch oder halbautomatisch) sollte strukturelle Merkmale, linguistische Eigenschaften und sprachliche Spezifika berücksichtigen.
Der geeignete Skopus und die geeignete Tiefe sind begründet zu dokumentieren.
Die Tagsets, Strukturierungstiefe, eingesetzte Verfahren und Tools bei automatischer Annotation und die erwartbare Annotationsgenauigkeit sollten dokumentiert werden.
Konkrete Empfehlungen für die strukturelle Annotation (Formate, Tag-Sets, Tools), siehe S. 23-24 der Empfehlungen.

Die **Qualitätssicherung** umfasst die Textqualität, die Qualität der strukturellen Annotation, der linguistischen Annotation und der Metadaten. Die geforderten Maßnahmen werden dokumentiert.

##### Standards und Tools

Als **Standard** hat sich das XML-Format etabliert.
Für die strukturelle Auszeichnung hat sich der TEI P5-Regelsatz etabliert, der sich mithilfe des ODD-Formalismus projektspezifisch einschränken und anpassen lässt.
Ein projektspezifisches Format gilt es zu begründen.
Für die linguistische Annotation sind möglichst spezialisierte Annotationstools, die ein standardisiertes Ausgabenformat ermöglichen, zu verwenden.
Anpassungen an Tools oder Tag-Sets und der erforderliche Aufwand sollten im Projektantrag dargelegt werden.

Falls **Tools** für wenig beforschte Themen **entwickelt** werden, sollte die Implementierung, Dokumentation und Verfügbarkeit zur Nachnutzung (zum Beispiel durch Veröffentlichung des Quellcodes) nach erforderlichen Standards erfolgen.

Für **Analysetools** mit standardisierten Ausgabeformate sollten die verschiedenen Datenmodellierungs-, Anfrage- und Analysemöglichkeiten je nach Datenbankparadigmen interdisziplinär erörtert werden.

Die **Metadaten** sollten folgende Angaben enthalten: Angaben zur Textquelle, zur digitalen Ausgabe, zum Projekthintergrund, zum Datenumfang, zur Annotationstiefe, zu den Richtlinien der Transkription beziehungsweise Textzusammenstellung und Annotation, inhaltliche Angaben, Angaben zu Nutzungsbedingungen, zur rechtlichen und technischen Verfügbarkeit, Hinweise zur korrekten Zitierweise.
Die Metadaten sollten in folgenden Formaten erstellt werden: [TEI-Header](https://tei-c.org/release/doc/tei-p5-doc/de/html/ref-teiHeader.html), [METS](http://www.loc.gov/standards/mets/)/[MODS](http://www.loc.gov/standards/mets/), [CMDI](https://www.clarin.eu/content/component-metadata), [EAD](https://www.loc.gov/ead/).

**Nachnutzung**: Im Projektantrag sollten die Möglichkeiten und Beschränkungen einer späteren Nachnutzung auch in anderen Projektzusammenhängen und für andere Fragestellungen dargestellt werden.
Die Kooperation mit einem spezialisierten Zentrum oder einer spezialisierten Institution wird empfohlen.
Die FAIR-Prinzipien sollten für die Daten und Metadaten berücksichtigt werden.
Die rechtlichen Aspekte der Nachnutzung sollten im **Vorfeld** der Antragstellung geklärt werden.
Konkrete Punkte, siehe S. 28 der Empfehlungen.

### Sprachwissenschaften (rechtliche Aspekte bei der Handhabung von Sprachkorpora)

**Disclaimer**: Das unten zitierte Dokument wurde von DFG-Seite zurückgezogen (Stand: Oktober 2022). Die Informationen sind wegen Änderungen des deutschen Urheberrechts potenziell veraltet. Aktualisierte Empfehlungen sind noch nicht bekannt. (Stand: 12. Januar 2023)

*Referenz: [Handreichung: Informationen zu rechtlichen Aspekten bei der Handhabung von Sprachkorpora](https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/informationen_fachwissenschaften/geisteswissenschaften/standards_recht.pdf) (2013).*

<!-- TO DO: Ggf. einen Kontakt für Rechtsfragen im Bereich Forschungsdatenmanagement eintragen oder löschen -->
Die Beauftragte für Rechtsfragen im Bereich Forschungsdatenmanagement an der FAU, Frau Petra Heermann (<petra.heermann@fau.de>), kann bei konkreten Fragen oder Unklarheiten kontaktiert werden.

#### Mündliche Korpora

**Datenschutzerklärungen, Einwilligungserklärungen und Anonymisierung/Pseudonymisierung**, Details siehe S. 6ff:
Beachten Sie die Bundesdatenschutzgesetze und Landesdatenschutzgesetze.
Der Umgang mit personenbezogenen Daten umfasst das Erheben, Speichern, Verarbeiten und Veröffentlichen der betreffenden Daten.
Für das Verarbeiten persönlicher Daten muss die Einverständnis der betroffenen Person eingeholt werden.

Datenschutzrechtliche Aspekte der Datenhandhabung und -verarbeitung sollten mit dem Datenschutzbeauftragten abgestimmt werden.
<!-- TO DO: Durch den Kontakt zum Datenschutzbeauftragten der Einrichtung ersetzen. -->
Nehmen Sie frühzeitig Kontakt mit dem [Datenschutzbeauftragten der FAU](https://www.fau.de/fau/leitung-und-gremien/gremien-und-beauftragte/beauftragte/datenschutzbeauftragter/) auf.

Betroffene Daten sind mindestens Audio- und Videoaufnahmen, Transkriptionen und Metadaten, die zu Sprechenden und Kommunikationssituationen festgehalten werden.

Um datenschutzrechtliche Bestimmungen zu befolgen, sollten Sie eine **Datenschutzerklärung** und darauf bezogene **Einwilligungserklärungen** der betroffenen Personen vorsehen.
Die Daten sollten in geeigneter Weise **anonymisiert** und/oder **pseudonymisiert** werden.

Details zu den **Einwilligungserklärungen** und den erforderlichen Inhalten finden Sie auf S. 6-8 der Empfehlungen.

Details zu **Anonymisierung und Pseudonymisierung** je nach Datentyp finden Sie auf S. 8-9 der Empfehlungen.
Die Formen der Anonymisierung und der Pseudonymisierung sollten in den Einwilligungserklärungen vermerkt werden.

Details zu **Urheberrechtlichen Aspekten** finden Sie auf S. 10-11 der Empfehlungen.
Mündliche Korpora für die Urheber- und Leistungsschutzrechte gelten, sind zum Beispiel Audio- oder Videoaufnahmen aus Rundfunk und Fernsehen, aus dem Internet, schriftliche Materialien als Zusatzmaterial zu einem mündlichen Korpus, sowie Fotos und Grafiken, die von Akteur\*innen im Forschungsfeld produziert wurden.

#### Schriftliche Korpora

Das **Urheberrecht** gilt für Texte einer bestimmten Schöpfungshöhe und deren Autor*in nicht seit mindestens 70 Jahren tot ist.
Leistungsschutzrechte gelten für Presseinhalte (1 Jahr), wenn diese öffentlich gemacht werden, für wissenschaftliche Ausgaben sowie für nachgelassene Werke (25 Jahre).
Schutzrechte gelten auch für die Struktur von Datenbanken (15 Jahre).

Die relevanten Regelungen des Urheberrechtsgesetzes betreffen vor allem das Vervielfältigungsrecht (§ 16), das Verbreitungsrecht (§ 17), das Recht der Recht der öffentlichen Zugänglichmachung (§ 19a), der Leistungsschutz wissenschaftlicher Ausgaben (§ 70) und nachgelassener Werke (§ 71), das Leistungsschutzrecht des Datenbankherstellers (§ 87b) und das Leistungsschutzrecht der Presseverleger (§ 87f).
Das Text-and-Data-Mining und die quantitative linguistische Analyse sind juristisch noch nicht geklärt.

Details zu urheberrechtlichen Aspekten finden Sie auf S. 12-13 der Empfehlungen.

Beachten Sie die Schrankenregelung und ihre Anwendung auf Textkorpora (S. 13-14).
Für den **Aufbau eines Korpus** gilt entweder das Einholen der Erlaubnis eines Rechteinhabers oder nur die Verwendung von Texten, die grundsätzlich nicht urheberrechtlich geschützt sind, an denen die Urheberrechte abgelaufen sind oder die keine ausreichende Schöpfungshöhe erreichen.
Für die vollständige Rechteklärung wird die Kooperation mit Zentren empfohlen, die den Rechtsstatus nachhalten und die Daten dauerhaft verfügbar halten können.

Beachten Sie die **Regelungen für Bearbeitungen und Umgestaltungen** von Werken (siehe S. 15-16):
Forschungsergebnisse wie Statistiken oder Annotationen aus **Text-and-Data-Mining** sind keine Bearbeitungen des Ausgangstextes.
Das Text-und-Data-Mining kann aber trotzdem vertraglich untersagt sein.
Auch löst es nicht das Problem, dass ein geschützter Ausgangstext zur Verarbeitung zwischengespeichert werden muss und auch dies vertraglich untersagt sein kann.

Beachten Sie die Regelungen für **Sammelwerke und Datenbankwerte** sowohl für die Aufnahme von Teilen einer Datenbank in Ihre Daten als auch die Verfügbarmachung Ihrer Daten (siehe S. 17).

Beachten Sie die Regelungen für **verwaiste Werke**, die eine öffentliche Zugänglichmachung und Vervielfältigung erlauben, sofern die Werke nicht bearbeitet oder umgestaltet werden (siehe S. 18).

Beachten Sie die Nutzungsbedingungen kommerzieller **Software** und von Software-Werkzeugen, die im akademischen Kontext entwickelt wurde und von Rechten Dritter betroffen sein kann.
Falls Sie selbst Software entwickeln und sie veröffentlichen, sollten Sie die zu vergebene Lizenz mit Ihrer arbeitgebenden Institution klären.

Beachten Sie, dass das **Datenschutzrecht** auch die Zusammenstellung und Verfügbarmachung von schriftlichen Texten (zum Beispiel Chatprotokolle) betreffen kann.
Eine **Pseudonymisierung/Anonymisierung** kann sinnvoll sein.
Wie bei den mündlichen Korpora sollte eine **informierte Einwilligungserklärung** eingeholt werden.

Die Handreichung gibt einige Empfehlungen:

Im Zweifelsfalle sollten **Lizenzen und Einwilligungen** eingeholt werden.
Lizenzen sollten möglichst früh in der Planungsphase einholt werden, damit eventuelle Lizenzgebühren in die Projektkosten einkalkuliert werden können.
In der Planungsphase sollte Kontakt zu einem **Zentrum** aufgenommen werden, das mit der Lizensierung Ihres Ressourcetyps Erfahrungen hat, um die Datengrundlage und Projektergebnisse in die Kurationsprozesse und Archivierungsprozesse aufzunehmen.
Die Handreichung verweist auf den [CLARIN-D Legal Helpdesk](https://www.clarin-d.net/de/hilfe/rechtliche-fragestellungen) für Empfehlungen zu Entwürfen von Lizenzverträgen.
Falls das Korpus aus rechtlichen Gründen nicht dauerhaft zur Verfügung gestellt werden kann, sollten die Gründe dokumentiert und **Kompromissstrategien** gesucht werden.
Dies kann eine **Dokumentation** sein, wie Nachnutzende die Rechte selbst einholen können.
Datenschutzrechtliche Belange sollten in die Planungsphase einbezogen werden und ein explizites Dokument zum **Datenschutzkonzept** erstellt werden.

Die Handreichung bietet weiter Empfehlung zur Verfügbarmachung von Textkorpora für einen eingeschränkten Nutzendenkreis und Hinweise zu Endnutzerlizenzvereinbarungen, siehe S. 20.

**Selbst geschaffenen Werke** sollten mit einer **Lizenzbestimmung** versehen werden: CC BY oder CC BY-SA beziehungsweise eine GNU-Lizenz für Software.
Wenn möglich sollten wissenschaftliche Werke eine CC BY-Lizenz bekommen und reine Daten eine CC0-Lizenz.

Falls zur Erstellung abgeleiteter Werke **Software** verwendet wird und die Lizenz nicht bekannt ist, sollten etwaige Einschränkungen für ihren Einsatz ermittelt werden.
Für die Nutzung kommerzieller Annotationswerkzeuge sollten für die Softwareausgabe Zusatzvereinbarungen getroffen werden.
Im Allgemeinen sollte geklärt werden, welche Bestimmungen für die Weiternutzung der Ausgaben der Software nach Lizenzablauf gelten.

### Literaturwissenschaften (wissenschaftliche Editionen)

*Referenz: [Förderkriterien für wissenschaftliche Editionen in der Literaturwissenschaft](https://www.dfg.de/resource/blob/172080/895fcc3cb72ec6cd1d5dc84292fb2758/foerderkriterien-editionen-literaturwissenschaft-data.pdf) (2015).*

Gegebenenfalls sind auch die Empfehlungen des DFG-Fachkollegiums 104 "Sprachwissenschaften" zu [datentechnischen Standards und Tools bei der Erhebung von Sprachkorpora](https://www.dfg.de/resource/blob/171632/383249f00d425a643bd8c120404bbff6/standards-sprachkorpora-data.pdf) und die [Informationen zu rechtlichen Aspekten bei der Handhabung von Sprachkorpora](https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/informationen_fachwissenschaften/geisteswissenschaften/standards_recht.pdf) (Urheberrecht, Datenschutz, Anonymisierung) interessant. (Zu beachten: Das Dokument "Informationen zu rechtlichen Aspekten bei der Handhabung von Sprachkorpora" wurde von der DFG zurückgezogen und noch nicht durch ein neues ersetzt).

Beachten Sie bitte, dass für Editionen in der Literaturwissenschaft die Förderrichtlinien neben Regelungen zum Datenmanagement auch einige allgemeine Anforderungen beinhalten (Publikationsform, Aufbau usw.).

Ein **Datenmanagementplan** ist nicht vorgeschrieben, kann aber in der Planungs- und Projektphase das Forschungsdatenmanagement strukturiert unterstützen. <!-- TO DO: Eventuell auf eine RDMO-Instanz oder ein anderes DMP-Tool verweisen. --> Hier kann der [RDMO-Fragenkatalog](https://rdmo.ub.fau.de/) „Wissenschaftliche Editionen in der Literaturwissenschaft“, der auf diesen Empfehlungen basiert, helfen.

Das **Datenmanagement der Textdaten ist Teil der Förderkriterien**.

Beschreiben Sie die erzeugten **Datentypen und Datenformate**. Es sollten möglichst standardisierte, nicht-proprietäre und allgemein beziehungsweise in der Community verbreitete Formate genutzt werden, um die Nachnutzung und Langzeitarchivierung zu ermöglichen.
Siehe hierzu auch detaillierte Erläuterungen im [nestor Handbuch](http://nestor.sub.uni-goettingen.de/handbuch/).

Auch für Printausgaben sollte die **Textgrundlage** in standardisierter digitaler Form, im sogenannten **Basisformat**, verfügbar gemacht werden.

Sie sollten etablierte **Standards** der Editionsphilologie einhalten, die zugrundeliegenden Editionsrichtlinien formulieren und die technische Umsetzung erläutern und methodisch begründen.
Die technischen Standards der digitalen Textaufbereitung (Textauszeichung, Struktur der Online-Präsentation, Such- und Navigationsmöglichkeiten im Datenbestand) müssen erläutert werden.
Abweichungen und Weiterentwicklungen von Standards, Methoden und Techniken sollten erwähnt werden.

Ihr Antrag sollte Angaben zum **Zeitpunkt und der Form der Veröffentlichung** (reine Druckausgabe, Hybridausgabe, digitale Veröffentlichung) Ihrer Edition beinhalten.
Auch wenn Ihre Edition in Printform veröffentlicht wird, wird eine Sicherung und **Bereitstellung der Textdaten in digitaler Form** erwartet.
Ihre Arbeitsweise zur **Texterstellung** sollte dem *state-of-the-art* hinsichtlich Softwareeinsatz, Arbeitsprozessen und Qualitätssicherung entsprechen.

Eine Zusammenarbeit mit geeigneten Institutionen wie Bibliotheken, Archiven, spezialisierten Forschungszentren ist sowohl für die Texterstellung als auch die Archivierung der Daten zu empfehlen.

Fragen zur Veröffentlichung, wie **Urheber-, Datenschutz-, Leistungsschutz-, und gegebenenfalls auch Persönlichkeitsrechte** sind **vorab** zu klären, ungeklärte Aspekte im Antrag zu benennen und im Hinblick auf die Auswirkungen auf die Veröffentlichung der Edition zu bewerten.
Da Maßnahmen zur Klärung der rechtlichen Situation vor der Antragstellung durchgeführt werden sollten, können diese nicht durch DFG-Mittel finanziert werden.

Eine Online-Publikation erfolgt idealerweise im **Open Access** („goldener Weg“), falls nötig mit maximal 24 Monaten Karenzzeit zur Berücksichtigung von Verlagsinteressen.
Nach Ablauf dieser Frist sollte eine **Zweitveröffentlichung** auf Servern öffentlich-rechtlicher Informations- oder Forschungseinrichtungen („grüner Weg“) ermöglicht werden.  
Erfolgt die Publikation nicht im Open Access, muss dies begründet werden.

Für die **bibliothekarischen Metadaten** müssen Standards eingehalten werden und die Auffindbarkeit der Online-Publikation über gängige bibliothekarische Nachweissysteme gewährleistet sein.
Sollte die Edition bei einem Verlag oder in einer dezidierten Plattform für Editionen veröffentlicht werden, werden die Metadaten normalerweise ohnehin in üblichen Formaten wie MARC21 zur Verfügung gestellt.

Gängige **Schnittstellen** für einen breiten Datenaustausch müssen implementiert werden.

Die digitale **Langzeitarchivierung** der digitalen Edition sollte über die Einbindung in zertifizierte Repositorien, Archive oder Nationalbibliotheken gesichert sein.
Dafür können keine Fördermittel beantragt werden.

Die zugrundeliegenden **Textdaten** sollten immer in einem **„Basisformat“** durch die Speicherung in einem fachlich anerkannten Repositorium zur Nachnutzung verfügbar gemacht werden.
Abweichungen vom Basisformat müssen ausdrücklich erläutert und begründet werden.
Dies ist unter Punkt 2.4 im Antrag zu erläutern.

Das **Basisformat** wird wie folgend definiert:
Der Text wird in **Unicode** gespeichert, in **XML-basiertem Markup** (aktuelle Version des TEI-Modells, zum Beispiel [TEI Simple](http://www.tei-c.org/Guidelines/) oder [DTABf](http://www.deutschestextarchiv.de/doku/basisformat)) codiert.
Sollten Sie eine andere Zeichencodierung als Unicode verwenden, begründen Sie, warum dies für Ihr Projekt nötig ist.
Bezüglich der Struktur- und Gliederungselemente des Textes im Markup verweisen die Förderkriterien zur Orientierung auf die TextGrid-Sammlung [Baseline Encodings](https://wiki.de.dariah.eu/display/TextGrid/Search+Index+and+Baseline+Encoding).
Die Entscheidung für eine bestimmte Repräsentation der Strukturelemente muss begründet werden.
Projektspezifische XML-Elemente und Attribut-Wert-Paare sind im TEI-Header zu dokumentieren.
Wenn dies nicht der Fall ist, begründen Sie, warum dies nicht geschieht.

Eine eventuelle langfristige **Sicherung der Präsentation** des Volltextes sollte durch eine geeignete Formatierungssprache (XSLT, CSS) sichergestellt werden.

Falls die archivierten Daten nicht der finalen Edition entsprechen, begründen Sie, warum dies der Fall ist.

Das Basisformat ist als **Mindeststandard** zu verstehen, das mit technischen Mitteln und angemessenem Einarbeitungs- und Arbeitsaufwand geleistet werden kann.
Fehlende Expertise sollte durch Kooperationen, zum Beispiel mit Bibliotheken, Archiven, Datenzentren usw., ausgeglichen werden. Auch kann dies eine effizientere Arbeitsaufteilung und Kostenersparnis bedeuten.
Die **Mittel** für die Aufbereitung im Basisformat können beantragt werden, dabei sind Angebote von externen Dienstleistenden als Vergleich heranzuziehen.
Die im Basisformat aufbereiteten Textdaten sollten spätestens **24 Monate** nach der eigentlichen Veröffentlichung zur Nachnutzung verfügbar gemacht werden.
Falls die Daten erst nach Ablauf einer Embargofrist/Sperrfrist zugänglich gemacht werden, begründen Sie dies bitte.

Außer den Textdaten im Basisformat können auch **andere Daten** in Editionsprojekten entstehen, zum Beispiel Digitalisate von Manuskripten, die Sie anderen Forschenden zur Verfügung stellen können.

Die Daten sollten in einem zertifizierten institutionellen oder fachlichen **Repositorium** archiviert werden, dass die langfristige Verfügbarkeit und Zitierfähigkeit durch **persistente Identifikatoren** (zum Beispiel DOIs) gewährleistet.
Die Bestätigung der Institution sollte dem Antrag beiliegen.
Die Archivierung muss mit einer **Lizenz** (zum Beispiel CC BY oder CC BY-SA), die eine freie Nachnutzung zu wissenschaftlichen Zwecken erlaubt, erfolgen.

## Lebenswissenschaften

### Biodiversitätsforschung

*Referenz: [Richtlinien zum Umgang mit Forschungsdaten in der Biodiversitätsforschung](https://www.dfg.de/resource/blob/171716/cad14d794c39d50da731d681a56eb651/richtlinien-forschungsdaten-biodiversitaetsforschung-data.pdf)(2015).*

Ihre Forschungsdaten sollten öffentlich und kostenlos **zugänglich** sein.
Einschränkungen aus rechtlichen, urheberrechtlichen oder ethischen Gründen können angeführt werden.

Es wird erwartet, dass die Datenqualität gesichert wird und die Daten ausreichend dokumentiert werden, um ihre Nachnutzung zu ermöglichen.

Ihrem Projektantrag sollte ein **Datenmanagementplan** beiliegen.
Siehe Punkt 3 der Richtlinien für die Aspekte, die im Datenmanagementplan behandelt werden sollten.

Für **Verbundprojekte** wird eine spezifische **Datenleitlinie** empfohlen, die die projektinterne Bereitstellung, Weitergabe und Nachnutzung von Daten regelt.

### Medizin und Biomedizin

*Referenz: [Leitlinien für qualitätssichernde Maßnahmen in der Medizin und Biomedizin](https://wissenschaftliche-integritaet.de/kommentare/leitlinien-fuer-qualitaetssichernde-massnahmen-in-der-medizin-und-biomedizin/) (2021).*

Die Leitlinien verweisen gegebenenfalls auf eine Beachtung der [Handreichung zu tierexperimenteller Forschung](https://www.dfg.de/download/pdf/dfg_im_profil/geschaeftsstelle/publikationen/handreichung_sk_tierversuche.pdf) (2019) hin.

Der Antrag sollte die Entscheidung für ein Modellsystem, eine Datenquelle, einen theoretischen Ansatz in Bezug auf die Forschungsfrage begründen und dabei insbesondere die **rechtlichen und ethischen Rahmenbedingungen** berücksichtigen.

Die **Eignung** der Forschungsdaten sollte, wenn möglich, belegt werden.

Sollten Sie **technische, methodische oder organisatorische Forschungsinfrastrukturen** für die Durchführung und Qualitätssicherung benötigen, sollten Sie dies im Antrag darlegen.
Falls Sie auf vorhandene Kompetenzen, Strukturen, Angebote vor Ort zurückgreifen, sollte Sie dies erwähnen.
Auf **vorhandene Datensätze und Bioproben** sollte, wo möglich, zurückgegriffen werden.

Sie sollten in Ihrem Antrag auf die **Art der Studie, die statistische Planung, Nutzung, Analyse und Bereitstellung** der Daten eingehen.
Handelt es sich um eine explorative oder konfirmative Studie?
Wäre eine Replikationsstudie sinnvoll?
Ist die Registrierung der Studie erforderlich?
Gibt es Quellen für Biases in Ihrer statistischen Planung und wie gehen Sie damit um?

Ihre Datensätze und Bioproben sollten in **zertifizierten Biobanken, Sammlungen oder Forschungsdatenrepositorien** (zum Beispiel der entsprechenden [NFDI-Konsortien](https://www.nfdi.de/konsortien/)) abgelegt werden.
PUBLISSO bietet eine ausführliche [Übersicht zu Repositorien aus den Lebenswissenschaften](https://www.publisso.de/open-access-publizieren/repositorien/fachrepositorium-lebenswissenschaften) an, die von Interesse sein können.
Falls ethische oder rechtliche Gründe gegen eine Veröffentlichung sprechen, erwähnen Sie dies.
Berücksichtigen Sie **Mittel** für die Nutzung der Strukturen.

Sie sollten mögliche **Biases** aufgrund der Fragestellung, Planung, Durchführung und Analyse der Daten diskutieren und Ihr Umgehen damit darlegen.

Gehen Sie auf Forschungsleistungen ein, die einen qualitativen Mehrwert für Ihre Fragestellung haben: systematisches Review, Replikationsstudie, Mitwirkung bei der Standardisierung ...

Benennen Sie konkret die Verantwortlichkeit für das Datenmanagement im Projekt.

## Naturwissenschaften

Siehe auch [Lebenswissenschaften](#lebenswissenschaften) für Biologie.

### Mathematik

*Referenz: [Fachkollegien "Mathematik" zum Umgang mit Forschungsdaten im Fachbereich Mathematik](https://www.dfg.de/resource/blob/176136/42e4fda0ee48534306f2c603780beeae/handreichung-fachkollegium-mathematik-forschungsdaten-data.pdf) (2023)*

Trotz der Vielfalt der Forschungsdaten in den unterschiedlichen mathematischen Disziplinen konnten sich in vielen Bereichen standardisierte Prozesse durchsetzen.
Die vorhandene **Best Practices** der internationalen Fachcommunities und der NFDI-Konsortien sollten daher beachtet werden.
Insbesondere das Konsortium **MaRDI** informiert über Anwendungsfälle und bietet Tutorials zum fachspezifischen Umgang mit Daten (https://www.mardi4nfdi.de/resources/publications).

Die Empfehlungen verweisen auf die **FAIR-Prinzipien** und die [DFG-Checkliste zum Umgang mit Forschungsdaten](https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/forschungsdaten/forschungsdaten_checkliste_de.pdf).

Im Antrag sollte dargestellt werden, ob **Kosten** durch die Zugänglich-, Nachnutzbarmachung oder Überführung der Daten in ein öffentliches Repositorium entstehen.
Diese können beantragt werden. Siehe [beantragbare Mittel](https://www.dfg.de/foerderung/grundlagen_rahmenbedingungen/forschungsdaten/beantragbare_mittel/index.html).

Die vertieften Hinweise und Regelungen orientieren sich an der Struktur der DFG-Checkliste:

**Beschreibung der Daten**

Der Antrag sollte zumindest **summarische Angaben zu Art und Umfang** der anfallenden Forschungsdaten sowie der verwendeten Software enthalten.
Sie sollten in Fachcommunities etablierte **Dateiformate** verwenden und die vorhandenen Strukturen und Dateiformate kurz beschreiben. Software sollte möglichst in Form von Paketen für (quell-offene) in der Community anerkannte Systeme
erstellt werden.

**Dokumentation und Datenqualität**

Wichtige Punkte zur **Datenaufbereitung, Qualitätssicherung und Dokumentation** sollten genannt werden. Die Empfehlungen verweisen hier insbesondere auf die [Guideline 12 - Dokumentation](https://wissenschaftliche-integritaet.de/en/code-of-conduct/documentation/). 

Zu einer transparenten Dokumentation der Daten kann ebenfalls eine geordnete Ablage,
beispielsweise projekt- oder publikationsbezogen, beitragen. Daten sollten mit **eindeutigen Identifikatoren** versehen werden, um ihre Zuordnung und Nachnutzung zu erleichtern. 

Die Antragstellenden sind dafür verantwortlich, dass die datenerhebenden Projektmitarbeitenden **Schulungen und umfangreiche Einweisungen zum Forschungsdatenmanagement** erhalten.
Im Antrag sollte dargelegt werden, in welcher Form dies erfolgt: Einführungsveranstaltungen, Schicht-Trainings, Einarbeitung durch erfahrende Kollaborationsmitglieder etc.
Bei Kooperationen kann dies organisatorisch erfolgen, zum Beispiel in integrierten Graduiertenschulen oder zentralen Projekten.

Die Antragstellenden sind für die **Datenqualität** (definiert in Abhängigkeit des Projektes) sowie die spätere Auswahl der Daten für die Archivierung und langfristigen Zugänglichkeit verantwortlich.

**Speicherung und technische Sicherung während des Projektverlaufs**

Die **Speicherung** der Daten sollte erläutert werden. Die bestehenden Vorgaben der jeweiligen Forschungseinrichtung sollten erwähnt werden.
Eine Archivierung für mindestens 10 Jahre ist gemäß den DFG-Vorgaben zu garantieren. Siehe [Leitlinie 17 - Archivierung](https://wissenschaftliche-integritaet.de/kodex/archivierung/), auf die verwiesen wird.

Die Fachkollegien sind sich bewusst, dass **nicht immer alle Daten aufbewahrt** werden können, aber Abweichungen von den Standards der Community sollten explizit im Antrag dargelegt werden.

**Rechtliche Verpflichtungen und Rahmenbedingungen** 

Rechtliche Verpflichtungen und Rahmenbedingungen sollten vorab geprüft und im Antrag kurz benannt werden. Die rechtlichen Verpflichtungen
beziehen sich übergreifend auf die Aufnahme, Dokumentation, Speicherung, Archivierung und Nachnutzung der erzeugten oder verwendeten Daten. Wo es Dritte gibt, die Rechte an den Daten haben, sollten sich daraus evtl. ergebende Einschränkungen dokumentiert werden.

**Datenaustausch und dauerhafte Zugänglichkeit der Daten**

Im Antrag sollte dargelegt werden, warum welche Daten **archivierungswürdig** sind (Auswahl mit Augenmaß).
**Daten, die Publikationen zugrunde liegen** sollten dauerhaft archiviert werden.
Zur Archivierung gehört nach Möglichkeit auch die **Zugänglichkeit** der Daten.

Zur Archivierung werden **zertifizierte Repositorien und Datenzentren** empfohlen, hier bitte auch die Empfehlungen der relevanten NFDI beachten.
Mit den geeigneten Institutionen sollte vor Projektbeginn Kontakt aufgenommen werden, um den **zeitlichen und finanziellen Bedarf berücksichtigen** zu können.
Um eine spätere Nachnutzung zu ermöglichen, sollten **archivfähige, standardisierte Datenformate** gewählt werden. Das Repositorium [Zenodo](https://www.zenodo.org) wird als sinnvoll Lösung mit Bestandsgarantie genannt.

Falls im Projekt **Lösungen zur Umwandlung von Datenformaten** entwickelt werden, sollten diese zur Nachnutzung bereitgestellt werden.



**Verantwortlichkeit und Ressourcen**

Der Antrag sollte darlegen, wer im Projektverlauf für das Datenmanagement verantwortlich ist.
Die **Verantwortung für das Datenmanagement und die Qualitätskontrolle** liegt bei den Projektleitenden, kann aber im Rahmen einer Kooperation auch bei einer genutzten Infrastruktur liegen.
Dies müsste im Antrag explizit erwähnt werden.

### Physik

*Referenz: [Fachkollegien "Physik" zum Umgang mit Forschungsdaten im Fachbereich Physik](https://www.dfg.de/resource/blob/175982/abe8f1e92c9ff1243961bfc3b01e8d83/handreichung-forschungsdaten-physik-data.pdf) (2023)*

Trotz der Vielfalt der Forschungsdaten in den unterschiedlichen physikalischen Disziplinen konnten sich in vielen Bereichen standardisierte Prozesse durchsetzen.
Die vorhandene **Best Practices** der internationalen Fachcommunities und der NFDI-Konsortien ([FAIRmat](https://www.fairmat-nfdi.eu/fairmat), [PUNCH4NFDI](https://www.punch4nfdi.de/), [DAPHNE4NFDI](https://www.daphne4nfdi.de/index.php)) sollten daher beachtet werden.

Die Empfehlungen verweisen auf die **FAIR-Prinzipien** und die [DFG-Checkliste zum Umgang mit Forschungsdaten](https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/forschungsdaten/forschungsdaten_checkliste_de.pdf).

Im Antrag sollte dargestellt werden, ob **Kosten** durch die Zugänglich-, Nachnutzbarmachung oder Überführung der Daten in ein öffentliches Repositorium entstehen.
Diese können beantragt werden. Siehe [beantragbare Mittel](https://www.dfg.de/foerderung/grundlagen_rahmenbedingungen/forschungsdaten/beantragbare_mittel/index.html).

Der Antrag sollte zumindest **summarische Angaben zu Art und Umfang** der anfallenden Forschungsdaten enthalten.

Das **internationale Einheitensystem (SI)** sollte verwendet oder die Möglichkeit der Umrechnung in SI-Einheiten bereitgestellt werden, um interoperable Messdaten zu gewährleisten.

Sie sollten in Fachcommunities etablierte **Dateiformate** verwenden und die vorhandenen Strukturen und Dateiformate kurz beschreiben. Als Beispiel wird das Flexible Image Transport System (FITS) in der Astronomie genannt.

Wichtige Punkte zur **Datenaufbereitung, Qualitätssicherung und Dokumentation** sollten genannt werden. Die Empfehlungen verweisen hier insbesondere auf die [Guideline 12 - Dokumentation](https://wissenschaftliche-integritaet.de/en/code-of-conduct/documentation/).

Auch wenn es noch nicht für alle Bereiche der Physik geeignete **Elektronische Laborjournale** gibt, wird die Nutzung nach Möglichkeit empfohlen.
Außer die Archivierung wird auch die Digitalisierung handschriftlicher Laborbücher für eine sinnvolle Ergänzung gehalten.

Daten sollten mit **eindeutigen Identifikatoren** versehen werden, um ihre Zuordnung und Nachnutzung zu erleichtern.
Für experimentelle Arbeiten sollten also Proben und Messungen einen Identifikator erhalten.

Die **Datenablage** sollte projekt- oder publikationsbezogen erfolgen.

Die Antragstellenden sind dafür verantwortlich, dass die datenerhebenden Projektmitarbeitenden **Schulungen zum Forschungsdatenmanagement** erhalten.
Im Antrag sollte dargelegt werden, in welcher Form dies erfolgt: Einführungsveranstaltungen, Schicht-Trainings, Einarbeitung durch erfahrende Kollaborationsmitglieder etc.
Bei Kooperationen kann dies organisatorisch erfolgen, zum Beispiel in integrierten Graduiertenschulen oder zentralen Projekten.

Die Antragstellenden sind für die **Datenqualität** (definiert in Abhängigkeit des Projektes) sowie die spätere Auswahl der Daten für die Archivierung und langfristigen Zugänglichkeit verantwortlich.

Die **Speicherung** der Daten sollte erläutert werden. Die bestehenden Vorgaben der jeweiligen Forschungseinrichtung sollten erwähnt werden.
Eine Archivierung für mindestens 10 Jahre ist gemäß den DFG-Vorgaben zu garantieren. Siehe [Leitlinie 17 - Archivierung](https://wissenschaftliche-integritaet.de/kodex/archivierung/), auf die verwiesen wird.

Die Fachkollegien sind sich bewusst, dass **nicht alle Daten aufbewahrt** werden können (z. B. Vorselektion der Rohdaten bei großen Teilchenbeschleunigern), aber Abweichungen von den Standards der Community sollten explizit im Antrag dargelegt werden.

**Rechtliche Verpflichtungen und Rahmenbedingungen** sollten vorab geprüft und im Antrag kurz benannt werden.

Sollten urheberrechtlich geschützte Daten einem **Embargo** unterliegen, um eine exklusive Nutzung zu ermöglichen, sollte der Zeitpunkt ihrer geplanten Veröffentlichung im Antrag angegeben werden.

Im Antrag sollte dargelegt werden, warum welche Daten **archivierungswürdig** sind.
Außer **Daten, die Publikationen zugrunde liegen**, sollten auch **weitere projektbezogene Messungen oder Daten aus Messreihen**, die nicht über eine Publikation einsehbar sind, dauerhaft archiviert werden.
Zur Archivierung gehört nach Möglichkeit auch die **Zugänglichkeit** der Daten.
Zur Archivierung werden **zertifizierte Repositorien und Datenzentren** empfohlen, hier bitte auch die Empfehlungen der relevanten NFDI beachten.
Mit den geeigneten Institutionen sollte vor Projektbeginn Kontakt aufgenommen werden, um den **zeitlichen und finanziellen Bedarf berücksichtigen** zu können.
Um eine spätere Nachnutzung zu ermöglichen, sollten **archivfähige, standardisierte Datenformate** gewählt werden.
Falls im Projekt **Lösungen zur Umwandlung von Datenformaten** entwickelt werden, sollten diese zur Nachnutzung bereitgestellt werden.

Der Antrag sollte darlegen, wer im Projektverlauf für das Datenmanagement verantwortlich ist.
Die **Verantwortung für das Datenmanagement und die Qualitätskontrolle** liegt bei den Projektleitenden, kann aber im Rahmen einer Kooperation auch bei einer genutzten Infrastruktur liegen.
Dies müsste im Antrag explizit erwähnt werden.

### Chemie

*Referenz: [Handlungsempfehlung zum Umgang mit Forschungsdaten](https://www.dfg.de/resource/blob/175558/e4310513bf5e2d3bc3a8bfd611c3989c/handreichung-fachkollegien-chemie-forschungsdaten-data.pdf) (2021).*

Ein **Datenmanagementplan** ist nicht vorgeschrieben, kann aber in der Planungs- und Projektphase das Forschungsdatenmanagement strukturiert unterstützen.
Hier kann der [RDMO-Fragenkatalog](https://rdmo.ub.fau.de/) „Chemie“, der auf diesen Empfehlungen basiert, helfen. <!-- TO DO: Eventuell auf eine RDMO-Instanz oder ein anderes DMP-Tool verweisen. -->

Die Empfehlung verweist auf die **NFDI-Konsortien** und deren im Aufbau befindliche Infrastrukturen, die für Einzelförderungen und Verbünde nützlich sein werden.
Für die Chemie sind das insbesondere die Konsortien [NFDI4Chem](https://www.nfdi4chem.de/), [NFDI4Cat](https://nfdi4cat.org/) und [FAIRmat](https://www.fairmat-nfdi.eu/fairmat/).

Folgende Punkte sollten im Antrag behandelt werden:

Die **Datenbeschreibung** sollte Art (rein analog, rein digital oder in beiden Formen), Ursprung (erzeugt, nachgenutzt) und Umfang der Daten beinhalten.
Der Umfang sollte zumindest in summarischen Angaben (reines Volumen etwa in GB oder Anzahl der erwarteten Dateien/Messungen) genannt werden, da so gegebenenfalls Kosten und Aufwand für das Datenmanagement besser beurteilt werden können.
Gehen Sie auch auf unterschiedliche Datenformate zum Beispiel für Rohdaten, aufbereitete Daten oder zur langfristigen Archivierung ein.
Datenformate können zum Beispiel sein:

- 3D-Modell aus cryo-EM-Messung (MRC, MRCS, TIFF, ...)
- Software-Code
- Daten aus einer Studie mit RAMAN-Spektroskopie (CSV, SPC, ...)
- kristallographische Strukturdaten (CIF)

Die **Aufbereitung, Qualitätssicherung und Dokumentation** der Daten sollte kurz erläutert werden.
Siehe auch der [Kommentar zur Dokumentation von Forschungsergebnissen in der experimentellen Chemie](https://wissenschaftliche-integritaet.de/kommentare/dokumentation-von-forschungsergebnissen-in-der-experimentellen-chemie/) als Anregung für das Vorgehen.

**Elektronische Laborbücher** werden als zukunftsweisend erachtet, jedoch gibt es noch nicht für alle Bereiche der Chemie die passende Lösung.
Als vielgenutzte Beispiele werden [Chemotion](https://www.chemotion.net/), [elabFTW](https://www.elabftw.net/) und [openBIS](https://openbis.ch/) genannt.
Siehe auch der [Kommentar zu Elektronischen Laborjournals und Repositorien in der Chemie](https://wissenschaftliche-integritaet.de/kommentare/elektronisches-laborjournal-und-repositorium-in-der-chemie/)
Falls **handschriftliche Laborbücher** verwendet werden, sollten diese digitalisiert und archiviert werden.

Die Proben und Messungen sollten einen eindeutigen **Identifikator** haben, um die Zuordnung zu ermöglichen und die Nachvollziehbarkeit des chronologischen Messablaufs, der Gesamtzahl der Messungen, der Nichtberücksichtigung von Messdaten und die Möglichkeit der späteren Nutzung zu garantieren.

Verschiedene Datentypen sollten mit standardisierten **Metadatenformaten** (beispielsweise CML oder CHMINF) beschrieben werden.

Sie sollten die einzelnen Schritte der **Datenaufbereitung** beschreiben und dabei auch auf die Datenformate eingehen.
Falls für die Datenaufbereitung ein eigenes Werkzeug entworfen oder programmiert wird, muss im Antrag begründet werden, warum kein bestehendes Werkzeug angepasst oder direkt verwendet werden kann.

Die **Datenablage** sollte projekt- oder publikationsbezogen erfolgen und dokumentiert werden.

Die **Datenqualität** ist projektabhängig zu definieren und zu dokumentieren, dazu zählen zum Beispiel Maßnahmen, die die **Datenintegrität** sicherstellen und die bereits während der **Datenerzeugung** durchgeführt werden.
Zur Sicherstellung der Datenintegrität zählen Checksummen, das Benennen von Datenstewards und automatische Backups.
Maßnahmen bei der Datenerzeugung umfassen standardisierte Protokolle zur Kalibrierung eines Messvorgangs, unabhängige Wiederholung aller Messungen/Kontrollproben und verblindete Analysen.

Die Projektmitarbeitenden sollte eine **Einweisung in das Forschungsdatenmanagement** erhalten.
In Verbünden kann dies organisatorisch zum Beispiel in integrierten Doktorandenschulen oder zentralen Projekten erfolgen.

Der Punkt **Speicherung und Sicherung** sollte die Sicherung, den Schreibschutz, die Zugriffsrechte und das Ordnungssystem umfassen.
Sie sollten die bestehenden Vorgaben der Einrichtung beachten.
Grundsätzlich ist die Archivierung von analogen und digitalen Daten für 10 Jahre zu gewährleisten.

Die **rechtlichen Verpflichtungen und Rahmenbedingungen** sollten **vorab** überprüft und im Antrag benannt werden: Datenschutzregelungen bei Arbeit mit personenbezogenen Daten, Urheberrecht zum Beispiel bei Nachnutzung eines geschützten Softwarecodes, Vertragsauflagen bei Kooperationen mit (kommerziellen) Partnern, Patentrecht, ...

Hinsichtlich **Datenaustausch und dauerhafte Zugänglichkeit der Daten** wird eine Begründung, warum welche Daten archiviert werden sollten, erwartet.
Alle relevanten und reproduzierbaren Daten, die Publikationen zugrunde liegen, sollten archiviert werden.
Dies gilt auch, wenn diese noch nicht konkret über eine Publikation einsehbar und nachnutzbar sind.
Daten, die auf etablierten Methoden und Messungen mit kommerziell erhältlichen Geräten basieren, sind standardmäßig zu handhaben und zu archivieren.
Für nicht standardisierte Daten, deren Archivierung und Zugänglichmachung nicht möglich oder praktikabel ist, sollte eine Erläuterung eingereicht werden.

Die Archivierung digitaler Daten sollte in **zertifizierten Repositorien/Datenzentren** erfolgen:
Die Nutzung der fachspezifischen Service-Angebote der NFDI ([NFDI4Chem](https://www.nfdi4chem.de/), [NFDI4Cat](https://nfdi4cat.org/), [FAIRmat](https://www.fairmat-nfdi.eu/fairmat/)) wird sehr begrüßt.
Siehe auch der [Kommentar zur Nutzung von Chemie-spezifischen Repositorien](https://wissenschaftliche-integritaet.de/kommentare/nutzung-chemie-spezifische-repositorien/).
Sie sollten sich vor der Antragsstellung mit den Repositorium beschäftigen, um die Eignung festzustellen.
Auch können so die Kosten für die Zugänglichmachung abgeschätzt und im Antrag integriert werden.
Für alle chemischen Disziplinen ohne spezifisches Repositorien bietet NFDI4Chem kostenfrei das Repositorium [RADAR4Chem](https://radar.products.fiz-karlsruhe.de/de/radarabout/radar4chem) an.

Es sollten archivfähige, standardisierte, nicht-proprietäre **Datenformate** verwendet werden, mit einer parallelen Speicherung aller Original-Rohdaten.
Weitere Kriterien sowie Erläuterungen sind zum Beispiel im [nestor Handbuch](http://nestor.sub.uni-goettingen.de/handbuch/) zu finden.
Falls Software-Lösungen für die praktikable **Umwandlung von Datenformaten** entwickelt werden, sollten diese der Fach-Community bereitgestellt werden.
Dies würde eine generelle Standardisierung unterstützen.

Die **Zuständigkeiten und Verantwortlichkeiten** für das Forschungsdatenmanagement im Projektverlauf sollten klar (möglichst namentlich) benannt werden.
Die Verantwortung für das Datenmanagement und die abschließende Qualitätskontrolle liegt bei den Projektleitenden.
Der Aufwand sollte berücksichtigt werden, um projektspezifische **Mittel** zu beantragen.

Kosten für das Datenmanagement können beantragt werden.
Dazu gehören die Kosten für Speicherplatz in einem Datenarchiv, Personalkosten, zum Beispiel 3 Personenmonate für eine/n Informatiker*in.
Speicherkosten zur Datenpublikation sollten unter der Rubrik ["Sonstige Kosten"](https://www.dfg.de/foerderung/grundlagen_rahmenbedingungen/forschungsdaten/beantragbare_mittel/index.html) zur Nutzung etablierter Infrastrukturen beantragt werden.
Dabei muss die Abgrenzung dieser Dienste von der Grundausstattung gewährleistet sein.

## Ingenieurwissenschaften

### Materialwissenschaft und Werkstofftechnik

*Referenz: [Handlungsempfehlung zum Umgang mit
Forschungsdaten : Fachforum Materialwissenschaft und Werkstofftechnik](https://www.dfg.de/resource/blob/173258/64ce3b7ee138953d13d80530d891bfa3/fk-materialwissenschaft-werkstofftechnik-2023-data.pdf) (2023).*

Die Empfehlungen nehmen direkt bezug auf die sechs Themenbereich der [DFG-Checkliste](https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/forschungsdaten/forschungsdaten_checkliste_de.pdf). Neben grundlegenden Erläuterungen werden jeweils **Kernanforderungen** dargestellt, die von Anträgen erfüllt werden **müssen**.

Diese Kernanforderungen werden hier nur knapp zusammengefasst:

1. **Beschreibung der Daten**: Es muss dargelegt werden, welche Datentypen in welcher Menge erzeugt und nach welchen Standards die unterschiedlichen Datentypen gespeichert werden. Zumindest müssen zu den im Projekt entstehenden oder verwendeten Daten Art und Format im Antrag beschrieben bzw. der Umfang abgeschätzt werden.
2. **Dokumentation und Datenqualität**: Die Antragstellenden müssen die wesentlichen Schritt der Dokumentation darlegen. Bei experimentellen Daten sind Erläuterungen zu den Messprotokollen zentraler Teil der Dokumentation. (Details siehe Originaldokument - insbesondere auch zur Verwendung von ELN und der Digitalisierung von analog erfassten Daten).
Die *Datenqualität* (u.a. Kriterien der statistischen Signifikanz und Konsistenz der Daten) soll vor dem Hintergrund des projektspezifischen Arbeitsprogramms diskutiert werden.
3. **Speicherung und technische Sicherung während des Projektverlaufs**: Redundante Datenspeicherung wird erwartet und muss im Antrag mit den wesentlichen Schritten, wie die Speicherung abläuft, erwähnt werden. Zentrale Komponenten: Zugriffsschutz, Nutzerverwaltung, Back-up, Fileserver und Share-Point-Lösungen (insbes. bei Gemeinschaftsprojekten).
4. **Rechtliche Verpflichtungen und Rahmenbedingungen**: Rechtliche Verpflichtungen und Rahmenbedingungen (Patente, Ausführverbote bei int. Kollaborationen, Dual-Use, (Nach-)Nutzungsrechte, Datenschutz, ...) müssen vorab geklärt und im Antrag benannt werden. 
5. **Datenaustausch und dauerhafte Zugänglichkeit der Daten**: Die Systematik und Grundkriterien des Datenaustausches sind darzustellen. In den Ausführungen zum Umgang mit Forschungsdaten ist darzustellen, welche Daten als extern
archivierungswürdig und als nachnutzbar angesehen werden. Es ist ebenfalls zu erörtern, wenn keine Daten für die allgemeine Nachnutzung freigegeben werden können oder die Freigabe individuell nach definierten Kriterien erfolgt. Falls Daten einer Karenzzeit oder Sperrfrist unterliegen, muss dies unter Angabe der Dauer der Zeit (Frist) dargestellt werden.
Alle relevanten und reproduzierbaren Daten, die Publikationen zugrunde liegen, sollten neben den Daten, die bereits über Publikationen aufbereitet zugänglich sind, öffentlich zugänglich archiviert bzw. publiziert werden.
6. **Verantwortlichkeit und Ressourcen**: Im Antrag muss klar dargelegt werden, wer im Projektverlauf für welche einzelnen Schritte des Forschungsdatenmanagements die Hauptzuständigkeit besitzt. 
Die langfristige Finanzierung der Infrastruktur zum Umgang mit Forschungsdaten ist sicherzustellen und im Antrag zu dem jeweiligen Forschungsvorhaben darzulegen.
