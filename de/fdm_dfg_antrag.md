# FDM im DFG-Antrag

<!-- Inhaltsverzeichnis für die GitLab-Version, muss für eine PDF-Version gelöscht/auskommentiert werden -->
[[_TOC_]]

## Allgemeine Regeln und Empfehlungen

<!-- TO DO: Hier den FDM-Kontakt der Einrichtung eintragen. -->
Bitte kontaktieren Sie möglichst früh im Antragsprozess das **Forschungsdatenmanagement-Team** der FAU: [forschungsdaten@fau.de](mailto:forschungsdaten@fau.de).
Das Forschungsdatenmanagement (FDM) sollte in die Projektplanung und Antragstellung einfließen. **Angaben zum Umgang mit Forschungsdaten** sind in Anträgen für Einzel- und Verbundvorhaben **verpflichtend**. Der Umgang mit Forschungsdaten fließt in die Begutachtung und Bewertung des Forschungsvorhabens ein.
Dies betrifft nicht nur digitale Forschungsdaten, sondern auch Angaben zu physischen Forschungsobjekten, Materialien, Substanzen oder Geweben.
Dabei immer auf die **[Forschungsdaten-Policy](https://fau.info/fdm-policy)** der FAU verweisen und sich auf die **[FAIR-Kriterien](https://www.go-fair.org/fair-principles/)** beziehen. <!-- TO DO: Ggf. hier auf die Policy der Einrichtung verweisen. -->

Hinsichtlich Ihrer bisherigen Aktivitäten sind in der neuen **[Lebenslaufvorlage](https://www.dfg.de/formulare/53_200_elan/index.jsp)** (ab 01.03.2023 obligatorisch) Angaben zu zusätzlichen Diensten an der Wissenschaft wie **Aufbau einer wissenschaftlichen Infrastruktur** und zu einer **Vielzahl von Publikationsformaten wie Datensätze und Softwarepakete** möglich.

Die **Hinweise der DFG** beachten:

- [Leitlinien zur Sicherung guter wissenschaftlicher Praxis](http://doi.org/10.5281/zenodo.3923602) (2019), Leitlinien 11-13, 17
- [Leitlinien zum Umgang mit Forschungsdaten](https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/forschungsdaten/richtlinien_forschungsdaten.pdf) (2015)
- [Checkliste zum Umgang mit Forschungsdaten](https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/forschungsdaten/forschungsdaten_checkliste_de.pdf) (12/2021)
- [Webseite über Umgang mit Forschungsdaten bei der DFG](https://www.dfg.de/foerderung/grundlagen_rahmenbedingungen/forschungsdaten/index.html)
- [Leitfaden für die Antragsstellung](https://www.dfg.de/formulare/54_01/54_01_de.pdf) (Sachbeihilfe, Emmy-Noether-Programm, Forschungsgruppe, Klinische Forschungsgruppe, Schwerpunkt) (Nr. 54.01 - 03/2024), Punkt 2.4

### Einzelvorhaben - Sachbeihilfe

Bei Einzelvorhaben sehen viele Anträge ½ - 1 ½ Seiten für das Forschungsdatenmanagement vor, 1-2 Sätze pro Thema reichen:

- [Beschreibung des Vorhabens - Projektantrag](http://www.dfg.de/formulare/53_01_elan/53_01_de_elan.rtf) (Nr. 53.01 elan - 03/24)

Das muss nicht unbedingt im **Abschnitt 2.4** des Antrags (bei Einzelvorhaben) geschehen, sondern kann bereits an anderer Stelle im Antrag erfolgt sein, etwa bei Einzelvorhaben unter **Abschnitt 2.3** bei der Beschreibung des geplanten Vorgehens. Sie können dann im Daten-Teil der Vollständigkeit halber einfach auf den relevanten Abschnitt verweisen.

### SFB

Die Checkliste [FDM im SFB-Antrag](de/fdm_sfb_antrag.md) enthält spezifische Hinweise für SFB-Anträge.

### GRK

Die Checkliste [FDM im GRK-Antrag](de/fdm_grk_antrag.md) enthält spezifische Hinweise für GRK-Anträge, insbesondere in Bezug auf das Qualifizierungskonzept. Siehe auch die Handreichung [„Erstellung eines Service-Portfolios zur FDM-Vermittlung in der Graduiertenausbildung"](https://doi.org/10.5281/zenodo.5572331) und die [FAU-Service-Matrix Schulungen](de/fau_service_matrix_schulungen.md).

### Fachspezifische Hinweise

Es gibt teilweise sehr genaue, umfangreiche [fachspezifische Anforderungen](https://www.dfg.de/foerderung/grundlagen_rahmenbedingungen/forschungsdaten/empfehlungen/index.html), die in den allgemeinen Antragsformularen nicht gut referenziert sind.

Zu folgenden Bereichen liegen Empfehlungen vor:

- Geistes- und Sozialwissenschaften: Psychologie; Erziehungs- und Bildungswissenschaften, Fachdidaktik; Sozialwissenschaften; Alte Kulturen; Sozial-und Kulturanthropologie, Außereuropäische Kulturen, Judaistik, Religionswissenschaften; Wirtschaftswissenschaften; Sprachwissenschaften; Wissenschaftliche Editionen
- Lebenswissenschaften: Biodiversitätsforschung; Medizin und Biomedizin
- Ingenieurswissenschaften: Materialwissenschaften und Werkstofftechnik
- Naturwissenschaften: Chemie; Physik; siehe auch Lebenswissenschaften für Biologie.

Die Checkliste [Fachspezifisches FDM im DFG-Antrag](de/fachspezifisches_fdm_dfg_antrag.md) bietet einen Überblick der Anforderungen und Empfehlungen.

### Methodenspezifische Hinweise - Besonderheit: Digitalisierungsprojekte

Achtung bei Digitalisierungsvorhaben: Es sind sehr detaillierte Angaben zu Metadaten, Formaten, Zugang, Archivierung, Präsentation etc. notwendig. <!-- TO DO: Mail-Adresse des FDM-Kontaktes der Einrichtung eintragen --> Fragen Sie unbedingt beim Forschungsdaten-Team ([forschungsdaten@fau.de](mailto:forschungsdaten@fau.de)) an. Einige Dokumente, die beachtet werden sollten:

- Förderprogramm „[Digitalisierung und Erschließung](https://www.dfg.de/foerderung/programme/infrastruktur/lis/lis_foerderangebote/digitalisierung_erschliessung/index.html)"
- [Formulare und Merkblätter](https://www.dfg.de/foerderung/programme/infrastruktur/lis/lis_foerderangebote/digitalisierung_erschliessung/formulare_merkblaetter/index.jsp)
- [Hinweise zur Antragstellung im Förderprogramm „Digitalisierung und Erschließung"](https://www.dfg.de/download/pdf/foerderung/programme/lis/digitalisierung_erschliessung_hinweise_antragstellung.pdf) (05/2024)
- [Merkblatt und ergänzender Leitfaden. Digitalisierung und Erschließung](http://www.dfg.de/formulare/12_15/12_15_de.pdf) (Nr. 12.15 - 05/2023)
- [DFG-Praxisregeln „Digitalisierung". Aktualisierte Fassung 2022]( https://doi.org/10.5281/zenodo.7435724) (02/2023)
- [Handreichung zur Digitalisierung archivalischer Quellen](https://www.archivschule.de/DE/forschung/digitalisierung-archivalischer-quellen/handreichungen-zur-digitalisierung-archivalischer-quellen.html) (2019)
- [Handreichungen zur Digitalisierung und Erschließung mittelalterlicher Handschriften](https://www.handschriftenzentren.de/materialien/)
- [Handreichungen zur Digitalisierung historischer Zeitungen](https://www.zeitschriftendatenbank.de/zeitungsdigitalisierung/) (2022)

### NFDI

Gegebenenfalls sollte mit den [NDFI-Konsortien](https://www.nfdi.de/konsortien/) Kontakt aufgenommen werden. Die **N**ationale **F**orschungs**D**aten**I**nfrastruktur ist eine Großinitiative des Bundes und der Länder, um (meist fachbezogene) Dienst und Best Practices im Datenmanagement zu entwickeln und zu etablieren. <!-- TO DO: Ggf. auf die Beteiligung der Einrichtung verweisen. --> Bei 11 von 27 Konsortien ist die FAU beteiligt:

- [DAPHNE4NFDI](https://www.daphne4nfdi.de/index.php) (Photonen- und Neutronenexperimente - Beugung, Tomographie, Bildgebung, Spektroskopie)
- [FAIRmat](https://www.fairmat-nfdi.eu/fairmat/) (Kondensierte Materie und chemische Physik)
- [KonsortSWD](https://www.konsortswd.de/) (Sozial-, Verhaltens- und Wirtschaftswissenschaften)
- [MaRDI](https://www.mardi4nfdi.de/) (Mathematik)
- [MatWerk](https://nfdi-matwerk.de/) (Material- und Werkstoffwissenschaft)
- [NFDI4Cat](https://nfdi4cat.org/) (Katalyseforschung)
- [NFDI4Culture](https://nfdi4culture.de/) (Materielle und immaterielle Kulturgüter)
- [NFDI4Earth](https://www.nfdi4earth.de/) (Erdsystemforschung - Geowissenschaften)
- [NFDI4Energy](https://nfdi4energy.uol.de/) (Interdisziplinäre Energiesystemforschung)
- [NFDI4Microbiota](https://nfdi4microbiota.de/) (Lebenswissenschaften)
- [NFDI4Objects](https://www.nfdi4objects.net/) (Materielle Hinterlassenschaften der Menschheitsgeschichte)

<!-- TO DO: Ggf. erwähnen, wer den Kontakt herstellen kann. -->
Den Kontakt vor Ort stellt auch gerne das [FAU Competence Center for Research Data and Information (CDI)](https://www.cdi.fau.de/) her.

Unterstützung beim Umgang mit Forschungsdaten durch die beteiligten Institutionen sollte im Antrag erwähnt werden.

## Zentrale Punkte im DFG-Antrag

Im Antrag sollten generell die nachfolgenden Themen abgedeckt werden.

1. **Datenbeschreibung:** Die Art, der Umfang, die Dokumentation, geplante Aufbewahrung und Nachnutzbarkeit der Daten sollten dargestellt werden. Erwähnen Sie hier auch die verwendeten Datenformate in den einzelnen Forschungsschritten z. B. Rohdaten, aufbereitete / anonymisierte Daten, aggregierte Daten.

2. **Dokumentation und Qualitätssicherung:** Das Ziel sollte sein, die Daten so gut zu dokumentieren, dass jemand aus der Fach-Community den Entstehungsprozess nachvollziehen und Ihre daraus abgeleiteten Ergebnisse reproduzieren kann. Um das zu unterstreichen, verweisen Sie, wo möglich, auf existierende Standards (Metadaten, Ontologien, Klassifikationen), die in der Community verbreitet sind. Falls es eine **NFDI** in Ihrem Fach gibt (siehe [oben](#nfdi)) und Sie einen dort empfohlenen Standard (oder Service) verwenden, **verweisen Sie gerne auf deren Empfehlungen**. Das zeigt auch, dass Ihnen die NFDI schon ein vertrauter Begriff ist. Wenn Sie besondere Software einsetzen oder selbstgeschriebene verwenden, dann gehen Sie auch darauf ein, wie Sie die Nachvollziehbarkeit der Forschung sicherstellen (Publizieren der eigenen Software; die verwendete Software ist in dem Fachgebiet breit im Einsatz etc.).
Erwähnen Sie außerdem knapp Qualitätssicherungsmaßnahmen jenseits von Dokumentation und Standards; etwa doppelte Datenerhebung oder feste Kalibrierungsprotokolle. Schließlich sollten Sie die Gelegenheit nutzen, **darauf hinzuweisen**, dass die Datenbeschreibung und Dokumentation mit Blick auf die **[FAIR-Kriterien](https://www.go-fair.org/fair-principles/)** erfolgt.
3. **Speicherung während der Forschungsphase:** Geben Sie an, wo die Daten gesichert werden und wie Sicherungskopien (Backups) angefertigt werden. <!-- TO DO: Bereitstellende an der Einrichtung erwähnen --> Wenn Sie ein Netzlaufwerk des [RRZE](https://www.rrze.fau.de/) nutzen, dann verweisen Sie darauf, dass das Projekt die Daten auf Serversystemen vorhält, die vom <!-- TO DO: siehe oben --> RRZE professionell gepflegt und gesichert werden. Wenn Sie Daten „im Feld“ (Wetterstationen, Interviews vor Ort, Videos von Brutgehegen ...) sammeln, sollten Sie auch knapp auf die Sicherung der Daten im Feld eingehen und wie die Daten schnellstmöglich auf Systeme mit regelmäßigem Backup kommen. Außerdem sollten Sie anmerken, ob besondere Schutzmaßnahmen ergriffen werden - etwa eine maßgeschneiderte Zugangsrechtevergabe bei sensiblen Daten. Wenn Sie bei der Speicherung Unterstützung von anderen Einrichtungen bekommen (<!-- TO DO: Ggf. austauschen -->RRZE, [HPC-Gruppe](https://www.rrze.fau.de/forschung/hpc/), [Leibniz-Rechenzentrum](https://www.lrz.de/)), verweisen Sie darauf, dass Sie die dortige Expertise nutzen. Bei Projekten mit Beteiligten außerhalb <!-- TO DO: Durch die Einrichtung ersetzen -->der FAU, bietet es sich an, auch auf Zugriffsmöglichkeiten für externe Partner und den geplanten Datentransfer einzugehen. <!-- TO DO: Ggf. eine geeignete Cloudlösung der Einrichtung erwähnen. --> Die [FAUbox](https://faubox.rrze.uni-erlangen.de/login) wird vom Datenschutzbeauftragten der FAU als geeignet für sensible Daten gesehen.
4. **Rechtliche Verpflichtungen:** Dieser Punkt ist besonders wichtig, wenn Sie urheberrechtlich oder datenschutzrechtlich relevante Daten erheben. Auch bei beabsichtigten Patenten oder bei Industrievereinbarungen wäre ein Hinweis auf daraus resultierende Verpflichtungen in Bezug auf die Daten sinnvoll. Wenn Sie unproblematische Daten haben (Messung von Atomen in einem optischen Gitter; Daten zur Bruchfestigkeit einer Legierung; Videos von Giraffen, auf denen keine Menschen zu sehen sind ...), dann genügt ein kurzer Satz, etwa: „Die im Forschungsprogramm entstehenden Daten unterliegen keinen rechtlichen oder forschungsethischen Vorgaben oder Verpflichtungen. <!-- TO DO: An die Einrichtung anpassen. --> Das Projekt folgt den in der [Forschungsdaten-Policy](https://fau.info/fdm-policy) der FAU festgelegten Prinzipien.“ So verweisen Sie auch gleich auf unsere FAU-Datenpolicy. Wenn es im Fach übliche Anforderungen oder etablierte Praktiken im Bereich Datenspeicherung gibt, folgen Sie diesen so weit möglich und weisen im Antrag auch explizit darauf hin.
5. **Datenaustauch und Zugang zu den Daten:** Grundsätzlich ist es der Wunsch der DFG, dass Daten der Community soweit möglich und sinnvoll zur Nachnutzung zur Verfügung stehen. Geben Sie an, warum Daten (nicht) zur Verfügung gestellt werden. Hier können Sie Nachnutzungspotenzial, großen Aufwand bei der Erhebung, unikalen Charakter oder etwa Datenschutzschranken, Urheberrecht, berechtigte Interessen von Industriepartnern etc. angeben. Wenn Sie die Daten nicht in einer langfristigen Infrastruktur bereitstellen bzw. archivieren (etwa in [Zenodo](https://zenodo.org/), [RatSWD-zertifizierte Repositorien](https://www.konsortswd.de/datenzentren/alle-datenzentren/), NFDI-empfohlenen Infrastrukturen ...), sollten Sie immer auf eine Archivierung der Daten, auf denen Publikationen fußen, für mindestens zehn Jahre (DFG-Vorgabe) an <!-- TO DO: an die Einrichtung anpassen: Repositorium und Kontakt. -->der FAU in der [FAUDataCloud](https://www.cdi.fau.de/services/faudatacloud-it-services-fuer-forschungsdaten/) hinweisen (hier bitte vor Antragstellung den Volumenbedarf mit dem [CDI](https://www.cdi.fau.de/) absprechen).
Wenn Sie Daten für andere Forschende bereitstellen bzw. Daten publizieren, dann gehen Sie bitte auf die Zugangsbedingungen ein (beispielsweise Sperrfristen bis zur Zugänglichkeit, Zugangsvoraussetzungen).

6. **Verantwortlichkeiten und Ressourcen:**
Schließlich sollten Sie die Personen benennen, die im Projekt für einzelne Schritte im Datenmanagement letztendlich verantwortlich sind. Typische Bereiche wären Einarbeiten neuer Mitarbeitender in die FDM-Workflows, Koordination zwischen Arbeitsgruppen, langfristige Speicherung. In kleineren Projekten ist dies oft eine Person. Sie sollten diese wenn möglich namentlich nennen. Geben Sie auch an, wer bzw. welche Stelle für den Umgang mit den Daten nach Ende der Förderung zuständig ist (Stichwort: Nachhaltigkeit). <!-- TO DO: Die oben erwähnten Speicher-Bereitstellenden erwähnen. --> Bei der Speicherung können Sie hier auf die Dienste des RRZE beziehungsweise der FAUDataCloud (siehe auch 5.) verweisen. Geben Sie hierbei auch an, welche Ressourcen in das FDM fließen (Personalmittel und sonstige Kosten, siehe auch nächster Abschnitt).

## Mittel für das Datenmanagement

Kosten für das Datenmanagement, z. B. Nachnutzungsgebühren für qualitative Forschungsdaten, Archivierungsgebühren oder Aufarbeitungsmaßnahmen für die Nachnutzbarkeit der Daten, können projektspezifisch beantragt werden. In einzelnen Projekten werden punktuell IT-Personalressourcen für das Datenmanagement benötigt. Beispielsweise eine IT-Stelle für nur 3 Monate, um eine Datenbank oder eine Forschungsumgebung anzupassen. <!-- TO DO: Hier ggf. die Möglichkeit erwähnen, auf einen IT-Expert*innen-Pool der Einrichtung zurückzugreifen. --> In solchen Fällen bitte das [CDI](https://www.cdi.fau.de/) kontaktieren: Das CDI hat Zugriff auf einen Pool von IT-Expert*innen, mit dem sichergestellt werden kann, dass entsprechend beantragte Personenmonate auch zum Projektbeginn bereitstehen.
