# FDM im GRK-Antrag

<!-- Inhaltsverzeichnis für die GitLab-Version, muss für eine PDF-Version gelöscht/auskommentiert werden -->
[[_TOC_]]

## Allgemeine Regeln und Empfehlungen

Bitte kontaktieren Sie möglichst früh im Antragsprozess das **Forschungsdatenmanagement-Team** der FAU: [forschungsdaten@fau.de](forschungsdaten@fau.de). <!-- TO DO: Einrichtungsname und Kontakt der FDM-Verantwortlichen der Einrichtung ersetzen. -->

Beachten Sie die **DFG-Dokumente** zu Forschungsdaten und die Hinweise zu den NFDI-Konsortien in [FDM in DFG-Anträgen](de/fdm_dfg_antrag.md).

Beachten Sie außerdem die **GRK-spezifischen** Hinweise der DFG:

- [Leitfaden für Antragsskizzen Graduiertenkollegs und Internationale Graduiertenkollegs](https://www.dfg.de/formulare/1_303/1_303_de.pdf) [Nr. 1.303 - 01/25]

- [Leitfaden für die Antragstellung. Graduiertenkollegs und Internationale Graduiertenkollegs (Einrichtungsanträge)](http://www.dfg.de/formulare/54_05/54_05_de.pdf), Punkt B.3.2 (Umgang mit Forschungsdaten) und Punkt B.4.1 (Studienprogramm) [Nr. 54.05 - 01/25]

Der Umgang mit Forschungsdaten sollte in der Skizze und im Antrag dargestellt werden:

In der **Skizze** wird üblicherweise ½ - 1 Seite dem Forschungsdatenmanagement und dem Qualifizierungskonzept im Bereich FDM gewidmet:

- [Beschreibung des Vorhabens - Antragsskizzen Graduiertenkollegs und Internationale Graduiertenkollegs](http://www.dfg.de/formulare/53_60_elan/53_60_de_elan.rtf) Abschnitt 2 "Forschungsprogramm" und Abschnitt 3 "Qualifizierungs- und Betreuungskonzept" [Nr. 53.60 elan - 08/22].

Im **Antrag** werden bis zu 2 Seiten inklusive einem Abschnitt zum Forschungsdatenmanagement im Qualifizierungskonzept erwartet:

- [Beschreibung des Vorhabens Graduiertenkollegs und Internationale Graduiertenkollegs](http://www.dfg.de/formulare/53_61_elan/53_61_de_elan.rtf), Abschnitt 3 "Forschungsprogramm" und Abschnitt 4 "Qualifizierungskonzept" [Nr. 53.61 elan - 08/22].

## Zentrale Punkte im GRK-Antrag

Beachten Sie für die Angaben zum Forschungsdatenmanagement im GRK-Antrag bitte den Abschnitt "**[Zentrale Punkte im DFG-Antrag](de/fdm_dfg_antrag.md#zentrale-punkte-im-dfg-antrag)**" in [FDM im DFG-Antrag](de/fdm_dfg_antrag.md).

Beachten Sie auch die **[fachspezifischen Empfehlungen](https://www.dfg.de/foerderung/grundlagen_rahmenbedingungen/forschungsdaten/empfehlungen/index.html)** der DFG.
Die Handreichung [Fachspezifisches FDM im DFG-Antrag](de/fachspezifisches_fdm_dfg_antrag.md) bietet einen Überblick der Anforderungen und Empfehlungen.

Für **GRK-Anträge** kommen folgende Punkte hinsichtlich des Forschungsdatenmanagements zusätzlich hinzu:

- **Gemeinschaftliche Konzeptentwicklung** zum Forschungsdatenmanagement innerhalb des Kollegs
- **Entwicklung von Methoden** im Bereich Forschungsdaten
- **Auf- und Ausbau von Dateninfrastrukturen**: Anschlussfähigkeit an existierende Strukturen, Überlegungen zur Nachhaltigkeit
- In welcher Form unterstützen die **am Verbund beteiligten Institutionen** das Daten-und Informationsmanagement?
- Im **Qualifizierungskonzept** sollten Schulungen zur Erhebung, Sicherung, Aufbereitung und nachhaltigen Bereitstellung von Forschungsdaten vorgesehen werden. Welche Ansprechpersonen innerhalb der <!--TO DO: Einrichtungsname ersetzen--> FAU und welche externen Dozierenden werden in das Schulungskonzept eingebunden? Mögliche Themenbereiche, siehe unten. Das Forschungsdatenmanagement-Team der FAU (<forschungsdaten@fau.de>) hilft auch hier gerne weiter. <!-- TO DO: Einrichtungsname und Kontakt der FDM-Verantwortlichen der Einrichtung ersetzen. -->

## Themenbereiche für FDM-Schulungen innerhalb des GRK

Die Vermittlung von Kernkompetenzen im Forschungsdatenmanagement an Promovierende wird immer wichtiger und sollte in einem GRK-Antrag bedacht werden.
Das Referat Forschungsdatenmanagement bietet einen
[Überblick über mögliche Schulungsthemen und Kontakte](de/fau_service_matrix_schulungen.md). <!-- TO DO: Durch die betreffende Kontaktstelle der Einrichtung ersetzen. -->
Für weitere Erläuterungen zu den Themenbereichen, die Sie in der Service-Matrix finden, siehe ["Erstellung eines Service-Portfolios zur FDM-Vermittlung in der Graduiertenausbildung"](https://doi.org/10.5281/zenodo.5572331).

Es bieten sich **10 zentrale Themenbereiche**, die sowohl Grundlagen als auch fachliche Themen und Projektspezifika betreffen, an:

1. (Digitale) Forschungsdaten und Datenmanagement
2. Rahmenbedingungen (Policies und Forderungen auf lokaler, nationaler und internationaler Ebene)
3. Datenmanagementpläne
4. Ordnung und Struktur
5. Speichern und Backup sowie Zugriffssicherheit
6. Dokumentation und Metadaten
7. Langzeitsicherung/-archivierung
8. Publikation von Daten
9. Rechtliche Aspekte
10. Besondere Infrastruktur/Software-Tools vor Ort

Für eine einführende Schulung, die alle Themen basal abdeckt, kann auf das Angebot der Universitätsbibliothek zurückgegriffen werden.
<!-- TO DO: Durch die betreffende Einrichtung ersetzen. -->

Für Fragen und Hilfe für die Zusammenstellung eines geeigneten Schulungsprogramms wenden Sie sich gerne an: [forschungsdaten@fau.de](mailto:forschungsdaten@fau.de).
<!-- TO DO: E-Mail-Adresse der FDM-Ansprechpersonen der Einrichtung -->

## Antrag: Textbeispiele zum Qualifizierungskonzept

"Das Qualifizierungskonzept umfasst Workshops zum Stellen von Förderanträgen, zum Forschungsdatenmanagement und Umgang mit sensiblen Daten.
Wo sinnvoll werden die FAU-internen Angebote durch externe Expertise gezielt vertieft, z. B. durch Workshops von *XYZ*."
<!-- TO DO: Durch interne Angebote der Einrichtung ersetzen. -->

"Aufbauend auf den Grundlagen-Seminaren wird das Thema Forschungsdatenmanagement im Graduiertenkolleg fach- und methodenspezifisch vertieft, Schwerpunkte sind dabei unter anderem *\[z. B. Social-Media-Daten, Urheberrecht, Datenschutz, Wikidata, DMPs in den Geisteswissenschaften, Literaturverwaltung ...\]*.
Hierfür wird sowohl auf Angebote des Digital Humanities Lab, der Universitätsbibliothek und des FAU Competence Center for Research Data and Information als auch auf externe Dozierende zurückgegriffen."
<!-- TO DO: Durch betreffende Serviceangebote der Einrichtung ersetzen. -->
