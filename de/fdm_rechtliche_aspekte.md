# FDM & Rechtliche Aspekte

<!--Zur Info: In den Quellenangaben der Gesetze sind zwischen den Abkürzungen und den Nummern geschützte Leerzeichen (&nbsp;), z. B. Art.&nbsp;4 Nr.&nbsp;1 DSGVO. Diese möglichst nicht entfernen.-->

[[_toc_]]

**Disclaimer:
Diese Handreichung soll Forschenden und Beratenden eine erste Orientierung zu gegebenenfalls rechtlich relevanten Aspekten im Forschungsdatenmanagement bieten.
Viele rechtliche Fragen sind in diesem Bereich noch ungeklärt.
Da meist eine Einzelfallprüfung erforderlich ist, ersetzt die Handreichung ausdrücklich keine verbindliche Rechtsberatung.**

## Einleitung

In Bezug auf Forschungsdaten gibt es im rechtlichen Bereich drei grundlegende Themen:

- Zuordnung der Forschungsdaten: Wer hat die Urheberschaft beziehungsweise Autorschaft? Wer hat die Entscheidungsbefugnis über die Veröffentlichung?
- Welche Lizenzbedingungen gelten für die Veröffentlichung?
- Welche rechtlichen Grenzen gibt es bei dem Umgang mit Forschungsdaten Dritter (Urheberrecht, Datenschutz)?

Verstöße gegen das Urheber-, Datenschutz- und Leistungsschutzrecht sind für Forschende mit Haftungsrisiken verbunden.
Daher empfiehlt es sich, die rechtlichen Implikationen zu Beginn eines Forschungsprojekts zu berücksichtigen.

Die Urheber- und Leistungsschutzrechte sollten so früh wie möglich – in der Regel vor der Erhebung der Forschungsdaten – geklärt werden.
Je nach Ergebnis sollte eine schriftliche Vereinbarung getroffen werden.
Unklarheiten innerhalb interner Arbeitsgruppen können durch interne Richtlinien z. B. auch in Datenmanagementplänen vorgebeugt werden.

## Datenschutzrecht

<!--TODO: Durch die Kontakte der eigenen Einrichtung ersetzen-->
Kontakte an der FAU:

- Referentin für Urheberrecht, Datenschutzrecht, Veröffentlichungsrecht an der Universitätsbibliothek: Petra Heermann, E-Mail: [petra.heermann@fau.de](mailto:petra.heermann@fau.de), [Website](https://ub.fau.de/forschen/daten-software-forschung/)
- [Datenschutzbeauftragter der FAU](https://www.fau.de/fau/leitung-und-gremien/gremien-und-beauftragte/beauftragte/datenschutzbeauftragter)

Forschungsdaten unterliegen dem Datenschutzrecht, wenn sie **personenbezogene oder personenbeziehbare Informationen** enthalten.

Bei **Daten mit Personenbezug** handelt es sich um „alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person [...] beziehen;  als identifizierbar wird eine natürliche Person angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung oder zu einem oder mehreren besonderen Merkmalen identifiziert werden kann.“ (Art.&nbsp;4 Nr.&nbsp;1 DSGVO).

Im Allgemeinen:

- **Daten ohne Personenbezug** wie Messdaten aus den Naturwissenschaften, Genom von Tieren und Pflanzen sind **datenschutzrechtlich nicht relevant**.
- **Personenbezogene Daten** sind Daten, die sich auf identifizierbare Menschen beziehen. Beispielsweise sind dies Name, Alter, Aussehen, Wohnort, Kennnummer, Standort, Krankheiten, IP-Adresse, Foto, E-Mail-Adresse, Telefonnummer, genetische oder biometrische Daten, Gesundheitsdaten, Matrikelnummern, Usernamen auf Onlineplattformen etc.

Das Datenschutzrecht ist seit 2018 durch die [**Datenschutz-Grundverordnung (DSGVO)**](https://dsgvo-gesetz.de/) europarechtlich geregelt.
An Hochschulen gilt zudem das jeweilige Landesdatenschutzgesetz.
Datenerhebung erlaubende Gesetze sind: Art.&nbsp;6 DSGVO, §&nbsp;27 BDSG (für nichtöffentliche Forschungseinrichtungen) sowie die jeweiligen Landesdatenschutzgesetze für Hochschulen.

Unter Art.&nbsp;6 DSGVO fallen personenbezogene Daten der „normalen Kategorie“. Zu Daten der „besonderen Kategorien“ gehören **besonders schützenswerte Daten**, die unter Art.&nbsp;9 DSGVO fallen.
Zum Beispiel sensible Daten wie ethnische Herkunft, religiöse und philosophische Angehörigkeit, Gewerkschaftszugehörigkeit, politische Meinung, Gesundheit, Sexualleben oder sexuelle Orientierung etc. dürfen nur unter besonderen Bedingungen verwendet werden.

Allgemein gilt:

Die **Verarbeitung personenbezogener Daten** bedarf immer einer Rechtsgrundlage.
Diese kann sich aus einem Gesetz ergeben oder in der Forschung meist eine **Einwilligung** sein.
Die Einwilligung muss **vor Beginn der Erhebung** vorliegen.
Eine Einwilligung setzt eine Information gemäß den Anforderungen des Art.&nbsp;13 DSGVO voraus.
Ausnahmen zu den Informationspflichten können sich aus Art.&nbsp;89 Abs.&nbsp;2 DSGVO ergeben.

Es gilt der Grundsatz der **Zweckbindung** (Art.&nbsp;5 Abs.&nbsp;1 lit.&nbsp;b DSGVO).
Dieser besagt, dass personenbezogene Daten für festgelegte, eindeutige und legitime Zwecke erhoben werden.
Das bedeutet, dass sie nicht in einer mit diesen Zwecken nicht zu vereinbarenden Weise weiterverarbeitet werden dürfen.
Für wissenschaftliche Zwecke ist der Grundsatz der Zweckbindung teilweise durchbrochen.
Fragen Sie frühzeitig die Datenschutzbeauftragten und ggf. die Ethikkommission.

In der Forschung ist der Zweck durch das jeweilige Forschungsprojekt festgelegt.
Es gilt der Grundsatz der **Datenminimierung**: nur personenbezogene Daten, die für die Erreichung des Zwecks erforderlich sind, dürfen erhoben werden (Art.&nbsp;5 Abs.&nbsp;1 lit.&nbsp;b DSGVO, Art.&nbsp;4 Abs.&nbsp;2 Nr.&nbsp;2 BayDSG).
<!--TODO: Ggf. das BayDSG durch eigene Landesdatenschutzgesetze ersetzen-->

Personenbeziehbare Daten sollten, wenn möglich, gleich bei der Erhebung **pseudonymisiert** werden (durch ID, Code o. ä.).
Ansonsten sind personenbezogene Daten, sobald der Forschungszweck es erlaubt, zu pseudonymisieren (Art.&nbsp;89 Abs.&nbsp;1 DSGVO).
Wenn die Daten geteilt werden, sind sie grundsätzlich zu pseudonymisieren.
Soweit eine **Anonymisierung** nicht den Forschungszweck beeinträchtigt oder die Forschungsergebnisse verfälscht, ist diese für die Veröffentlichung der Daten vorzunehmen.
(Siehe [Handreichung Datenschutz](https://doi.org/10.17620/02671.50) des RatSWD, S. 29f.)
**Pseudonymisierte Daten** unterliegen weiterhin den Regeln des Datenschutzrechts, da die ursprüngliche Information wiederhergestellt werden kann.

Durch **Anonymisierung** kann oft Abhilfe geschafft werden, sofern jene nicht die Aussagekraft der Daten beeinflusst.
Anonymisiert sind personenbezogene Daten erst dann, wenn auch kein theoretischer Bezug auf konkrete Personen mehr denkbar ist (z. B. statistische Daten).
Oft ist durch Kombination mit anderen Datenquellen doch noch eine Personenbeziehbarkeit gegeben.
Anonymisierte Daten, die nicht mehr einer bestimmten Person zugeordnet werden können, unterliegen nicht dem Datenschutzrecht.

Folgend einige Tools und Informationen, die bei der Anonymisierung/Pseudonymisierung unterstützen können:

- [interactive Virtual Assistant 1 - Anwendungsbereich DSGVO](https://www.berd-nfdi.de/iva1/): Unterstützung bei der Prüfung, welche datenschutzrechtlichen Vorschriften bei einem Forschungsvorhaben zu beachten sind.
- [interactive Virtual Assistant 2 - Einwilligung](https://www.berd-nfdi.de/iva2/): Unterstützung bei der Prüfung der Voraussetzungen für eine wirksame Einwilligung.
- [Amnesia](https://amnesia.openaire.eu/) von OpenAire: bei der Anonymisierung unterstützendes Tool für komplexe objektrelationale Daten in Textdateien.
- [QualiAnon](https://www.qualiservice.org/de/helpdesk/webinar/tools.html) von Qualiservice: Tool für die Anonymisierung/Pseudonymisierung von Textdaten.
- [Hilfestellungen zur Anonymisierung und Pseudonymisierung](https://www.forschungsdaten-bildung.de/anonymisieren-pseudonymisieren) vom VerbundFDB

### Kenntnisnahme von Straftaten im Rahmen von Forschungsprojekten

Die Handreichung [Umgang mit der Kenntnisnahme von Straftaten im Rahmen der Durchführung von Forschungsvorhaben](https://doi.org/10.17620/02671.74) des RatSWD beschäftigt sich mit dem ethischen Dilemma, vor dem Forschende stehen, ob die Einhaltung von Datenschutzvorgaben höher zu bewerten ist als der Schutz von (potenziellen) Tatopfern.
Insbesondere Forschende, die sich mit der Erforschung illegaler Aktivitäten befassen, können in einen Konflikt zwischen rechtlichen und ethischen Erwägungen geraten.

Bei Kenntnis bestimmter Straftaten ist die Anzeige bei der zuständigen Behörde -- trotz Datenschutz -- erlaubt, wenn nicht sogar erforderlich.
Unterlassene Informationsweitergabe ist dann relevant, wenn die Erkenntnisse die Planung von schwerwiegenden Verbrechen (§&nbsp;138 StGB) betreffen oder die Taten einen Unglücksfall für das Tatopfer darstellen (§&nbsp;323c StGB).

In datenschutzkonformen Einwilligungserklärungen sollten Vertraulichkeitsversprechen dahingehend eingeschränkt werden, dass die Forschenden unter Umständen dazu verpflichtet sein können, mitgeteilte Informationen weitergeben zu müssen, um sich nicht selbst strafbar zu machen (vgl. S. 33 der [Handreichung](https://doi.org/10.17620/02671.74)).

Die Erläuterung geschieht anhand von vier Beispielen:

**Beispiel 1: Bekanntwerden einer geplanten oder bereits durchgeführten rechtsextremistischen Straftat (Raubüberfall)**

Planung eines Raubüberfalls (§&nbsp;249 StGB):

- Unterlassen der Informationsweitergabe = strafbar (§&nbsp;138 Abs.&nbsp;1 StGB).
- Unglücksfall für das potenzielle Opfer = unterlassene Hilfeleistung (§&nbsp;323c Abs.&nbsp;1 StGB); Geheimhaltungspflicht des Forschenden tritt zurück.
- Lebenswichtige Interessen des potenziellen Opfers können betroffen sein, deswegen können Art.&nbsp;6 Abs.&nbsp;1 UAbs.&nbsp;1 lit.&nbsp;d DSGVO und Art.&nbsp;6 Abs.&nbsp;1 UAbs.&nbsp;1 lit.&nbsp;f DSGVO herangezogen werden.

Gefallen an der Idee eines Raubüberfalls:

- Tatneigung einer Person ist nicht mitteilungspflichtig (§&nbsp;138 StGB).
- Geheimhaltungspflicht tritt nicht zurück.
- Lebenswichtige Interessen des potenziellen Opfers sind noch nicht konkret betroffen.

Durchgeführter Raubüberfall:

- Vergangene Straftaten sind nicht meldepflichtig (§&nbsp;138 StGB).
- Es liegt kein Unglücksfall für das Tatopfer mehr vor, da die Tat schon begangen wurde.
- Lebenswichtige Interessen des potenziellen Opfers sind nicht mehr konkret betroffen.

**Beispiel 2: Straftat im Kontext sexuellen Missbrauchs**

- Keine Strafbarkeit bei Nichtweitergabe der Information (§&nbsp;138 StGB).
- Nichtweitergabe der Information bei bevorstehenden Taten: Strafbare unterlassene Hilfeleistung, da es sich um einen Unglücksfall aus Sicht des Opfers handelt (§&nbsp;323c Abs.&nbsp;1 StGB).
- Lebenswichtige Interessen des potenziellen Opfers können betroffen sein, deswegen können Art.&nbsp;6 Abs.&nbsp;1 UAbs.&nbsp;1 lit.&nbsp;d DSGVO und Art.&nbsp;6 Abs.&nbsp;1 UAbs.&nbsp;1 lit.&nbsp;f DSGVO die Informationsweitergabe begründen.

**Beispiel 3: Kenntnisnahme von Selbsttötungsabsichten**

- Suizid ist straflos (§&nbsp;138 StGB) daher keine Strafbarkeit bei Nichtweitergabe der Information.
- kein Unglücksfall, daher keine unterlassene Hilfeleistung (§&nbsp;323c StGB).
- Selbstbestimmungsrecht, daher Informationsweitergabe nicht erlaubt.

**Beispiel 4: Grenzfall: Pflege / Vernachlässigung**

- Da Vernachlässigung keine Straftat (§&nbsp;138 StGB) ist, ist Nichtweitergabe der Information an sich nicht strafbar. Informationsweitergabe kann verpflichtend sein, wenn eine gravierende Vernachlässigung feststeht und die zu pflegende Person in Lebensgefahr schwebt.
- Unterlassene Hilfeleistung kommt in Betracht, wenn die Vernachlässigung zu Körperverletzung oder Tod führt (§&nbsp;323c Abs.&nbsp;1 StGB).
- Wenn lebenswichtige Interessen des potenziellen Tatopfers betroffen sind, kann die Informationsweitergabe auf Art.&nbsp;6 Abs.&nbsp;1 UAbs.&nbsp;1 lit.&nbsp;d und  lit.&nbsp;f DSGVO gestützt werden.

Fazit: Um das Vorgehen bei Kenntnisnahme von Straftaten rechtlich zu erörtern, sollten sich Forschende insbesondere auf §&nbsp;138 StGB (Nichtanzeige geplanter Straftaten), §&nbsp;323c StGB (Unterlassene Hilfeleistung), Art.&nbsp;6 Abs.&nbsp;1 UAbs.&nbsp;1 lit.&nbsp;d DSGVO sowie Art.&nbsp;6 Abs.&nbsp;1 UAbs.&nbsp;1 lit.&nbsp;f DSGVO (Rechtsgrundlagen nach DSGVO für die Informationsweitergabe) stützen.

## Urheberrecht & Leistungsschutzrecht

<!--TODO: Durch den Kontakt der eigenen Einrichtung ersetzen-->
Kontakt an der FAU: Referentin für Urheberrecht, Datenschutzrecht, Veröffentlichungsrecht an der Universitätsbibliothek: Petra Heermann, E-Mail: [petra.heermann@fau.de](mailto:petra.heermann@fau.de), [Website](https://ub.fau.de/forschen/daten-software-forschung/)

Das Urheberrecht schützt die Schöpfer*innen von Werken der Literatur, Wissenschaft und Kunst (§&nbsp;1 UrhG).
Bei der **Nachnutzung fremder Materialien** sollte die urheberrechtlich gebotene Zustimmung **vor der Arbeit** mit ihnen eingeholt werden.
Das Urheberrecht schützt während einer Schutzdauer von **70 Jahren**  bis nach dem Tod des Urhebers/der Urheberin die ideellen und wirtschaftlichen Interessen am Werk.
Sowohl das unberechtigte Herunterladen und Hochladen von urheberrechtlich geschützten Forschungsdaten verletzt das Urheberrecht.
Die Nutzung sollte nur mit gesetzlicher Erlaubnis oder mit der Zustimmung der Rechteinhabenden erfolgen.

Für **qualitative Daten** sollte ein Urheberrechtsschutz angenommen werden, sofern keine Einzelfallprüfung möglich ist.
Vorausgesetzt wird eine hinreichende Individualität.
Individualität ergibt sich durch den Gestaltungsspielraum, der nicht durch die Fachsprache oder durch Sachzwänge vorgegeben ist.
Grundsätzlich gilt: Ideen und Inhalte sind frei, urheberrechtlicher Schutz besteht nur für die konkrete Darstellung, nicht für die in den Daten enthaltenen Informationen.

**Quantitative Daten** und **maschinell erfasste Rohdaten** sind nicht urheberrechtlich geschützt.
Auch rein KI-generierte Texte sind nicht urheberrechtlich schutzfähig, da keine persönliche geistige Schöpfung gegeben ist.

Zumeist sind **Texte, Abbildungen, Kunstobjekte und Software** geschützt.
Historische Objekte können gemeinfrei sein.

Nicht nur Daten und Datensätze können geschützt sein, sondern auch **Datenbanken**, in denen sie abgespeichert sind, und die **Software**, die zur Bedienung erforderlich ist.
Für **Sammel- und Datenbankwerke** gilt die Person als Urheberin, die die Parameter der Auswahl und Anordnung der Elemente festgelegt hat (§&nbsp;7 UrhG).
Haben mehrere Personen Forschungsdaten zusammen generiert, ist für jeden Bestandteil einzeln festzustellen, wer Schöpfer\*in/Erzeuger\*in ist.

Auch im Falle der Übertragung von Nutzungsrechten, z. B. an die Hochschule im Rahmen eines Dienstverhältnisses, bleibt das Urheberrecht davon unberührt.

Prüfen Sie vor der Nachnutzung von Forschungsdaten (Daten, Datenbanken, Software), ob ein urheberrechtlicher Schutz besteht. Holen Sie gegebenenfalls die Zustimmung der Rechteinhabenden ein; dies kann z. B. über eine Lizenz erfolgen.

### Leistungsschutzrecht

Das Leistungsschutzrecht setzt keine schöpferische Leistung voraus.
Ansonsten entspricht das Leistungsschutzrecht dem Umfang des Urheberrechts.
Das Leistungsschutzrecht betrifft **Lichtbilder, Laufbilder, Tonträger und Datenbanken**.

#### Lichtbilder

Für Fotografien, die keine ausreichende Individualität aufweisen und damit nicht urheberrechtlich geschützt sind, gilt das Leistungsschutzrecht (§&nbsp;72 UrhG).
Die Schutzfrist für Lichtbilder beträgt **50 Jahre**.

Eine Besonderheit sind **Digitalisate**:
Digitalisate **zweidimensionaler Objekte** unterliegen meist nicht dem Leistungsschutzrecht.
Digitalisate **dreidimensionaler Objekte** erfordern eine komplexere handwerklich-technische Leistung und können daher unter das Leistungsschutzrecht fallen.

#### Datenbanken

Das Leistungsschutzrecht für Datenbanken kommt dem **Datenbankhersteller** zu (§&nbsp;87a UrhG).
Dies bezeichnet den „Investor“, der eine „wesentliche Investition“ getätigt hat.
Zu berücksichtigende Investitionen sind: Einsatz von Geld, Zeit und Arbeit bei der Beschaffung, Überprüfung oder Darstellung des Datenbankinhaltes.
Allgemein gilt: Datenbankhersteller ist, wer die zur Erstellung notwendige finanzielle Investition getätigt hat, d. h. eine Universität (im Rahmen des Arbeitsverhältnisses) oder eine Fördereinrichtung (bei Drittmitteln) können als juristische Personen die Rolle der Datenbankherstellerin innehaben.
Der Datenbankhersteller hat das „ausschließliche Recht, die Datenbank insgesamt oder einen nach Art und Umfang wesentlichen Teil der Datenbank zu vervielfältigen, zu verbreiten und öffentlich wiederzugeben“ (§&nbsp;87b Abs.&nbsp;1 Satz&nbsp;1 UrhG).
Das Leistungsschutzrecht für Datenbanken liegt bei **15 Jahren** nach der Veröffentlichung oder 15 Jahre ab Herstellung bei fehlender Veröffentlichung.
Die Frist verlängert sich nach einer wesentlichen Überarbeitung.

#### Tonträger

Im Bereich Forschungsdaten sind z. B. Aufnahmen von Interviews oder von Tönen und Geräuschen relevant.
Maßgeblich ist die Gewährleistung einer wiederholten Wiedergabe in gleicher Form, nicht der Aufwand der Tonaufnahme.
Das Tonträgerrecht (§&nbsp;85 UrhG) bezieht sich auf die **wirtschaftliche Investition** in die Herstellung der Aufnahme und kommt daher dem Arbeitgeber zu.
Das Tonträgerrecht gilt **70 Jahre** nach Erscheinen des Tonträgers oder 70 Jahre nach erstmaliger Nutzung eines nicht-erschienenen Tonträgers.
Dieser muss innerhalb von 50 Jahren nach Herstellung mit Erlaubnis benutzt worden sein.
Für nicht-erschienene und nicht erlaubt genutzte Tonträger erlischt das Recht nach 50 Jahren.

#### Laufbilder

Das Leistungsschutzrecht für Laufbilder (§&nbsp;95 UrhG) bezieht sich auf Filmwerke, aber auch auf Filmausschnitte, Live-Sendungen, Streams von Parlamentssitzungen, Dokumentarfilme, Amateurfilme, Videoaufnahmen zur Dokumentation von Interviews etc.
Die Schutzrechte stehen dem Arbeitgeber als Träger der **wirtschaftlichen und organisatorischen Verantwortung** zu.
Das Leistungsschutzrecht besteht **50 Jahre** ab Erscheinen oder ab Herstellung bei Nichtveröffentlichung (§&nbsp;94 Abs.&nbsp;3 UrhG).

## Nutzungsrechte (Veröffentlichung & Verwertung)

Auch an urheberrechtlich geschützten Daten können Nutzungsrechte durch Dritte bestehen.
Nutzungsrechte können durch einen Nutzungsrechte-Vertrag eingeräumt werden oder implizit aus einem privatrechtlichen Arbeits- beziehungsweise einem öffentlich-rechtlichen Dienstverhältnis entstehen (siehe Beispiele unten).
**Nutzungsrechte betreffen die Vervielfältigung, die Speicherung in einem Repositorium oder die Zugänglichmachung.**

Trotz Nutzungsrechten gilt eine Rücksichtnahmepflicht des Dienstherrn insbesondere gegenüber Forschenden mit Qualifikationsstellen wie Doktoranden oder Habilitanden: Wenn eine Forschende im allgemeinen Einvernehmen Forschungsdaten für ihre Qualifikationsarbeit nutzen darf, so sollte dies im Sinne der Wissenschaftsfreiheit und der Berufsfreiheit nicht später verwehrt werden, auch wenn sie die Institution vor der Fertigstellung ihrer Qualifikationsarbeit verlässt.

### Hochschulprofessor*innen

(*Gilt auch für Privatdozierende, außerplanmäßige Professor\*innen und Gastdozierende*)

Für Forschungsdaten, die im Rahmen der „**zweckfreien Forschung**“ produziert werden, erhält der Dienstherr **kein Nutzungsrecht** (§&nbsp;43 UrhG).
Die Forschungsergebnisse müssen nicht zur Verfügung gestellt werden.
Ausnahme: gegebenenfalls Open Access-Pflicht bei drittmittelfinanzierter Forschung.

Für Forschungsdaten, die **im Rahmen einer Dienstpflicht** produziert werden, erhält der Dienstherr **Nutzungsrechte**.
Beispiele für die Dienstpflicht sind: Erstellung einer Patientenakte, die auch in der Forschung Verwendung findet; Forschungsdaten, die im Rahmen eines Forschungsauftrags bei Zusammenarbeit mit einem kommerziell tätigen Unternehmen produziert werden.

Forschungsdaten, die in einer Forschungsdatenbank gesammelt werden, können auch unter Schutzrecht für die investitionstätigende Institution stehen.
Hier sollte umgekehrt den Forschenden ein einfaches Nutzungsrecht am Datenbankherstellerrecht eingeräumt werden.

### Nichtwissenschaftliches Personal

*Beispiel: medizinisch-technische Angestellte, studentische Hilfskräfte*

Die Forschungsdaten entstehen im Rahmen einer weisungsgebundenen Tätigkeit, also erhält der Dienstherr Nutzungsrecht.
Idealerweise wird dem Arbeitgeber und dem/der weisungsgebenden Forschenden das Nutzungsrecht erteilt.

### Wissenschaftliche Mitarbeiter*innen

Für Forschungsdaten, die **im Rahmen der eigenen Forschung** produziert werden, erhält der Arbeitgeber, soweit nicht anders vereinbart, kein Nutzungsrecht (§&nbsp;43 UrhG).

Für Forschungsdaten, die **im Rahmen einer weisungsgebundenen** Arbeit entstehen, beispielsweise auch Lehrmaterialien, erhält der Arbeitgeber Nutzungsrecht.

Der Fall, dass Daten für das eigene Forschungsprojekt produziert werden, aber auch Teil eines großen Forschungsprojekts sind, muss **vorher** z. B. über die Betreuungsvereinbarung geklärt werden.

Auch die eventuelle Mitnahme der Daten bei Verlassen der Institution sollte geklärt werden, da nicht der/die (leitende) Forschende, sondern der Dienstherr das Nutzungsrecht erhält.

### Wissenschaftliche Mitarbeiter*innen von außeruniversitären Forschungseinrichtungen

Die an einer außeruniversitären Forschungseinrichtung tätigen leitenden Forschenden haben dem Zweck ihrer Forschungseinrichtung zu dienen. Für Werke, die im Rahmen der beruflichen Tätigkeit geschaffen worden sind, gehen damit **Nutzungsrechte an den Arbeitgeber** über (§&nbsp;43, §&nbsp;69b UrhG).

### Externe Personen

*Beispiel: externe Doktoranden*

Für Forschungsdaten, die im Rahmen der **eigenen Forschung** produziert werden, erhält der Arbeitgeber kein Nutzungsrecht.

Produziert die Person Forschungsdaten **im Rahmen eines Gemeinschaftsprojekts** und erhält dafür keine Vergütung, kann aber im Gegenzug auf die Forschungsdaten des Projekts zugreifen, wird die Betreuungszusage als Vertrag gewertet.
Dieser räumt die Nutzungsrechte an den Daten der externen Person ein.

Umgekehrt ist es wichtig, festzulegen, was passiert, wenn die betreuende Person die Institution zwischenzeitlich verlässt. In diesem Fall sollte sichergestellt werden, dass die relevanten Daten aus dem Datenpool weiterhin für die externe Person zugänglich sind.

### Forschungskooperation

Vereinbarungen zur Mitnahme und Weiternutzung sollten zu Beginn des Forschungsprojekts getroffen werden, d. h. in Textform festgehaltene Regelungen darüber, wer wann welche Daten zu welchen Zwecken nutzen darf.
Dies kann in Form von projektinternen Richtlinien oder im Rahmen von Kooperations- und Konsortialverträgen erfolgen.

## Patentrecht

<!-- TODO: Durch den Kontakt der eigenen Einrichtung ersetzen -->
Kontakt an der FAU: [Erfinderberatung und Patentmanagement an der FAU](https://www.fau.de/outreach/erfindungen-und-patente/erfinden-und-patentieren/)

Erfindungen auf allen Gebieten der Technik können als Patent geschützt werden, sofern sie auf einer erfinderischen Tätigkeit beruhen, neu und gewerblich anwendbar sind (§&nbsp;1 Abs.&nbsp;1 PatG).
Handelt es sich bei der Erfindung um eine **Diensterfindung**, entscheidet der Arbeitgeber über die Veröffentlichung der Forschungsdaten in Bezug auf die Erfindung.
Nach Anzeigen der Diensterfindung beim Arbeitgeber dürfen die Forschungsdaten in der Regel erst nach zwei Monaten veröffentlicht werden, da der Arbeitgeber zwei Monate Zeit hat, über die Inanspruchnahme der Diensterfindung zu entscheiden (§&nbsp;42 Nr.&nbsp;1 ArbnErfG).
Bei der Archivierung und Veröffentlichung von Forschungsdaten ist eine mögliche Patentfähigkeit von Forschungsergebnissen zu beachten.
Erfindungen, die auf der Grundlage von Daten gemacht werden, können nicht mehr zum Patent angemeldet werden, sobald die Daten der Öffentlichkeit (einem nicht überschaubaren Personenkreis) zugänglich gemacht wurden.

## Geschäftsgeheimnisse

Geschäftsgeheimnisse können sich aus in Daten verkörperten Informationen ergeben.
Selbstständig arbeitende Forschende, insbesondere Hochschullehrende, sind selbst Inhabende des Geschäftsgeheimnisses, ansonsten steht das Geschäftsgeheimnis der Forschungseinrichtung zu.
Daraus folgt, wer befugt ist, über die Veröffentlichung zu entscheiden.
Zu beachten ist außerdem, dass auch Geschäftsgeheimnisse Dritter berührt sein können und eine enge Abstimmung über den Umgang mit den betroffenen Daten erforderlich ist.

## Dual-Use Problematik

Die *Dual-Use*-Problematik bezeichnet die **doppelte Verwendungsmöglichkeit von Forschungsergebnissen zu nützlichen und schädlichen Zwecken**.
Darunter fällt insbesondere besorgniserregende sicherheitsrelevante Forschung: Wissen, Produkte oder Technologien, die missbraucht werden können, um Menschenwürde, Leben, Gesundheit, Freiheit, Eigentum, Umwelt oder ein friedliches Zusammenleben erheblich zu schädigen.
Einige Forschungsbereiche, die unter erhöhter Aufmerksamkeit betrachtet werden, sind die Genomchirurgie, Synthetische Biologie, Robotik und Künstliche Intelligenz.

Durch die *Dual-Use*-Problematik ergeben sich nicht nur rechtliche, sondern auch ethische Grenzen der Wissenschaft.
Forschende haben eine besondere Verantwortung aufgrund ihres Wissens, ihrer Erfahrung und ihrer Fähigkeiten, die sie zur Erkennung der Risiken einsetzen müssen.
Daraus ergibt sich eine Selbstregulierung der Wissenschaft, die über die rechtlichen Vorgaben hinausgeht.
Dies bedeutet, dass gesetzlich nicht untersagte Forschungsvorhaben unter Umständen nur modifiziert oder überhaupt nicht durchgeführt werden sollten.

Vor der Veröffentlichung von Forschungsergebnissen, also auch Daten, Software und Algorithmen, sollte überprüft werden, ob diese unmittelbar -- „ohne erhebliches Wissen und ohne aufwendige Umsetzungs- und Anwendungsprozesse“ -- zu spezifischen Gefahren oder erheblichem Schaden führen können.
Dies bedeutet nicht unbedingt Verzicht auf die Publikation, sondern kann bedeuten, relevante Forschungsergebnisse kenntlich auszunehmen oder nur mit bestimmten Personen zu teilen.

Die DFG hat im März 2018 den Umgang mit sicherheitsrelevanten Aspekten in ihren Leitfaden für die Antragsstellung aufgenommen und bietet weiterführende Informationen auf der Seite „[Zum Umgang mit Sicherheitsrelevanter Forschung](https://www.dfg.de/foerderung/grundlagen_rahmenbedingungen/sicherheitsrelevante_forschung/index.html)“.

In **DFG-Anträgen** und **Horizon Europe-Anträgen** wird eine **ethische Selbstevaluation** gefordert.
Beachten Sie auch die jeweiligen Empfehlungen in Ihrem Fachbereich.

Die [Kommission für Ethik sicherheitsrelevanter Forschung an der FAU](https://www.fau.de/fau/leitung-und-gremien/gremien-und-beauftragte/kommissionen/#collapse_5) sollte konsultiert werden, wenn eine *Dual-Use*-Problematik besteht. <!--TODO: Gegebenenfalls durch die Kommission der eigenen Einrichtung ersetzen.-->

Weiterführende Informationen zur *Dual-Use*-Problematik:

- Nationale Akademie der Wissenschaften Leopoldina und Deutsche Forschungsgemeinschaft: [Wissenschaftsfreiheit und Wissenschaftsverantwortung - Empfehlungen zum Umgang mit sicherheitsrelevanter Forschung / Scientific Freedom and Scientific Responsibility – Recommendations for Handling of Security-Relevant Research](https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/sicherheitsrelevante_forschung/empfehlungen_de_en.pdf) (2022).
- Nationale Akademie der Wissenschaften Leopoldina und Deutsche Forschungsgemeinschaft: [Gemeinsamer Ausschuss zum Umgang mit sicherheitsrelevanter Forschung – Vierter Tätigkeitsbericht zum 1. November 2022](https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/sicherheitsrelevante_forschung/taetigkeitsbericht_de.pdf) (2022).

## Lizenzen für Forschungsdaten

Es empfiehlt sich, auf standardisierte Lizenzen zurückzugreifen, wenn Forschungsdaten veröffentlicht oder archiviert werden.

### Creative Commons

Für Texte, Abbildungen und Daten werden die [Creative Commons](https://creativecommons.org/), insbesondere CC BY, CCO und CCO+ empfohlen.

CCO: Diese Lizenz ist ein Sonderfall.
Sie bietet möglichst große Annäherung an die Gemeinfreiheit.
Ein Werk kann in Deutschland nicht in die Gemeinfreiheit gegeben werden, da das Urheberrecht nicht abgegeben werden kann.
Nicht urheberrechtlich geschützte Daten können wiederum nicht mit CCO versehen werden, da sonst die Annahme über ein Urheberrecht vergeben wird.
Im Zweifelsfall ist es besser, die Daten mit „Gemeinfrei“ zu kennzeichnen.

Zusatz: Das Element *Non-Commercial (NC)* beinhaltet eine Unklarheit, da „kommerzielle Zwecke“ nicht klar definiert ist. Handelt es sich nur um die kommerzielle Verwendung oder muss der Verwender auch nicht kommerziell auftreten?

### GNU General Public Licence

Für Software wird die [GNU General Public Licence (GPL)](https://www.gnu.org/licenses/gpl-3.0.en.html) empfohlen.

### Open Data Commons

Für Daten und Datenbanken empfiehlt sich auch eine der [Open Data Commons](https://opendatacommons.org/) Lizenzen.

### Datenlizenz Deutschland

Die [Datenlizenz Deutschland](https://www.govdata.de/lizenzen) betrifft Daten der öffentlichen Verwaltung.

## Legal Helpdesks

Einige NFDI bieten über ihre Kontaktformulare explizit Informationen zu rechtlichen und ethischen Fragen, jedoch keine Rechtsberatung an:

- [GHGA](https://www.ghga.de/about-us/contact) (*Deutsches Humangenom-Archiv*)
- [NFDI4Culture](https://nfdi4culture.de/de/kontakt.html) (*Konsortium für Forschungsdaten zu materiellen und immateriellen Kulturgütern*)
- [Text+](https://www.text-plus.org/kontakt/) (*Sprach- und textbasierte Forschungsdateninfrastruktur*)

Außerdem:

- [CLARIAH-DE](https://www.clariah.de/beratung-und-schulung/rechtliches-ethisches-lizenzen) bietet eine Kontaktaufnahme zu rechtlichen und ethischen Fragen in den Geisteswissenschaften an.

## Kontakte an der FAU
<!--TODO: Durch die Kontakte der eigenen Einrichtung ersetzen-->

- [FAU Competence Center for Research Data and Information (CDI)](https://www.cdi.fau.de/)
- Referent für Forschungsdatenmanagement an der Universitätsbibliothek: Dr. Jürgen Rohrwild, E-Mail: [juergen.rohrwild@fau.de](mailto:juergen.rohrwild@fau.de), [Website](https://ub.fau.de/forschen/daten-software-forschung/)
- Referentin für Urheberrecht, Datenschutzrecht, Veröffentlichungsrecht an der Universitätsbibliothek: Petra Heermann, E-Mail: [petra.heermann@fau.de](mailto:petra.heermann@fau.de), [Website](https://ub.fau.de/forschen/daten-software-forschung/)
- [Datenschutzbeauftragter der FAU](https://www.fau.de/fau/leitung-und-gremien/gremien-und-beauftragte/beauftragte/datenschutzbeauftragter)
- [Kommission für Ethik sicherheitsrechtlicher Forschung an der FAU](https://www.fau.de/fau/leitung-und-gremien/gremien-und-beauftragte/kommissionen/#collapse_5) – in Bezug auf *Dual-Use*-Risiken von Forschungsdaten und Forschungssoftware
- [Erfinderberatung und Patentmanagement an der FAU](https://www.fau.de/outreach/erfindungen-und-patente/erfinden-und-patentieren/)

## Weiterführende Materialien

- BAUMANN, Paul, Philipp KRAN und Anne LAUBER-Rönsberg, 2019. Entscheidungsbaum: Datenschutzrechtliche Fragestellungen für die Veröffentlichungen von Forschungsdaten. Verfügbar unter: <https://tu-dresden.de/gsw/phil/irget/jfbimd13/ressourcen/dateien/dateien/DataJus/Entscheidungsbaum-Digital-12-02.pdf?lang=de>.
- BAUMANN, Paul und Philipp KRAHN, 2020. Rechtliche Rahmenbedingungen des FDM. 2020. Verfügbar unter: <https://tu-dresden.de/gsw/phil/irget/jfbimd13/ressourcen/dateien/dateien/DataJus/Rechtliche-Rahmenbedingungen-des-FDM_.pdf?lang=de>.
- BAUMANN, Paul, Philipp KRAHN und Anne LAUBER-RÖNSBERG, 2021. Forschungsdatenmanagement und Recht: Datenschutz-, Urheber- und Vertragsrecht. Feldkirch/Düns: Wolfgang Neugebauer Verlag. Arbeitshefte der Arbeitsgemeinschaft für juristisches Bibliotheks- und Dokumentationswesen. ISBN 978-3-85376-328-5.
- FISCHER, Veronika und Grischka PETRI, 2022. Bildrechte in der kunsthistorischen Praxis – ein Leitfaden. - Zweite, überarbeitete und erweiterte Auflage. Verband Deutscher Kunsthistoriker. Verfügbar unter: <https://archiv.ub.uni-heidelberg.de/artdok/7769/>.
- KLIMPEL, Paul, 2022. In Bewegung. Die Rechtsfibel für Digitalisierungsprojekte in Kulturerbe-Einrichtungen. November 2022. Verfügbar unter: <https://nbn-resolving.org/urn:nbn:de:0297-zib-86485>.
- KLIMPEL, Paul und Fabian RACK, 2022. Einschätzung der rechtlichen Rahmenbedingungen für die Archivierung von Social-Media- Inhalten im Archiv der sozialen Demokratie. Annabel WALZ und Andreas MARQUET (Hrsg.), Sicher Sichern? Social-Media-Archivierung aus rechtlicher Perspektive im Archiv der sozialen Demokratie. 2022. Bd. 17, S. 15–48. Verfügbar unter: <https://library.fes.de/pdf-files/adsd/19590.pdf>.
- KUSCHEL, Linda, 2018. Wem „gehören“ Forschungsdaten? Forschung & Lehre. 12 September 2018. Bd. 9. Verfügbar unter: <https://www.forschung-und-lehre.de/forschung/wem-gehoeren-forschungsdaten-1013>.
- KUSCHEL, Linda, 2020. Urheberrecht und Forschungsdaten. Ordnung der Wissenschaft. 2020. Nr. 1, S. 43–52. DOI [10.17176/20200103-154726-0](https://doi.org/10.17176/20200103-154726-0).
- LAUBER-RÖNSBERG, Anne und Felix MAGIN, 2023. Wer hat an der Universität welche Rechte an Forschungsdaten? Forschungsdaten.info live. 23.01.2023. [Folien](https://forschungsdaten.info/securedl/sdl-eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2NzU3NDYyNDYsImV4cCI6MTY3NjQzNzQ0NCwidXNlciI6MCwiZ3JvdXBzIjpbMCwtMV0sImZpbGUiOiJmaWxlYWRtaW5cL2tvb3BlcmF0aW9uZW5cL2J3ZmRtXC9mZG1cL2ZvcnNjaHVuZ3NkYXRlbi5pbmZvX2xpdmVcL0ZvbGllbnNhdHpfZmQtaW5mby1saXZlX1JlY2h0c2ZyYWdlbi0yNC0wMS0yM19BTFIucGRmIiwicGFnZSI6MTEzNzA2fQ.PauqwbivIH1KobEsVqrR-sZjYHzOxfz2G2BJtAhFqdI/Foliensatz_fd-info-live_Rechtsfragen-24-01-23_ALR.pdf).
- NATIONALE AKADEMIE DER WISSENSCHAFTEN LEOPOLDINA und DEUTSCHE FORSCHUNGSGEMEINSCHAFT, 2022. Wissenschaftsfreiheit und Wissenschaftsverantwortung - Empfehlungen zum Umgang mit sicherheitsrelevanter Forschung / Scientific Freedom and Scientific Responsibility – Recommendations for Handling of Security-Relevant Research. 2. aktualisierte Auflage. Halle (Saale). Verfügbar unter: <https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/sicherheitsrelevante_forschung/empfehlungen_de_en.pdf>.
- NATIONALE AKADEMIE DER WISSENSCHAFTEN LEOPOLDINA und DEUTSCHE FORSCHUNGSGEMEINSCHAFT, 2022. Gemeinsamer Ausschuss zum Umgang mit sicherheitsrelevanter Forschung – Vierter Tätigkeitsbericht zum 1. November 2022. Halle (Saale). Verfügbar unter: <https://www.dfg.de/download/pdf/foerderung/grundlagen_dfg_foerderung/sicherheitsrelevante_forschung/taetigkeitsbericht_de.pdf>.
- RATSWD, 2020. Handreichung Datenschutz. 2. vollständig überarbeitete Auflage. RatSWD Output 8 (6). Berlin, Rat für Sozial- und Wirtschaftsdaten (RatSWD). DOI: [10.17620/02671.50](https://doi.org/10.17620/02671.50)
- Rechtswissenschaftliche Handreichungen zu juristischen Problemen aus dem Projekt „Mining and Modeling Text“ (U. Trier): [IRDT PaperSeries](https://irdt.uni-trier.de/projekte/paperseries/).
- WÜNSCHE, Stephan, Volker SOSSNA, Vanessa KREITLOW und Pia VOIGT, 2022. Urheberrechte an Forschungsdaten – Typische Unsicherheiten und wie man sie vermindern könnte: Ein Diskussionsimpuls. Bausteine Forschungsdatenmanagement. 14 März 2022. Nr. 1, S. 26–42. DOI [10.17192/bfdm.2022.1.8369](https://doi.org/10.17192/bfdm.2022.1.8369).
