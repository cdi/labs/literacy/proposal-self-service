# Potenzielle Kandidaten für fachliche Themenschwerpunkte

<!-- Inhaltsverzeichnis für die GitLab-Version, muss für eine PDF-Version gelöscht/auskommentiert werden -->
[[_TOC_]]

## Generell

| Bezeichnung | Beschreibung | Webseite |
| - | - | - |
| forschungsdaten.info | Informative Seite über das FDM | [https://www.forschungsdaten.info/](https://www.forschungsdaten.info/) |
| Praxis Kompakt auf forschungsdaten.info|Kurse, Tutorien und Anleitungen auf forschungsdaten.info | [https://www.forschungsdaten.info/praxis-kompakt/tutorials-kurse-und-anleitungen/](https://www.forschungsdaten.info/praxis-kompakt/tutorials-kurse-und-anleitungen/) |
| UAG Schulungen und Fortbildungen | Materialsammlung zu Schulungen im Forschungsdatenmanagement; von der UAG Schulungen/Fortbildungen der DINI/nestor AG Forschungsdaten kuratiert | [https://rs.cms.hu-berlin.de/uag_fdm/pages/themes.php](https://rs.cms.hu-berlin.de/uag_fdm/pages/themes.php) |
| RfII | Mailingliste des Rats für Informationsinfrastrukturen | [https://listserv.gwdg.de/mailman/listinfo/rfii_infoticker](https://listserv.gwdg.de/mailman/listinfo/rfii_infoticker) |
| NFDI | Veranstaltungskalender der NFDI | [https://www.nfdi.de/termine/](https://www.nfdi.de/termine/) |
| The Carpentries | Hilfe bei Organisation von Workshops zu Forschungsdaten und Software oder Finden von Referierenden für selbst-organisierte Workshops |[https://carpentries.org/workshops/#workshop-curriculum](https://carpentries.org/workshops/#workshop-curriculum) [https://carpentries.org/instructors/](https://carpentries.org/instructors/) |
Verein Deutscher Bibliothekarinnen und Bibliothekare (VDB) | Durchführung von Workshops mit von The Carpentries zertifizierten Instruktorinnen und Instruktoren und kann diese auch vermitteln | [https://www.vdb-online.org/kommissionen/qualifikation/index.php](https://www.vdb-online.org/kommissionen/qualifikation/index.php) |

## Regionale Angebote

| Bezeichnung | Beschreibung | Webseite |
| - | - | - |
| fdm-bayern.org | Forschungsdatenmanagement Bayern | [https://www.fdm-bayern.org/](https://www.fdm-bayern.org/) |
| Kompetenz-Pool FDM des BVB | Bibliotheksverbund Bayern | [https://www.bib-bvb.de/web/kvb](https://www.bib-bvb.de/web/kvb) |
| bw2FDM | Baden-Württemberg | [https://www.forschungsdaten.info/fdm-im-deutschsprachigen-raum/baden-wuerttemberg/schulungsangebote/](https://www.forschungsdaten.info/fdm-im-deutschsprachigen-raum/baden-wuerttemberg/schulungsangebote/) |
| fdm.nrw | Workshops von fdm.nrw für Mitglieder von Hochschulen aus NRW | [https://www.fdm.nrw/index.php/veranstaltungen/](https://www.fdm.nrw/index.php/veranstaltungen/) |
| Thüringer Kompetenznetzwerk Forschungsdatenmanagement | Thüringen | [https://forschungsdaten-thueringen.de/veranstaltungen.html](https://forschungsdaten-thueringen.de/veranstaltungen.html) |
| SaxFDM | Sachsen |[https://saxfdm.de/veranstaltungen/](https://saxfdm.de/veranstaltungen/) |
| FDM-BB | Berlin-Brandenburg | [https://fdm-bb.de/](https://fdm-bb.de/) |
| HeFDI | Hessen | [https://www.uni-marburg.de/de/hefdi/events](https://www.uni-marburg.de/de/hefdi/events) |
| Hamburg Open Science | Hamburg | [https://openscience.hamburg.de/de/startseite-hamburg-open-science/](https://openscience.hamburg.de/de/startseite-hamburg-open-science/) |
| FDM.rlp | Rheinland-Pfalz | [https://fdm.rlp.net/](https://fdm.rlp.net/)|

## Fachliche Angebote

### Geistes-, Sozial- und Verhaltenswissenschaften

| Bezeichnung | Beschreibung | Webseite |
| - | - | - |
| NFDI4Culture | materielle und immaterielle Kulturgüter: Architektur, Kunst-, Musik-, Tanz-, Theater-, Film- und Medienwissenschaften | [https://nfdi4culture.de/news-events/events.html](https://nfdi4culture.de/news-events/events.html) |
| KonsortSWD | Sozial-, Verhaltens-, Bildungs- und Wirtschaftswissenschaften | [https://www.konsortswd.de/aktuelles/veranstaltungen/](https://www.konsortswd.de/aktuelles/veranstaltungen/)|
| Text + | text- und sprachbasierte Forschungsdaten | [https://www.text-plus.org/](https://www.text-plus.org/) |
| Berd\@NFDI | Wirtschafts- und ökonomische Daten | [https://www.berd-nfdi.de/](https://www.berd-nfdi.de/) |
| AG Digitale Romanistik | Vermittlung von Workshops zu digitalen Methoden in der Romanistik |[https://www.deutscher-romanistenverband.de/ag-digitale-romanistik/ag-aktivitaeten/](https://www.deutscher-romanistenverband.de/ag-digitale-romanistik/ag-aktivitaeten/) |
| forTEXT | Workshops zu Methoden und Software für die digitale Textanalyse; auch die Webseite gibt über viele *state of the art*-Analyseverfahren Auskunft | [https://fortext.net/ueber-fortext/fortext-in-forschung-und-lehre](https://fortext.net/ueber-fortext/fortext-in-forschung-und-lehre) |
| AG Digitale Geschichtswissenschaften im VHD | Hinweise zu Workshops (Praxislabore) im Bereich Forschungsdaten für die Geschichtswissenschaften | [https://digigw.hypotheses.org/category/veranstaltungen](https://digigw.hypotheses.org/category/veranstaltungen) |
| DGPhil-AG „Philosophie der Digitalität" | erarbeitet unter anderem Möglichkeiten einer digitalen beziehungsweise *digital operierenden* Philosophie und bietet Veranstaltungen an | [https://digitale-philosophie.de/](https://digitale-philosophie.de/)|
| Arbeitskreis Digitale Kunstgeschichte | Workshops und Vernetzungsmöglichkeiten | [https://digitale-kunstgeschichte.de/](https://digitale-kunstgeschichte.de/) |
| ZPID (Psychologie) | Veranstaltungen zu drei Forschungsbereichen: Forschungsliteralität, Forschungssynthesen und Big Data | Forschungsliteralität: [https://leibniz-psychology.org/forschung/forschungsliteralitaet/#c109](https://leibniz-psychology.org/forschung/forschungsliteralitaet/#c109), Forschungssynthesen: [https://leibniz-psychology.org/forschung/forschungssynthesen/#c131](https://leibniz-psychology.org/forschung/forschungssynthesen/#c131), Big Data: [https://leibniz-psychology.org/forschung/big-data/#c228](https://leibniz-psychology.org/forschung/big-data/#c228) |
| VerbundFDB | Erziehungs-/Bildungswissenschaft); verschiedene Online-Seminare und Workshops auf Nachfrage | [https://www.forschungsdaten-bildung.de/schulung?la=de](https://www.forschungsdaten-bildung.de/schulung?la=de) |
| GESIS | Weiterbildungen und Workshops zu Datenanalyse, Survey Methodology und Computational Social Science | [https://www.gesis.org/gesis-training/home](https://www.gesis.org/gesis-training/home) |
| CESSDA | Trainingsmaterial zu Datenmanagement in den Sozialwissenschaften an |[https://www.cessda.eu/Training/Training-Resources](https://www.cessda.eu/Training/Training-Resources) |
| Forschungsdatenzentrum Qualiservice | Workshops zu qualitativen Daten in den Sozialwissenschaften | [https://www.qualiservice.org/de/workshops.html](https://www.qualiservice.org/de/workshops.html) |
| Forschungsdatenzentrum eLabour | Workshops im Bereich arbeitssoziologische Daten an | [http://elabour.de/aktuelles/elabour-workshops/](http://elabour.de/aktuelles/elabour-workshops/) |
| FID Osmikon / OstData | Ost-, Ostmittel- und Südosteuropa-Forschung;  Workshops und Webinare | [https://www.osmikon.de/services/schulungen](https://www.osmikon.de/services/schulungen) |
| FID Anglo-American Culture and History | Workshops | [https://libaac.de/about/events/](https://libaac.de/about/events/) |
| FID Asien | Hilfe im Bereich Forschungsdaten | [https://crossasia.org/service/forschungsdaten/](https://crossasia.org/service/forschungsdaten/) |
| FID Benelux | Expertise im Bereich Forschungsdaten | [https://www.fid-benelux.de/information-vernetzung/expertise/](https://www.fid-benelux.de/information-vernetzung/expertise/) |

### Naturwissenschaften

| Bezeichnung | Beschreibung | Webseite |
| - | - | - |
| NFDI4Biodiversity | Umweltwissenschaften/Ökologie | [https://www.nfdi4biodiversity.org/de/](https://www.nfdi4biodiversity.org/de/) |
| NFDI4Chem | Chemie | [https://nfdi4chem.de/index.php/events/](https://nfdi4chem.de/index.php/events/), insbesondere [Workshop Series: FAIR Research Data Management: Basics for Chemists](https://www.nfdi4chem.de/index.php/event/workshop-fair-research-data-management-basics-for-chemists/) |
| NFDI4Cat | Katalyse | [https://nfdi4cat.org/events/](https://nfdi4cat.org/events/) |
| FAIRmat | Physik: kondensierte Materie und chemische Physik fester Stoffe | [https://www.fairmat-nfdi.eu/fairmat/events-fairmat](https://www.fairmat-nfdi.eu/fairmat/events-fairmat), Hands-on Tutorials: https://www.fairmat-nfdi.eu/fairmat/outreach-fairmat/tutorials-fairmat |
| PUNCH4NFDI | Teilchen-, Astro-, Astroteilchen-, Hadronen- und Kernphysik | [https://www.punch4nfdi.de/news_amp_events/events/](https://www.punch4nfdi.de/news_amp_events/events/) |
| DataPLANT | Pflanzen-Grundlagenforschung | [https://nfdi4plants.de/](https://nfdi4plants.de/) |
| DAPHNE4NFDI | Photonen- und Neutronenforschung | [https://www.daphne4nfdi.de/86.php](https://www.daphne4nfdi.de/86.php) |
| NFDI4Earth | Erdsystemforschung | [https://nfdi4earth.de/home/events/upcoming-events](https://nfdi4earth.de/home/events/upcoming-events) |
| NFDI4Microbiota | Mikrobiota-Forschung | [https://nfdi4microbiota.de/training/training](https://nfdi4microbiota.de/training/training) |
| Gesellschaft Deutscher Chemiker | kostenpflichtige Fortbildungen zu Datenmanagement | [https://www.gdch.de/veranstaltungen.html](https://www.gdch.de/veranstaltungen.html) |

### Technik

| Bezeichnung |Beschreibung | Webseite |
| - | - | - |
| NFDI4ing | Ingenieurwissenschaften | [https://nfdi4ing.de/events/](https://nfdi4ing.de/events/) |
| NDFIxCS | Informatik | [https://nfdixcs.org/veranstaltungen](https://nfdixcs.org/veranstaltungen) |
| Gesellschaft für Informatik | Veranstaltungen mit Bezug zu Forschungsdaten im Kalender | [https://gi.de/aktuelles/veranstaltungen](https://gi.de/aktuelles/veranstaltungen) |
| NFDI4DataScience | Datenwissenschaften und Künstliche Intelligenz | [https://www.nfdi4datascience.de/events/](https://www.nfdi4datascience.de/events/) |
| NFDI-MatWerk | Materialwissenschaften | [https://nfdi-matwerk.de/events](https://nfdi-matwerk.de/events) |
| MaRDI | Mathematik | [https://www.mardi4nfdi.de/events.html](https://www.mardi4nfdi.de/events.html) |
| FID move | Beratung und Workshops zu Forschungsdaten im Bereich Mobilitäts- und Verkehrsforschung | [https://www.fid-move.de/de/forschungsdaten/beratung-zu-forschungsdaten](https://www.fid-move.de/de/forschungsdaten/beratung-zu-forschungsdaten) |

### Medizin

| Bezeichnung |Beschreibung | Webseite |
| - | - | - |
| NFDI4Health | personenbezogene Gesundheitsdaten | [https://www.nfdi4health.de/de/aktuelles/veranstaltungen/](https://www.nfdi4health.de/de/aktuelles/veranstaltungen/) |
| NFDI Neuroscience | Neurowissenschaften | [https://nfdi-neuro.de/events/](https://nfdi-neuro.de/events/) |
| GHGA | Humane Genomdaten | [https://www.ghga.de/events](https://www.ghga.de/events) |
| ZB Med | Unterstützung bei der Durchführung von Workshops im Bereich Lebenswissenschaften in Kooperation mit örtlichen FDM-Beauftragten | [https://www.publisso.de/open-access-beraten/webinare/](https://www.publisso.de/open-access-beraten/webinare/), [https://www.publisso.de/open-access-beraten/workshops/](https://www.publisso.de/open-access-beraten/workshops/) |
| TMF | Medizin; Fortbildungen | [https://www.tmf-ev.de/Ueber_uns/TMFAkademie.aspx](https://www.tmf-ev.de/Ueber_uns/TMFAkademie.aspx) |

---

Tabelleninhalte aus: Krömer, Cora, & Rohrwild, Jürgen. (2021). Erstellung eines Service-Portfolios zur FDM-Vermittlung in der Graduiertenausbildung (Version 1). Zenodo. DOI: [10.5281/zenodo.5572331](https://doi.org/10.5281/zenodo.5572331), S. 10-12.
