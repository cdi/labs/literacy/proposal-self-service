# Unterstützung im FDM an der FAU

<!-- Inhaltsverzeichnis für die GitLab-Version, muss für eine PDF-Version gelöscht/auskommentiert werden -->
[[_TOC_]]

## Generelle Begleitung bei der Antragstellung

Allgemeine Hinweise zur Förderung, Beratung und Antragstellung an der FAU: [https://www.fau.de/research/service-fuer-forschende/beratung-bei-drittmittelprojekten/foerderberatung-und-antragskonzeption/#collapse_0](https://www.fau.de/research/service-fuer-forschende/beratung-bei-drittmittelprojekten/foerderberatung-und-antragskonzeption/#collapse_0).

### Einzelförderungen

Ansprechperson für ERC-Grants: [Dr. Sabine Eber](https://www.fau.de/person/sabine-eber/) (Präsidialstab).

Ansprechpersonen für DFG-Anträge:
[https://www.fau.de/research/service-fuer-forschende/foerderung-beratung-und-antragsstellung/projektantraege-und-abwicklung/deutsche-forschungsgemeinschaft/](https://www.fau.de/research/service-fuer-forschende/foerderung-beratung-und-antragsstellung/projektantraege-und-abwicklung/deutsche-forschungsgemeinschaft/).

Für das Forschungsdatenmanagement, siehe unten ["Begleitung beim Forschungsdatenmanagement"](#begleitung-beim-forschungsdatenmanagement) und die Checklisten zu [FDM im DFG-Antrag](de/fdm_dfg_antrag.md) oder die Checklisten [FDM im Horizon Europe-Antrag](de/fdm_horizon_europe_antrag.md) und [FDM im ERC-Antrag](de/fdm_erc_antrag.md).

### DFG-Gruppenförderung

#### SFB

Kontakt für die Beantragung von Sonderforschungsbereichen:
[S-Forschende](https://www.fau.de/fau/leitung-und-gremien/geschaeftsverteilungsplan-der-verwaltung/pr-stab-s-praesidialstab/referat-s-forschende/).

Für das Forschungsdatenmanagement, siehe unten ["Begleitung beim Forschungsdatenmanagement"](#begleitung-beim-forschungsdatenmanagement) und Checkliste [FDM im SFB-Antrag](de/fdm_sfb_antrag.md).

#### GRK

Kontakt für die Beantragung von Graduiertenkollegs:
[S-Forschende](https://www.fau.de/fau/leitung-und-gremien/geschaeftsverteilungsplan-der-verwaltung/pr-stab-s-praesidialstab/referat-s-forschende/).

Für das Forschungsdatenmanagement, siehe unten ["Begleitung beim Forschungsdatenmanagement"](#begleitung-beim-forschungsdatenmanagement) und Checkliste [FDM im GRK-Antrag](de/fdm_grk_antrag.md) sowie die [FAU Service-Matrix Schulungen](de/fau_service_matrix_schulungen.md).

## Begleitung beim Forschungsdatenmanagement

Frühe Kontaktaufnahme mit dem Forschungsdatenmanagement-Team der FAU, damit die Antragsplanung bestmöglich begleitet werden kann: [forschungsdaten@fau.de](mailto:forschungsdaten@fau.de).

Im Antrag: Verweis auf die [Forschungsdaten-Policy](https://fau.info/fdm-policy) der FAU.

Informieren Sie sich bitte darüber, was die Forschungsförderer aktuell hinsichtlich Ihrer Forschungsdaten erwarten. Listen der Policies der wichtigsten Fördermittelgeber sind hier zusammengestellt: [Webseite der Universitätsbibliothek](https://ub.fau.de/forschen/daten-software-forschung/policies-der-forschungsfoerderorganisationen-und-verlage/) und [fdm-bayern.org](https://www.fdm-bayern.org/policies/).

### FAU Competence Center for Research Data and Information (CDI)

(ehemals [AG Digitale Forschungsdaten und
Forschungsinformation](https://www.agfd.fau.de/))

Zentrale wissenschaftliche Einrichtung zur Unterstützung aller
Forschenden beim Forschungsdatenmanagement und
Forschungsinformationssystem
[CRIS](https://cris.fau.de/converis/portal?lang=de_DE).

Services:

- [Antragsberatung](https://www.cdi.fau.de/services/antraege/)
- Beratung zu Metadaten (Erstellung und Ergänzung)
- Beratung zur Speicherung und Archivierung von Daten
- Schulungen
- Beratung zur [FAUDataCloud](https://www.cdi.fau.de/services/faudatacloud-it-services-fuer-forschungsdaten/)
- Beratung zu [WissKI und die FAUWissKICloud](https://www.cdi.fau.de/services/wisski-und-fauwisskicloud/)
- Beratung zu [elektronischen Laborbüchern](https://www.cdi.fau.de/services/elektronisches-laborbuch/)
- Beratung zu [CRIS](https://www.cdi.fau.de/services/forschungsinformationssystem-fau-cris/)

Webseite: [https://www.cdi.fau.de](https://www.cdi.fau.de)

Kontakt:

Dr. Marcus Walther\
Freyeslebenstr. 1, Raum 04.6411\
91058 Erlangen\
Tel: +49 9131 85-70701\
Sprechzeiten: Mi, 13:00 - 15:00, Raum 08.154, gerne anrufen für andere Zeiten
E-Mail: [marcus.walther@fau.de](mailto:marcus.walther@fau.de)

### Universitätsbibliothek

#### Referat Forschungsdatenmanagement

Services:

- Beratung zum FDM: Unterstützung von der Anfangsplanung und Antragsprozess bis Projektende zu Recherche, Planung, Beschreibung, Datenpublikation, Zugriffsbestimmungen, Nutzungsbedingungen und Datenformaten.
- Schulungen: [Einführung ins Forschungsdatenmanagement](https://ub.fau.de/schulungen/einfuehrung-ins-forschungsdatenmanagement/) (120 Minuten, nach Vereinbarung, min. 5 Teilnehmende)
- Bereitstellung der [RDMO-Instanz](https://rdmo.ub.fau.de/) und Hilfe zum Umgang ([FAQ](https://ub.fau.de/haeufig-gestellte-fragen-faq/forschungsdaten/rdmo/) und [Videos auf StudOn](https://www.studon.fau.de/studon/goto.php?target=lm_2993053))
- [Zugang zum RADAR-Repositorium](https://ub.fau.de/forschen/daten-software-forschung/archivierung-und-publikation-der-daten/#collapse_0) zur Publikation und Archivierung von Forschungsdaten abgeschlossener wissenschaftlicher Studien und Projekte

Webseite: [https://ub.fau.de/forschen/daten-software-forschung/](https://ub.fau.de/forschen/daten-software-forschung/)

Kontakt:

Dr. Jürgen Rohrwild\
Technisch-naturwissenschaftliche Zweigbibliothek\
Raum: Raum 0.314\
Erwin-Rommel-Str. 60\
91058 Erlangen\
Tel: +49 9131 85-28591\
Fax: +49 9131 85-27843\
E-Mail: [juergen.rohrwild@fau.de](mailto:juergen.rohrwild@fau.de)

#### Rechtsfragen im Bereich Open Access, Forschungsdatenmanagement

Kontakt:

Petra Heermann\
Alte Universitätsbibliothek\
Universitätsstraße 4\
91054 Erlangen\
Tel: +49 9131 85-23920\
E-Mail: <petra.heermann@fau.de>

#### Digital Humanities (Software-Werkzeuge, Normdaten, Ontologien und Linked Data)

Services:

- Beratung und Schulungen für die digitalen Geisteswissenschaften zu:
  - Datenmanagement
  - Softwarewerkzeuge
  - Datenstrukturen
  - Normdaten Ontologien und Linked Data

Webseite mit Hinweisen zu Beratungen:
[https://ub.fau.de/ub-coach/forschen/digital-humanities/](https://ub.fau.de/ub-coach/forschen/digital-humanities/)

Webseite des Digital Skills HUB:
[https://ub.fau.de/lernen/digital-skills-hub/](https://ub.fau.de/lernen/digital-skills-hub/)

Kontakt für Schulungen:

Stephanie Kolbe\
Wirtschafts- und Sozialwissenschaftliche Zweigbibliothek\
Lange Gasse 20\
90403 Nürnberg\
Tel: +49 9131 85-95495
E-Mail: [stephanie.kolbe@fau.de](mailto:stephanie.kolbe@fau.de)

Kontakt Digital Skills HUB:

Tonka Stoyanova\
Hauptbibliothek\
Universitätsstraße 4\
91054 Erlangen\
Tel: +49 9131 85-22161\
E-mail: [tonka.stoyanova@fau.de](mailto:tonka.stoyanova@fau.de)

### Regionales Rechenzentrum Erlangen (RRZE)

Services:

- [Serverdienste](https://www.rrze.fau.de/serverdienste)
(Serverhosting, Serverhousing), Webdienste, Datenbanken
- [Hochleistungsrechner (HPC)](https://www.rrze.fau.de/serverdienste/hpc/)
- [Gitlab](https://www.rrze.fau.de/serverdienste/infrastruktur/gitlab/)
- [Backup und Archivierung](https://www.rrze.fau.de/serverdienste/infrastruktur/backup-archivierung/)

Übersicht über Dienste rund um Forschungsdaten:
[https://www.rrze.fau.de/forschung/forschungsdaten/](https://www.rrze.fau.de/forschung/forschungsdaten/)

Webseite des RRZE: [https://www.rrze.fau.de/](https://www.rrze.fau.de/)

Kontakt:

Zentrale Service-Theke\
Martensstraße 1\
Raum 1.031\
91058 Erlangen\
Tel: +49 9131 85-29966\
E-Mail: [rrze-zentrale@fau.de](mailto:rrze-zentrale@fau.de)

## Werkzeuge für das Forschungsdatenmanagement

Folgend eine Auflistung einiger Werkzeuge, die entweder an der FAU (mit-)entwickelt oder extern entwickelt, aber als hilfreich und zu empfehlen befunden werden.

### Datenmanagementpläne (DMP)

Auch wenn noch nicht alle Fördermittelgeber einen Datenmanagementplan (DMP) einfordern, ist es sinnvoll für sich und das Projekt, gerade auch für kollaboratives Arbeiten mit Daten, einen DMP zu erstellen und über die Projektlaufzeit aktuell zu halten.

Für Fragen zum DMP, gerne eine E-Mail an [forschungsdaten@fau.de](mailto:forschungsdaten@fau.de) schreiben.

Es gibt verschiedene Software-Werkzeuge, die bei der Erstellung eines DMPs unterstützen:

- Internes Tool: [RDMO-Instanz](https://rdmo.ub.fau.de/) (Research Data Management Organizer) der FAU
  - Weitere Hinweise zum RDMO als [FAQ](https://ub.fau.de/haeufig-gestellte-fragen-faq/forschungsdaten/rdmo/#collapse_19479)
  - Hinweise und Anleitungen zum Umgang mit dem RDMO auf [StudOn](https://www.studon.fau.de/studon/goto.php?target=lm_2993053).
- Externe Tools:
  - [DMPOnline](https://dmponline.dcc.ac.uk/): sinnvoll, falls der DMP den Auflagen eines Forschungsförderers aus dem Vereinigten Königreich genügen soll.
  - [DMPTool](https://dmptool.org/): sinnvoll, falls der DMP den Auflagen eines Forschungsförderers aus den USA genügen soll.
  - [DMP-Wizard](https://www.clarin-d.net/de/aufbereiten/datenmanagementplan-entwickeln) des CLARIN-Projektes für Forschungsprojekte über Sprache. Sehr nützlich, wenn die Daten in einem der [CLARIN-Datenzentren](https://www.clarin-d.net/de/aufbereiten/clarin-zentrum-finden) archiviert werden sollen.
  - [DataWiz](https://datawiz.leibniz-psychology.org/DataWiz/): speziell für Psychologie.
  - [GFBio Data Management Plan Tool](https://www.gfbio.org/plan): speziell für Biodiversität/Lebenswissenschaft, kann auch in verwandten Disziplinen hilfreich sein.
  - [ARGOS](https://argos.openaire.eu/home) von OpenAIRE
  - [Data Stewardship Wizard](https://ds-wizard.org/): Science Europe und Horizon 2020 Fragenkataloge, mit FAIR Metrics.

**Fragenkataloge** für verschiedene Fördermittelgeber können bei der Erstellung des DMPs helfen:

In der FAU-[RDMO](https://rdmo.ub.fau.de/)-Instanz finden Sie:

- RDMO-Fragenkatalog (Standard)
- RDMO-Fragenkatalog mit Zusatzinformationen für die Geisteswissenschaften
- DFG-Anträge
- DFG-Anträge (Alte Kulturen, Fachkollegium 101)
- DFG-Anträge (Wirtschafts-/Sozialwissenschaften)
- DFG-Anträge (Erziehungswissenschaften, Fachkollegium 109)
- DFG-Anträge (Sozial- & Kulturanthropologie, Judaistik, Religionswissenschaften)
- DFG-Anträge (Wissenschaftliche Editionen in Literaturwissenschaften)
- DFG-Anträge (Chemie)
- DFG-Anträge (Physik)
- Horizon 2020 Katalog
- VW-Stiftung -- Science Europe Datenmanagementplan

### Tools

- [WissKI](http://wiss-ki.eu/), eine virtuelle Forschungsumgebung:
WissKI ist ein System zum Management, zur Analyse und zur Publikation von Forschungsdaten in den Kulturwissenschaften. Es implementiert Semantic-Web Methoden zur Akquisition, Speicherung, Wiederverwendung und semantischen Annotation von Artefakten. Durch eine standardisierte Basis-Ontologie ([CIDOC-CRM](http://www.cidoc-crm.org/)) mit domänenspezifischen Erweiterungen wird einerseits interdisziplinäre Wiederverwendbarkeit sichergestellt und andererseits ein flexibles Datenmodell ermöglicht.
  - [WissKI distillery](<https://github.com/FAU-CDI/wisski-distillery>): ein Cloud-System für WissKIs, das von dem CDI betrieben und entwickelt wird. Damit lassen sich WissKIs für FAU-Projekte auf Knopfdruck erstellen und zentral auf den neusten technischen Stand halten (siehe auch [WissKI und FAUWissKICloud](https://www.cdi.fau.de/services/wisski-und-fauwisskicloud/) auf der CDI-Website oder das Wiki [FAUWissKICloud](https://gitlab.rrze.fau.de/cdi/labs/wisski/FAUWissKICloud/-/wikis/FauWissKIcloud))
  - [AG WissKI](https://www.izdigital.fau.de/team/wisski/) der FAU
  - Für technische Fragen: [Laura Albers](mailto:laura.albers@fau.de) und [Tom Wiesing](mailto:tom.wiesing@fau.de)
- [DataCite-Metadaten-Generator](https://dhvlab.gwi.uni-muenchen.de/datacite-generator/) zur Erstellung von Metadaten nach dem DataCite-Schema
  - [DataCite Best Practice Guide](https://doi.org/10.5281/zenodo.3559799)

### Speicherung & Archivierung/Repositorien

- [FAUbox](https://www.rrze.fau.de/serverdienste/server/faubox/#collapse_0) zur Speicherung und Datenaustauch während der Projektlaufzeit, auch für sensible Daten geeignet, 50GB.
- [FAUDataCloud](https://www.cdi.fau.de/services/faudatacloud-it-services-fuer-forschungsdaten/), als Grundversorgung 2 TB Archivplatz, 100 GB Projektspeicher, zukünftig 50 GB Datenbankplatz, allgemeine Dienste wie GitLab, Chat-Service und spezifische Dienste wie ELN, WissKI, JupyterLab etc. Bedarfe darüber hinaus können bei dem CDI beantragt werden. Großprojekte wie GRK/SFB müssen gesondert beantragt werden.
- Datenrepositorium [RADAR](https://www.radar-service.eu/de): weitere Infos auf [ub.fau.de](https://ub.fau.de/forschen/daten-software-forschung/archivierung-und-publikation-der-daten/#collapse_0)
- [FAU Zenodo Community](https://zenodo.org/communities/fau/?page=1&size=20)
- Suche nach weiteren Repositorien auf [re3data](https://www.re3data.org/)
- Speicherung und Archivierung der Forschungsdaten durch das [RRZE](https://www.rrze.fau.de/forschung/forschungsdaten/)

### Hochleistungsrechner (HPC)

Für numerische Simulationen und Big-Data-Anwendungen stellt das RRZE Hochleistungsrechner und Compute-Server zur Verfügung.

Webseiten: [https://hpc.fau.de/](https://hpc.fau.de/) und
[HPC-Beratung](https://www.rrze.fau.de/infocenter/kontakt-hilfe/hpc-beratung/)

Kontakt:

HPC-Beratung\
Zentrale Systeme\
Martensstraße 1\
91058 Erlangen\
Email: [support-hpc@fau.de](mailto:support-hpc@fau.de)
