# FDM im SFB-Antrag

<!-- Inhaltsverzeichnis für die GitLab-Version, muss für eine PDF-Version gelöscht/auskommentiert werden: -->
[[_TOC_]]

## Allgemeine Regeln und Empfehlungen

Bitte kontaktieren Sie möglichst früh im Antragsprozess das **Forschungsdatenmanagement-Team** der FAU: [forschungsdaten@fau.de](mailto:forschungsdaten@fau.de).

Beachten Sie die **DFG-Dokumente** zu Forschungsdaten und die Hinweise zu den NFDI-Konsortien in [FDM im DFG-Antrag](de/fdm_dfg_antrag.md).

Beachten Sie die **SFB-spezifischen** Hinweise der DFG:

- [Merkblatt Sonderforschungsbereiche](https://www.dfg.de/formulare/50_06/50_06_de.pdf), Abschnitt I.1 "Ziel" und Abschnitt III. 1.3 "Teilprojekt Informationsinfrastruktur" [Nr. 50.06 - 09/24].

Der Umgang mit Forschungsdaten sollte in der Skizze und im Antrag dargestellt werden:

In der **Skizze** wird üblicherweise 1 Seite dem Forschungsdatenmanagement gewidmet:

- [Beschreibung des Vorhabens - Antragsskizze Sonderforschungsbereiche](http://www.dfg.de/formulare/53_120_elan/53_120_de_elan.rtf), Abschnitt 2.2 "Forschungsprogramm und langfristiges Forschungsziel" [Nr. 53.120 elan - 09/24].

Im **Antrag** sollten 2 Seiten nicht unterschritten werden; bei datenlastigen Projekten wird eine ausführliche Stellungnahme erwartet:

- [Antragsmuster für die Einrichtung eines Sonderforschungsbereichs [[RTF-Version](https://www.dfg.de/resource/blob/168434/8c90fe21ab9057d4605e33cf13697a18/60-100-de-rtf-data.rtf)], Abschnitt 1.2.2 "Ausführliche Darstellung des Forschungsprogramms", Abschnitt 1.3.3 "Forschungsinfrastruktur". Falls ein Integriertes Graduiertenkolleg beantragt wird, ist auch Abschnitt B 3.3 "Qualifizierungskonzept" relevant. Abschnitt C betrifft ein mögliches Teilprojekt Informationsinfrastruktur [Nr. 60.100 - 09/24].

## Zentrale Punkte im SFB-Antrag

Beachten Sie für die Angaben zum Forschungsdatenmanagement im SFB-Antrag bitte den Abschnitt "**[Zentrale Punkte im DFG-Antrag](de/fdm_dfg_antrag.md#zentrale-punkte-im-dfg-antrag)**" in [FDM im DFG-Antrag](de/fdm_dfg_antrag.md).

Beachten Sie auch die [fachspezifischen Empfehlungen](https://www.dfg.de/de/grundlagen-rahmenbedingungen/grundlagen-und-prinzipien-der-foerderung/forschungsdaten/empfehlungen) der DFG.
Die Handreichung [Fachspezifisches FDM im DFG-Antrag](de/fachspezifisches_fdm_dfg_antrag.md) bietet einen Überblick der Anforderungen und Empfehlungen.

Für SFB-Anträge kommen folgende Punkte hinsichtlich des Forschungsdatenmanagements hinzu:

- Gemeinsame Konzeptentwicklung zum Umgang mit Forschungsdaten im Verbund
- Auf- und Ausbau von Dateninfrastrukturen: Anschlussfähigkeit an existierende Einrichtungen und Strukturen, Überlegungen zur Nachhaltigkeit
- Entwicklung von Methoden im Umgang mit Daten
- Elemente der Aus- und Weiterbildung im Bereich Forschungsdaten

Die Beschreibung des Umgangs mit verwendeten, neu erhobenen und/oder verarbeiteten Daten sollte im **Forschungsprogramm (1.2.2)** oder falls inhaltlich sinnvoll in der **Projektbeschreibung der Teilprojekte (A.3.4)** mit Verweis in 1.2.2 erfolgen.
In welcher Form die **am Verbund beteiligten Institutionen** das Datenmanagement unterstützen, sollte in **Abschnitt 1.3.3** erläutert werden.
Falls ein **Teilprojekt Informationsinfrastruktur** beantragt wird, kann an dieser Stelle darauf verwiesen werden und die unten genannten Angaben dort gemacht werden:

- Charakterisierung der entstehenden, erzeugten, ausgewerteten Daten
- Fachspezifische Konzepte und Überlegungen zur Qualitätssicherung
- Gibt es disziplinäre Standards?
- Archivierung: möglichst fachspezifische Repositorien
- Erläuterung eventueller rechtlicher Einschränkungen?
- Planungen zum Zeitpunkt der Datenpublikation?
- In welcher Form unterstützen die am Verbund beteiligten Institutionen das Daten- und Informationsmanagement?

Für Aufgaben des Forschungsdatenmanagements im Rahmen des Koordinationsprojekts oder in den Teilprojekten können [DFG-Mittel](https://www.dfg.de/de/grundlagen-rahmenbedingungen/grundlagen-und-prinzipien-der-foerderung/forschungsdaten/beantragbare-mittel) beantragt werden.

## Teilprojekt Informationsinfrastruktur

Falls das Vorhaben **datengetrieben** ist oder **besondere Herausforderungen in der Datenverwaltung** anfallen werden: [Beantragung eines INF-Projektes](https://www.dfg.de/de/foerderung/foerdermoeglichkeiten/programme/koordinierte-programme/sfb/antragsteller/programmelement-inf) zum Aufbau einer spezifischen Infrastruktur zur Unterstützung des Datenmanagements in Kooperation mit Einrichtungen wie Universitätsbibliothek und Rechenzentrum.
Zu den Zielen und der Darstellung eines Arbeitsprogramms, siehe Antrag, Anschnitt C.3.4 "Planung des Teilprojekts" (S. 37):

- Aufbau einer Datenbank zur Speicherung von Forschungsdaten, Sicherstellung der Interoperabilität
- Pflege, Vernetzung und Erschließung von Forschungsdaten
- Virtuelle Forschungsumgebungen
- Aufbau von interoperablen Komponenten für die virtuelle Zusammenarbeit im SFB
- Adaptation und Implementierung neuer Technologien und Verfahren mit Nutzen für den SFB (INF nicht als reiner Dienstleister, sondern mit enger wissenschaftlicher Verknüpfung)

Laut den [Hinweisen zur Begutachtung](http://www.dfg.de/formulare/60_14/60_14_de.pdf) [Nr. 60.14 - 09/24] werden folgende Punkte hinsichtlich eines INF-Projektes besonders bewertet:

- Prototypische Entwicklungen
- Besondere, neuartige Ideen im Datenumgang
- Umsetzung fachspezifischer Standards zu Dateninhalten und -formaten oder formulierte Alternativen falls nicht vorhanden.
- Anschluss an andere Einrichtungen, Vernetzung mit Einrichtungen am Standort
- Angemessene inhaltliche Würdigung der wissenschaftlichen Teilprojekte
- Vermittlung von Kompetenz an Teilprojektleitende und Weiterbildung des wissenschaftlichen Nachwuchses im Bereich Forschungsdaten
- Überlegungen zur Nachhaltigkeit/Langzeitverfügbarkeit der zu entwickelnden Informationsinfrastruktur

## Integriertes Graduiertenkolleg

Falls ein integriertes Graduiertenkolleg beantragt wird: Beachten Sie auch die Hinweise in der Checkliste [FDM im GRK-Antrag](de/fdm_grk_antrag.md) zum Qualifizierungskonzept im Bereich Forschungsdaten („Schulungen zur Erhebung und nachhaltigen Sicherung von Forschungsdaten"), außerdem die Handreichung [„Erstellung eines Service-Portfolios zur FDM-Vermittlung in der Graduiertenausbildung"](https://doi.org/10.5281/zenodo.5572331) und die [FAU Service-Matrix Schulungen](de/fau_service_matrix_schulungen.md).
