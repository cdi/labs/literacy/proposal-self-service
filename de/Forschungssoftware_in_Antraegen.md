# Forschungssoftware
## Was ist Forschungssoftware?
Forschungssoftware umfasst eine Vielzahl von Programmen, Skripten und Algorithmen, die in wissenschaftlichen Untersuchungen eingesetzt werden. Sie reicht von einfachen Skripten zur Datenaggregation und Software-Werkzeugen zur Steuerung von Messgeräten über komplexe Datenanalysetools bis hin zu aufwändigen Simulationen.

Forschungssoftware stellt damit einen entscheidenden Baustein für die Entstehung neuer Forschungsergebnisse und damit auch für deren die Reproduzierbarkeit dar. Daher fordert  Open Science und Forschungsförderer, die dies auf ihre Fahnen geschrieben haben, auch soweit möglich Transparenz, Offenheit und die breiter Zugangs im Bezug auf Forschungssoftware.

## Vorgaben / Empfehlungen der Forschungsförderer
### DFG
Die DFG ist Mitglied der „Research Software Alliance“ (https://www.researchsoft.org/) und Unterzeichner der „Amsterdam Declaration on Funding Research Software Sustainability“ (https://adore.software/declaration).

Die DFG fordert derzeit (Stand 01/25) die DFG nicht explizit einen Softwaremanagementplan. Das Thema Forschungssoftware sollte im Förderantrag – sofern zutreffend – trotzdem behandelt werden. Die nachfolgenden Punkte folgen den Empfehlungen des DFG-Papiers „Handling of Research Software in the DFG`s Funding Activities“ (2024), S. 7-10):

* Inwieweit findet Neu- oder Weiterentwicklung von Forschungssoftware statt?
* Dokumentation der eingesetzten oder (weiter)entwickelten Software, um eine langfristige Zugänglichkeit zu den Forschungsergebnissen zu gewährleisten. Dies betrifft nicht nur den Quellcode, die Workflows und die Funktionalität der Software, sondern auch die umfassende Dokumentation der einzelnen Parameter, um den Forschungsprozess verifizierbar und reproduzierbar zu gestalten.
* Versionsmanagement der Software
* Maßnahmen zur Qualitätssicherung der Software im Forschungs- und Entwicklungsprozess
* Integration, Nutzung und Lizenzierung von Codes Dritter 
* transparente Zitation
* Klärung der Rechte bezüglich der Autorenschaft der Softwareentwickler vor Projektstart
* Lizenzbedingungen
* Publikation von Forschungssoftware und Softwaredokumentation entsprechend den Maßgaben von Open Science und FAIR4RS [Link] (im Rahmen der rechtlichen Möglichkeiten)
* Maßnahmen zur Gewährleistung von Zugänglichkeit, Erhaltung, Weiterentwicklung, Auffindbarkeit und Interoperabilität (speziell mit Blick auf die langfristige Perspektive)

DFG-Mittel können beantragt werden für Personal- und Materialkosten, aber auch für externe Serviceprovider, welche die Forschungssoftwarekomponenten entwickeln. Maßnahmen zur Verbesserung der ökologischen Nachhaltigkeit der Software sind ebenfalls förderfähig.

Sofern in der jeweiligen Ausschreibung konkrete Aussagen in Bezug auf Softwaremanagement und Softwaremanagementpläne enthalten sind, sind diese zu berücksichtigen.

Es sei auch auf die Leitlinien der DFG zur guten wissenschaftlichen Praxis verwiesen (https://www.dfg.de/download/pdf/foerderung/rechtliche_rahmenbedingungen/gute_wissenschaftliche_praxis/kodex_gwp.pdf).

### Vorgaben weiterer Forschungsförderer

**ERC / Horizon Europe** fordert zwar aktuell (Stand 02/25) nicht standardmäßig einen Softwaremanagementplan (im Gegensatz zum Datenmanagementplan). Allerdings gibt es die Vorgabe, dass der Zugang zu den Forschungsdaten und -ergebnissen gewährleistet sein soll. Um dies zu erreichen, ist vielfach der Einsatz von und damit Zugang zu spezifischer Forschungssoftware nötig. 

Das **BMBF** hat aktuell meist keine allgemeinen Vorgaben in Bezug auf Forschungssoftware. Es gibt jedoch oft Vorgaben in den einzelnen Förderlinien.

Ähnlich verhält es sich bei der **Volkswagenstiftung**. Auch hier wird Softwaremanagement nicht explizit gefordert. Da Software jedoch nicht nur als „Beiprodukt“, sondern als essenzieller Bestandteil der Forschung angesehen wird, sollte beim Antrag ggf. auf dieses Thema eingegangen werden. 

## FAIR4RS-Prinzipien
Die FAIR4RS-Prinzipien (FAIR for Research Software) der Research Data Alliance (RDA) adaptieren die bekannten FAIR-Prinzipien auf Forschungssoftware https://zenodo.org/records/6623556). Wenn die eigene Entwicklung diese Prinzipen folgt, bietet es sich an, das  auch im Antrag zu erwähnen.
1.	**Findable** (Auffindbar): Forschungssoftware sollte durch eindeutige, dauerhafte Identifikatoren wie DOI oder URN auffindbar sein. Außerdem sollte die Software in Repositorien oder Katalogen hinterlegt werden, die gut dokumentiert und zugänglich sind.
2.	**Accessible** (Zugänglich): Die Software sollte zugänglich sein, wobei dies sowohl den Download als auch den Zugriff auf den Quellcode und die Dokumentation umfasst. Auch Lizenzbedingungen und Nutzungsrechte sollten klar und transparent dargestellt werden.
3.	**Interoperable** (Interoperabel): Forschungssoftware sollte so entwickelt sein, dass sie mit anderen Software-Systemen und Daten kompatibel ist. Sie sollte offene Standards, Schnittstellen und Protokolle verwenden, um die Integration und Interoperabilität zu fördern.
4.	**Reusable** (Wiederverwendbar): Der Code sollte in einer Weise dokumentiert und strukturiert sein, die es anderen Forschenden ermöglicht, ihn zu verstehen, zu modifizieren und wiederzuverwenden. Hierbei spielen transparente Lizenzen und ausführliche Anleitungen eine wichtige Rolle.

## Weitere interessante Dokumente:

### Helmholtz-Zentren
* Richtlinie zur Verwertung und Lizenzierung von Forschungssoftware https://www.gfz.de/fileadmin/gfz/zentrum/Transfer_Innovation/Technologietransfer/GFZ_Software-Richtlinie-v.1.1.pdf  (Stand 04/23)
* Leitfaden Forschungssoftware: https://www.gfz.de/fileadmin/gfz/zentrum/Transfer_Innovation/Technologietransfer/GFZ_Software-Leitfaden-v.2.0.pdf (Stand 04/23)
### Allianz der Wissenschaftsorganisationen 
* „Handreichung zum Umgang mit Forschungssoftware“ (https://zenodo.org/records/1172970) (2018) 
* „Digitale Dienste für die Wissenschaft – wohin geht die Reise“ https://zenodo.org/records/4301924

Wilson, G; Bryan, J. et al (2017): Good enough practices in scientific computing. PLoS Comput Biol 13(6). https://doi.org/10.1371/journal.pcbi.1005510

Videos zu diversen Themen rund um Forschungssoftware auf der Homepage der ZB Med: https://darum.zbmed.de/warum-braucht-es-forschungssoftware/

## Quellen
Biernacka, K., & Helbig, K. (2023). Train-the-Trainer-Konzept zum Thema Forschungsdatenmanagement: Erweiterungsmodul Softwaremanagementplan (SMP). Zenodo. https://doi.org/10.5281/zenodo.10197107
https://forschungsdaten.info/themen/beschreiben-und-dokumentieren/softwareentwicklung-und-gute-wissenschaftliche-praxis/

