# FDM im Horizon Europe-Antrag

<!-- Inhaltsverzeichnis für die GitLab-Version, muss für eine PDF-Version gelöscht/auskommentiert werden -->
[[_TOC_]]

## Allgemeine Regeln und Empfehlungen

<!-- TO DO: E-Mail-Adresse der FDM-Verantwortlichen der Einrichtung. -->
Bitte kontaktieren Sie möglichst früh im Antragsprozess das **Forschungsdatenmanagement-Team** der FAU: <forschungsdaten@fau.de>.

<!-- TO DO: Ggf. durch die Forschungsdaten-Policy der Einrichtung ersetzen. -->
- Verweisen Sie immer auf die [Forschungsdaten-Policy](https://fau.info/fdm-policy) und die [Open Science Policy](https://doi.org/10.5281/zenodo.5602559) der FAU.
- Beziehen Sie sich auf die [FAIR-Prinzipien](https://www.go-fair.org/fair-principles/).

In Horizon Europe soll **Open Science** als *Modus Operandi* für alle Forschenden gelten: Wissen, Daten und Instrumente werden früh möglichst offen geteilt.

Beachten Sie bitte die Angaben zu Forschungsdaten und Open Science:

- [Model Grant Agreement (MGA)](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/common/agr-contr/general-mga_horizon-euratom_en.pdf) (Annex 5, Artikel 17)
- [Annotated Grant Agreement (AGA)](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/common/guidance/aga_en.pdf) (Anmerkungen zu Annex 5, Artikel 17).

Weitere Informationen finden Sie in den relevanten Abschnitten zu „Open Science" im [Horizon Europe Programme Guide](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/horizon/guidance/programme-guide_horizon_en.pdf).

Qualität und Angemessenheit der Forschungsverfahren werden im Hinblick auf Open Science unter **Part B, Abschnitt 1. Excellence** (insbesondere unter **1.2 Methodology**) bewertet.
Open Science-Praktiken sollten auf ca. einer Seite erläutert werden.
Falls Open Science-Praktiken in Ihrem Projekt nicht möglich sind, begründen Sie dies.
Forschungsprojekte, die Daten erzeugen oder nachnutzen, sollten auf einer (zusätzlichen) Seite ihr Forschungsdatenmanagement darstellen.
Innovative Forschungsverfahren, die sicherstellen, dass die Forschungsergebnisse zeitnah, relevant, transparent und reproduzierbar sind, können im Rahmen des Förderprogramms gefördert werden.
Im dazugehörigen Grant Agreement werden üblicherweise Details zu Veröffentlichung, Lizenzen und Metadaten festgelegt.
Forschende sollte diese kennen und in ihre Anträge einbeziehen.
Unter **Abschnitt 3. Quality and efficiency of the implementation** (insbesondere **3.2 Capacity of participants and consortium as a whole**) sollten auch die Expertise und nachweislichen Erfolge der Projektpartner\*innen im Hinblick auf Open Science miteinbezogen werden.

Je nach Arbeitsprogramm/Ausschreibung im Rahmen von Horizon Europe kann es weitere Forderungen in Bezug auf Open Science und Datenmanagement geben.
Beachten Sie bitte alle relevanten Dokumente und Hinweise.

### NFDI

Gegebenenfalls sollte mit den [NFDI-Konsortien](https://www.nfdi.de/konsortien/) Kontakt aufgenommen werden. Die **N**ationale **F**orschungs**D**aten**I**nfrastruktur ist eine Großinitiative des Bundes und der Länder, um (meist fachbezogene) Dienste und Best Practices im Datenmanagement zu entwickeln und zu etablieren. <!-- TO DO: Ggf. die Beteiligung der Einrichtung an NFDI-Konsortien erwähnen. --> Bei 11 von 27 Konsortien ist die FAU beteiligt:

- [DAPHNE4NFDI](https://www.daphne4nfdi.de/index.php) (Photonen- und Neutronenexperimente - Beugung, Tomographie, Bildgebung, Spektroskopie)
- [FAIRmat](https://www.konsortswd.de/https:/www.fair-di.eu/fairmat/fairmat_/consortium) (Kondensierte Materie und chemische Physik)
- [KonsortSWD](https://www.konsortswd.de/) (Sozial-, Verhaltens- und Wirtschaftswissenschaften)
- [MaRDI](https://www.mardi4nfdi.de/) (Mathematik)
- [MatWerk](https://nfdi-matwerk.de/) (Material- und Werkstoffwissenschaft)
- [NFDI4Cat](https://nfdi4cat.org/) (Katalyseforschung)
- [NFDI4Culture](https://nfdi4culture.de/) (Materielle und immaterielle Kulturgüter)
- [NFDI4Earth](https://www.nfdi4earth.de/) (Erdsystemforschung - Geowissenschaften)
- [NFDI4Energy](https://nfdi4energy.uol.de/) (Interdisziplinäre Energiesystemforschung)
- [NFDI4Microbiota](https://nfdi4microbiota.de/) (Lebenswissenschaften)
- [NFDI4Objects](https://www.nfdi4objects.net/) (Materielle Hinterlassenschaften der Menschheitsgeschichte)

<!-- TO DO: Ggf. mit betreffendem Kontakt ersetzen -->
Den Kontakt vor Ort stellt auch gerne das [FAU Competence Center for Research Data and Information (CDI)](https://www.cdi.fau.de/) her.
Unterstützung beim Umgang mit Forschungsdaten durch die beteiligten Institutionen sollte im Antrag erwähnt werden.

## Zentrale Punkte im Antrag

Im Antrag sollten generell die nachfolgenden Themen abgedeckt werden.

1. **Generierung oder Nachnutzung von Daten**:
Die Art und der Umfang der Daten sollten dargestellt werden.
Erwähnen Sie hier auch die verwendeten **Datenformate** in den einzelnen Forschungsschritten (z. B. Rohdaten, aufbereitete / anonymisierte Daten, aggregierte Daten).
Erwähnen Sie außerdem knapp **Qualitätssicherungsmaßnahmen** jenseits von Dokumentation und Standards; etwa doppelte Datenerhebung oder feste Kalibrierungsprotokolle.
Schließlich sollten Sie die Gelegenheit nutzen, darauf hinzuweisen, dass die Datenbeschreibung und Dokumentation mit Blick auf die [FAIR-Kriterien](https://www.go-fair.org/fair-principles/) erfolgt.

2. **Auffindbarkeit**:
Grundsätzlich sollen Daten der Community so früh wie möglich und im Rahmen der im Datenmanagementplan festgelegten Fristen, soweit möglich und sinnvoll, zur Nachnutzung in **vertrauenswürdigen Repositorien** (siehe [AGA](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/common/guidance/aga_en.pdf), Art. 17) zur Verfügung stehen.
Anträge für Arbeitsprogramme, die das Nutzen von der EOSC föderierter Repositorien erfordern, sollten die Nutzung dieser Repositorien explizit erwähnen.
Wenn Sie die Daten nicht in einer langfristigen Infrastruktur bereitstellen bzw. archivieren (etwa in [Zenodo](https://zenodo.org/), [RatSWD-zertifizierte Repositorien](https://www.konsortswd.de/datenzentren/alle-datenzentren/), NFDI-empfohlene Infrastrukturen [z. B. [NFDI4Culture](https://nfdi4culture.de/de/ressourcen/repositorien.html), [NFDI4Chem](https://www.nfdi4chem.de/index.php/repos/)] ...), sollten Sie immer auf eine Archivierung der Daten, auf denen Publikationen fußen, für mindestens zehn Jahre an der <!-- TO DO: Ggf. mit der entsprechenden Institution und ihrem Service ersetzen --> FAU in der [FAUDataCloud](https://www.cdi.fau.de/services/faudatacloud-it-services-fuer-forschungsdaten/) hinweisen (hier bitte vor Antragstellung den Volumenbedarf mit dem [CDI](https://www.cdi.fau.de/) absprechen).

3. **Zugang zu Daten**:
Grundsätzlich gilt **Open Access** zu Forschungsdaten: "so offen wie möglich, so beschränkt wie nötig"; idealerweise mit **CC0** oder **CC BY**-Lizenz.
Wenn Sie Daten für andere Forschende bereitstellen bzw. Daten publizieren, dann gehen Sie bitte auf die Zugangsbedingungen ein (beispielsweise Sperrfristen bis zur Zugänglichkeit, Zugangsvoraussetzungen).
Geben Sie an, warum Daten (nicht) zur Verfügung gestellt werden. Hier können Sie Nachnutzungspotenzial, großer Aufwand bei der Erhebung, unikalen Charakter oder etwa Datenschutzschranken, Urheberrecht, berechtigte Interessen von Industriepartnern etc. angeben.

4. **Interoperabilität**: Das Ziel sollte sein, die Daten so gut zu dokumentieren, dass jemand aus der Fach-Community den Entstehungsprozess nachvollziehen und die Daten nachnutzen kann.
Um das zu unterstreichen, verweisen Sie, wo möglich, auf existierende **Standards** (Metadaten, Ontologien, Klassifikationen), die in der Community verbreitet sind.
Falls es eine **NFDI** in Ihrem Fach gibt (siehe oben) und Sie einen dort empfohlenen Standard (oder Service) verwenden, **verweisen Sie gerne auf deren Empfehlungen**.
**FAIRe Metadaten** sollten mit **CC0** oder ähnlicher Lizenz veröffentlicht werden und gegebenenfalls persistente Identifikatoren der zugehörigen Publikationen und anderen Forschungsergebnissen beinhalten.

5. **Nachnutzung**: Sie sollten die **Nutzungslizenzen** der Daten und den Zugang zu Tools, Software und Modelle für die Datenerhebung und Validierung, Interpretation, Nachnutzung erwähnen.
Wenn Sie besondere Software einsetzen oder selbstgeschriebene verwenden, dann gehen Sie auch darauf ein, wie Sie die Nachvollziehbarkeit der Forschung sicherstellen (Publizieren der eigenen Software; die verwendete Software ist in dem Fachgebiet breit im Einsatz etc.).
Die **Dokumentation** von Software, Algorithmen und Modelle sollte bereitgestellt werden, um die Reproduzierbarkeit und Nachnutzungsmöglichkeit sicher zu stellen.

6. **Kuratierungs-, Sicherungs- und Archivierungskosten**:
Schließlich sollten Sie die Personen benennen, die im Projekt für einzelne Schritte im Datenmanagement letztendlich verantwortlich sind.
Typische Bereiche wären Einarbeiten neuer Mitarbeitender in die FDM-Workflows, Koordination zwischen Arbeitsgruppen, langfristige Speicherung.
In kleineren Projekten ist dies oft eine Person.
Sie sollten diese wenn möglich namentlich nennen.
Geben Sie auch an, wer bzw. welche Stelle für den Umgang mit den Daten nach Ende der Förderung zuständig ist (Stichwort: Nachhaltigkeit).
Bei der Speicherung können Sie hier z. B. auf die Dienste des <!-- TO DO: Ggf. durch die entsprechende Einrichtung (RRZE) und Services (FAUDataCloud) ersetzen --> RRZE beziehungsweise der FAUDataCloud (siehe auch 2.) verweisen.
Geben Sie hierbei auch an, welche **Ressourcen** in das FDM fließen (Personalmittel und sonstige Kosten).
Außerdem sollten Sie die Kosten des Datenmanagements (Qualitätssicherung, Speicherung, Verarbeitung, Archivierung) im Budget vorsehen, da diese erstattungsfähig sind.
In einzelnen Projekten werden punktuell IT-Personalressourcen für das Datenmanagement benötigt.
Beispielsweise eine IT-Stelle für nur 3 Monate, um eine Datenbank oder eine Forschungsumgebung anzupassen.
In solchen Fällen bitte <!-- TO DO: Ggf. durch den entsprechenden Kontakt ersetzen --> das [CDI](https://www.cdi.fau.de/) kontaktieren:
Das CDI hat Zugriff auf einen Pool von IT-Expert\*innen, mit dem sichergestellt werden kann, dass entsprechend beantragte Personenmonate auch zum Projektbeginn bereitstehen.

## Datenmanagementplan

Ein vollständiger Datenmanagementplan ist bei Antragseinreichung nicht erforderlich, sollte aber idealerweise vor Projektstart begonnen werden und muss normalerweise **6 Monate nach Projektstart** vorgelegt werden.
Ein aktualisierter DMP ist nach der Hälfte der Projektlaufzeit (wenn länger als 12 Monate) und am Ende der Projektlaufzeit einzureichen.
Idealerweise sollten DMPs veröffentlicht werden.
Ein [**DMP-Template**](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/horizon/temp-form/report/data-management-plan_he_en.docx) wird für Horizon Europe zur Verfügung gestellt.
<!-- TO DO: Ggf. Link zur RDMO-Instanz ersetzen oder ein anderes DMP-Tool erwähnen. -->
<!--Bei der Erstellung des DMPs für Horizon Europe kann der entsprechende **[RDMO-Fragenkatalog](https://rdmo.ub.fau.de/) genutzt werden.-->

Wir empfehlen die Verwendung der Vorlage, auch wenn sie sehr umfangreich ist. Dies erleichtert die formale Überprüfung Ihres DMPs auf Vollständigkeit und Übereinstimmung.

Wenn Sie sich dafür entscheiden, die Horizon Europe DMP-Vorlage nicht zu verwenden, sollten Sie sicherstellen, dass Sie dieselben Themen abdecken; insbesondere:

- Beschreibung der Datensätze: wissenschaftlicher Fokus, technische Herangehensweise, Datentypen, Datenvolumen.
- Standards und Metadaten.
- Name und persistente Identifikatoren der Datensätze.
- Kuratierungs- und Erhaltungsmaßnahmen: Standards zur Sicherung der Datenintegrität, Ablage in vertrauenswürdiges Repositorium.
- Datensicherheit: Backup-Maßnahmen, Maßnahmen zur Behandlung sensibler Daten.
- Datenverfügbarkeit: Zugang zu Daten zwecks Validierung ist zu gewährleisten; Nutzungslizenzen, Zugangsbeschränkungen, Zeitpunkt der Veröffentlichung, Dauer. Daten sollten so früh wie möglich nach der Generierung, spätestens bei Projektende veröffentlicht werden. Mit einer Publikation verbundene Daten sollten mit der Publikation zugänglich gemacht werden. Nicht nur Publikationen und Daten sollten offen verfügbar gemacht werden, sondern möglichst auch Software, Modelle, Apps, Algorithmen, Protokolle, Workflows, Elektronische Laborbücher etc. die im Forschungsprojekt entwickelt und/oder verwendet werden. Auch diese sollten im Datenmanagementplan berücksichtigt werden.
- Ethische und rechtliche Aspekte, zum Beispiel Informierte Einwilligungserklärung.
- Kosten des Datenmanagements und Verantwortlichkeiten: Kosten zur Generierung und/oder Nachnutzung von Daten, der Datendokumentation, der Datensicherung, der Veröffentlichung; Verantwortliche des Datenmanagements und der Qualitätssicherung.
